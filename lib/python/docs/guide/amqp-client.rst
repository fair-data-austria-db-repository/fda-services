AMQP API Client
===============

.. automodule:: dbrepo.AmqpClient
    :members:
    :no-index:
