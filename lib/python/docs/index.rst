DBRepo Python Library documentation
===================================

Use the DBRepo SDK to create, update, configure and manage DBRepo services such as the Data Service to get data as
Pandas `DataFrame <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html>`_ for analysis. The DBRepo SDK
provides an object-oriented API as well as low-level access to DBRepo services.

.. note::
   The SDK has been implemented and documented for DBRepo version 1.7.1, earlier versions may be supported but are not tested for compatibility.

Quickstart
----------

Find numerous quickstart examples on
the `DBRepo website <https://www.ifs.tuwien.ac.at/infrastructures/dbrepo/1.6/api/>`_.

AMQP API Client
-----------

.. toctree::
   :maxdepth: 2

   guide/amqp-client

REST API Client
-----------

.. toctree::
   :maxdepth: 2

   guide/rest-client

Upload API Client
-----------

.. toctree::
   :maxdepth: 2

   guide/upload-client

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
