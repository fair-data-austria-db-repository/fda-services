| **Metric**                  | **Description**                           |
|-----------------------------|-------------------------------------------|
| `dbrepo_message_receive`    | Received AMQP message from Broker Service |
| `dbrepo_subset_create`      | Create subset                             |
| `dbrepo_subset_data`        | Get subset data                           |
| `dbrepo_subset_find`        | Find subset                               |
| `dbrepo_subset_list`        | Find subsets                              |
| `dbrepo_subset_persist`     | Persist subset                            |
| `dbrepo_table_data_create`  | Insert tuple                              |
| `dbrepo_table_data_delete`  | Delete tuple                              |
| `dbrepo_table_data_export`  | Get table data                            |
| `dbrepo_table_data_history` | Get history                               |
| `dbrepo_table_data_import`  | Import dataset                            |
| `dbrepo_table_data_list`    | Get table data                            |
| `dbrepo_table_data_update`  | Update tuple                              |
| `dbrepo_table_schema_list`  | Find tables                               |
| `dbrepo_table_statistic`    | Get table statistic                       |
| `dbrepo_view_data`          | Get view data                             |
| `dbrepo_view_data_export`   | Get view data                             |
| `dbrepo_view_schema_list`   | Find views                                |
