# Search Service

## Actuator

- Health: http://localhost:5000/api/search/health
- Prometheus: http://localhost:5000/metrics

## Swagger UI Endpoints

- Swagger UI: http://localhost:5000/swagger-ui/

## OpenAPI Endpoints

- OpenAPI v3 as .json: http://localhost:5000/api-search.json