# Search Service - Init Container

Responsible for:

* Creating `database` index if not existing
* Importing database(s) from the Metadata Database
* Exit

## Development

Open in `./dbrepo-search-service` directory (depends on `clients` package).