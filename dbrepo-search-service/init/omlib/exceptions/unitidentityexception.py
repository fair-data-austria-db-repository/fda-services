class UnitIdentityException(Exception):

    def __init__(self, message):
        self.message = message


class ScaleIdentityException(Exception):

    def __init__(self, message):
        self.message = message
