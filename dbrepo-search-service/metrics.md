| **Metric**                      | **Description**                                         |
|---------------------------------|---------------------------------------------------------|
| `dbrepo_search_index_list`      | Time needed to list search index                        |
| `dbrepo_search_type_list`       | Time needed to list search types                        |
| `dbrepo_search_fuzzy`           | Time needed to search fuzzy                             |
| `dbrepo_search_type`            | Time needed to search by type                           |
| `dbrepo_search_update_database` | Time needed to update a database in the search database |
| `dbrepo_search_delete_database` | Time needed to delete a database in the search database |