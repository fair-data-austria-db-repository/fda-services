{{- if .Values.ingress.enabled }}
1. Get the application URL by running these commands:
  https://{{ .Values.hostname }}
{{- else }}
Enable ingress to access the UI with `ingress.enabled: true`.
{{- end }}
