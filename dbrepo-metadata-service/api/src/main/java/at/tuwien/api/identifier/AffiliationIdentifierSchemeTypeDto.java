
package at.tuwien.api.identifier;

import lombok.Getter;

@Getter
public enum AffiliationIdentifierSchemeTypeDto {
    ROR,
    GRID,
    ISNI
}
