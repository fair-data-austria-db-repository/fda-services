package at.tuwien.api.orcid.activities.employments.affiliation.group.summary.organization.disambiguated;

public enum OrcidDisambiguatedSourceTypeDto {
    RINGGOLD,
    ROR
}
