package at.tuwien.api.identifier;

import at.tuwien.api.database.LanguageTypeDto;
import at.tuwien.api.database.LicenseDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.extern.jackson.Jacksonized;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
@ToString
public class IdentifierSaveDto {

    @NotNull
    @Schema(example = "68e11675-1e0f-4d24-a6d9-887ad1c4445d")
    private UUID id;

    @NotNull
    @JsonProperty("database_id")
    @Schema(example = "null")
    private UUID databaseId;

    @JsonProperty("query_id")
    @Schema(example = "null")
    private UUID queryId;

    @JsonProperty("view_id")
    @Schema(example = "null")
    private UUID viewId;

    @JsonProperty("table_id")
    @Schema(example = "null")
    private UUID tableId;

    @NotNull
    @Schema(example = "database")
    private IdentifierTypeDto type;

    @Schema(example = "10.1111/11111111")
    private String doi;

    @NotNull
    @NotEmpty
    private List<SaveIdentifierTitleDto> titles;

    private List<SaveIdentifierDescriptionDto> descriptions;

    private List<SaveIdentifierFunderDto> funders;

    private List<LicenseDto> licenses;

    @JsonProperty("publication_day")
    @Schema(example = "15")
    private Integer publicationDay;

    @JsonProperty("publication_month")
    @Schema(example = "12")
    private Integer publicationMonth;

    @NotBlank
    @Schema(example = "TU Wien")
    private String publisher;

    private LanguageTypeDto language;

    @NotNull
    @JsonProperty("publication_year")
    @Schema(example = "2022")
    private Integer publicationYear;

    @NotNull
    @NotEmpty
    private List<SaveIdentifierCreatorDto> creators;

    @JsonProperty("related_identifiers")
    private List<SaveRelatedIdentifierDto> relatedIdentifiers;

}
