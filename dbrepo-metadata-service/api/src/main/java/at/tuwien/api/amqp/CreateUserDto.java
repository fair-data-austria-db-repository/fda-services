package at.tuwien.api.amqp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
@ToString
public class CreateUserDto {

    @Schema(example = "s3cr3t1nf0rm4t10n")
    private String password;

    @Schema(example = "administrator")
    private String tags;

}
