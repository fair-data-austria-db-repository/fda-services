package at.tuwien.api.identifier.ld;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.extern.jackson.Jacksonized;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
@ToString
public class LdDatasetDto {

    @NotNull
    @JsonProperty("@context")
    private String context;

    @NotNull
    @JsonProperty("@type")
    private String type;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String url;

    @NotNull
    private List<String> identifier;

    private String license;

    @NotNull
    private List<LdCreatorDto> creator;

    @NotNull
    private String citation;

    @NotNull
    private List<LdDatasetDto> hasPart;

    @NotNull
    private String temporalCoverage;

    @NotNull
    private Instant version;

}
