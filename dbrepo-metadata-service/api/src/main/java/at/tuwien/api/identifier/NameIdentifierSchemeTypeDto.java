
package at.tuwien.api.identifier;

import lombok.Getter;

@Getter
public enum NameIdentifierSchemeTypeDto {
    ORCID,
    ROR,
    ISNI,
    GRID
}
