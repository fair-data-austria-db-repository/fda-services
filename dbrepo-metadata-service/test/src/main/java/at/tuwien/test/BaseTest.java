package at.tuwien.test;

import at.tuwien.ExportResourceDto;
import at.tuwien.api.amqp.CreateVirtualHostDto;
import at.tuwien.api.amqp.ExchangeDto;
import at.tuwien.api.amqp.GrantVirtualHostPermissionsDto;
import at.tuwien.api.amqp.QueueDto;
import at.tuwien.api.auth.CreateUserDto;
import at.tuwien.api.container.ContainerBriefDto;
import at.tuwien.api.container.ContainerDto;
import at.tuwien.api.container.image.*;
import at.tuwien.api.database.*;
import at.tuwien.api.database.query.*;
import at.tuwien.api.database.table.*;
import at.tuwien.api.database.table.columns.*;
import at.tuwien.api.database.table.columns.concepts.*;
import at.tuwien.api.database.table.constraints.ConstraintsDto;
import at.tuwien.api.database.table.constraints.CreateTableConstraintsDto;
import at.tuwien.api.database.table.constraints.foreign.*;
import at.tuwien.api.database.table.constraints.primary.PrimaryKeyDto;
import at.tuwien.api.database.table.constraints.unique.UniqueDto;
import at.tuwien.api.datacite.DataCiteBody;
import at.tuwien.api.datacite.DataCiteData;
import at.tuwien.api.datacite.doi.DataCiteDoi;
import at.tuwien.api.identifier.*;
import at.tuwien.api.keycloak.*;
import at.tuwien.api.maintenance.BannerMessageCreateDto;
import at.tuwien.api.maintenance.BannerMessageDto;
import at.tuwien.api.maintenance.BannerMessageTypeDto;
import at.tuwien.api.maintenance.BannerMessageUpdateDto;
import at.tuwien.api.orcid.OrcidDto;
import at.tuwien.api.orcid.activities.OrcidActivitiesSummaryDto;
import at.tuwien.api.orcid.activities.employments.OrcidEmploymentsDto;
import at.tuwien.api.orcid.activities.employments.affiliation.OrcidAffiliationGroupDto;
import at.tuwien.api.orcid.activities.employments.affiliation.group.OrcidEmploymentSummaryDto;
import at.tuwien.api.orcid.activities.employments.affiliation.group.summary.OrcidSummaryDto;
import at.tuwien.api.orcid.activities.employments.affiliation.group.summary.organization.OrcidOrganizationDto;
import at.tuwien.api.orcid.person.OrcidPersonDto;
import at.tuwien.api.orcid.person.name.OrcidNameDto;
import at.tuwien.api.orcid.person.name.OrcidValueDto;
import at.tuwien.api.semantics.*;
import at.tuwien.api.user.UserAttributesDto;
import at.tuwien.api.user.*;
import at.tuwien.api.user.internal.UpdateUserPasswordDto;
import at.tuwien.entities.container.Container;
import at.tuwien.entities.container.image.ContainerImage;
import at.tuwien.entities.container.image.Operator;
import at.tuwien.entities.database.*;
import at.tuwien.entities.database.table.Table;
import at.tuwien.entities.database.table.columns.TableColumn;
import at.tuwien.entities.database.table.columns.TableColumnConcept;
import at.tuwien.entities.database.table.columns.TableColumnType;
import at.tuwien.entities.database.table.columns.TableColumnUnit;
import at.tuwien.entities.database.table.constraints.Constraints;
import at.tuwien.entities.database.table.constraints.foreignKey.ForeignKey;
import at.tuwien.entities.database.table.constraints.foreignKey.ForeignKeyReference;
import at.tuwien.entities.database.table.constraints.foreignKey.ReferenceType;
import at.tuwien.entities.database.table.constraints.primaryKey.PrimaryKey;
import at.tuwien.entities.database.table.constraints.unique.Unique;
import at.tuwien.entities.identifier.*;
import at.tuwien.entities.maintenance.BannerMessage;
import at.tuwien.entities.maintenance.BannerMessageType;
import at.tuwien.entities.semantics.Ontology;
import at.tuwien.entities.user.User;
import at.tuwien.test.utils.ArrayUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.Principal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MINUTES;

/**
 * Database 1 (Private Data, Private Schema, User 1) -> Container 1
 * <ul>
 * <li>Table 1 (Private Data, Private Schema)</li>
 * <li>Table 2 (Private Data, Public Schema)</li>
 * <li>Table 3 (Private Data, Private Schema)</li>
 * <li>Table 4 (Public Data, Private Schema)</li>
 * <li>Query 1</li>
 * <li>View 1 (Private Data, Private Schema)</li>
 * <li>View 2 (Public Data, Public Schema)</li>
 * <li>View 3 (Public Data, Private Schema)</li>
 * <li>Identifier 1 (Title=en, Description=en, type=database)</li>
 * <li>Identifier 2 (Title=en, Description=en, type=subset, queryId=1)</li>
 * <li>Identifier 3 (Title=en, Description=en, type=view, viewId=1)</li>
 * <li>Identifier 4 (Title=en, Description=en, type=table, tableId=1)</li>
 * </ul>
 * <p>
 * Database 2 (Private Data, Public Schema, User 2) -> Container 1
 * <ul>
 * <li>Table 5 (Public Data, Public Schema)</li>
 * <li>Table 6 (Public Data, Private Schema)</li>
 * <li>Table 7 (Public Data, Public Schema)</li>
 * <li>Query 2</li>
 * <li>Query 6</li>
 * <li>View 4 (Public Data, Private Schema)</li>
 * <li>Identifier 5 (Title=de, Description=de)</li>
 * </ul>
 * <p>
 * Database 3 (Public Data, Private Schema, User 3) -> Container 1
 * <ul>
 * <li>Table 8 (Private Data, Private Schema)</li>
 * <li>Query 3</li>
 * <li>Query 4</li>
 * <li>Query 5</li>
 * <li>View 5 (Public Data, Public Schema)</li>
 * <li>Identifier 6 (Title=en, Description=en, Query=3)</li>
 * </ul>
 * <p>
 * Database 4 (Public Data, Public Schema, User 4) -> Container 4
 * <li>Table 9</li>
 * <li>Identifier 7</li>
 * <li>Query 7</li>
 * <ul>
 * </ul>
 * <br />
 * User 1 (read)
 * <br />
 * User 2 (write-own)
 * <br />
 * User 3 (write-all)
 */
public abstract class BaseTest {

    public static final String MINIO_IMAGE = "minio/minio:RELEASE.2024-06-06T09-36-42Z";

    public static final String MARIADB_IMAGE = "mariadb:11.3.2";

    public static final String RABBITMQ_IMAGE = "rabbitmq:3.13.7";

    public static final String KEYCLOAK_IMAGE = "quay.io/keycloak/keycloak:26.0.4";

    public static final String[] DEFAULT_SEMANTICS_HANDLING = new String[]{"default-semantics-handling",
            "create-semantic-unit", "execute-semantic-query", "table-semantic-analyse", "create-semantic-concept"};

    public static final String[] DEFAULT_VIEW_HANDLING = new String[]{"update-database-view", "create-database-view",
            "delete-database-view", "list-database-views", "modify-view-visibility", "find-database-view"};

    public static final String[] ESCALATED_SEMANTICS_HANDLING = new String[]{"escalated-semantics-handling",
            "update-semantic-concept", "modify-foreign-table-column-semantics", "delete-ontology", "list-ontologies",
            "update-semantic-unit", "create-ontology", "update-ontology"};

    public static final String[] DEFAULT_CONTAINER_HANDLING = new String[]{"default-container-handling",
            "create-container", "list-containers", "modify-container-state"};

    public static final String[] ESCALATED_CONTAINER_HANDLING = new String[]{"escalated-container-handling",
            "modify-foreign-container-state", "delete-container"};

    public static final String[] DEFAULT_DATABASE_HANDLING = new String[]{"default-database-handling",
            "update-database-access", "modify-database-visibility", "create-database", "modify-database-owner",
            "delete-database-access", "check-database-access", "list-databases", "modify-database-image",
            "create-database-access", "find-database", "import-database-data"};

    public static final String[] ESCALATED_DATABASE_HANDLING = new String[]{"escalated-database-handling",
            "delete-database"};

    public static final String[] DEFAULT_IDENTIFIER_HANDLING = new String[]{"default-identifier-handling",
            "create-identifier", "find-identifier", "list-identifiers", "publish-identifier", "delete-identifier"};

    public static final String[] ESCALATED_IDENTIFIER_HANDLING = new String[]{"escalated-identifier-handling",
            "modify-identifier-metadata", "update-foreign-identifier", "create-foreign-identifier"};

    public static final String[] DEFAULT_QUERY_HANDLING = new String[]{"default-query-handling", "view-table-data",
            "execute-query", "view-table-history", "list-database-views", "export-query-data", "create-database-view",
            "delete-database-view", "delete-table-data", "export-table-data", "persist-query", "re-execute-query",
            "insert-table-data", "find-database-view"};

    public static final String[] ESCALATED_QUERY_HANDLING = new String[]{"escalated-query-handling"};

    public static final String[] DEFAULT_TABLE_HANDLING = new String[]{"default-table-handling",
            "list-tables", "create-table", "modify-table-column-semantics", "find-table", "delete-table",
            "update-table-statistic", "update-table"};

    public static final String[] ESCALATED_TABLE_HANDLING = new String[]{"escalated-table-handling",
            "delete-foreign-table"};

    public static final String[] DEFAULT_USER_HANDLING = new String[]{"default-user-handling", "modify-user-theme",
            "modify-user-information"};

    public static final String[] ESCALATED_USER_HANDLING = new String[]{"escalated-user-handling", "find-user"};

    public static final String[] DEFAULT_RESEARCHER_ROLES = ArrayUtils.merge(List.of(new String[]{"default-researcher-roles"},
            DEFAULT_CONTAINER_HANDLING, DEFAULT_DATABASE_HANDLING, DEFAULT_IDENTIFIER_HANDLING, DEFAULT_QUERY_HANDLING,
            DEFAULT_TABLE_HANDLING, DEFAULT_USER_HANDLING, DEFAULT_SEMANTICS_HANDLING, DEFAULT_VIEW_HANDLING));

    public static final String[] DEFAULT_DEVELOPER_ROLES = ArrayUtils.merge(List.of(new String[]{"default-developer-roles"},
            DEFAULT_CONTAINER_HANDLING, DEFAULT_DATABASE_HANDLING, DEFAULT_IDENTIFIER_HANDLING, DEFAULT_QUERY_HANDLING,
            DEFAULT_TABLE_HANDLING, DEFAULT_USER_HANDLING, ESCALATED_USER_HANDLING, ESCALATED_CONTAINER_HANDLING,
            ESCALATED_DATABASE_HANDLING, ESCALATED_IDENTIFIER_HANDLING, ESCALATED_QUERY_HANDLING,
            ESCALATED_TABLE_HANDLING, DEFAULT_VIEW_HANDLING));

    public static final String[] DEFAULT_DATA_STEWARD_ROLES = ArrayUtils.merge(List.of(new String[]{"default-data-steward-roles"},
            ESCALATED_IDENTIFIER_HANDLING, DEFAULT_SEMANTICS_HANDLING, ESCALATED_SEMANTICS_HANDLING, DEFAULT_VIEW_HANDLING));

    public static final String[] DEFAULT_LOCAL_ADMIN_ROLES = new String[]{"system"};

    public static final List<GrantedAuthorityDto> AUTHORITY_LOCAL_ADMIN_ROLES = Arrays.stream(DEFAULT_LOCAL_ADMIN_ROLES)
            .map(GrantedAuthorityDto::new)
            .collect(Collectors.toList());

    public static final List<GrantedAuthorityDto> AUTHORITY_DEFAULT_RESEARCHER_ROLES = Arrays.stream(DEFAULT_RESEARCHER_ROLES)
            .map(GrantedAuthorityDto::new)
            .collect(Collectors.toList());

    public static final List<GrantedAuthorityDto> AUTHORITY_DEFAULT_DEVELOPER_ROLES = Arrays.stream(DEFAULT_DEVELOPER_ROLES)
            .map(GrantedAuthorityDto::new)
            .collect(Collectors.toList());

    public static final List<GrantedAuthorityDto> AUTHORITY_DEFAULT_DATA_STEWARD_ROLES = Arrays.stream(DEFAULT_DATA_STEWARD_ROLES)
            .map(GrantedAuthorityDto::new)
            .collect(Collectors.toList());

    public static final List<GrantedAuthority> AUTHORITY_DEFAULT_LOCAL_ADMIN_AUTHORITIES = AUTHORITY_LOCAL_ADMIN_ROLES.stream()
            .map(a -> new SimpleGrantedAuthority(a.getAuthority()))
            .collect(Collectors.toList());

    public static final List<GrantedAuthority> AUTHORITY_DEFAULT_RESEARCHER_AUTHORITIES = AUTHORITY_DEFAULT_RESEARCHER_ROLES.stream()
            .map(a -> new SimpleGrantedAuthority(a.getAuthority()))
            .collect(Collectors.toList());

    public static final List<GrantedAuthority> AUTHORITY_DEFAULT_DEVELOPER_AUTHORITIES = AUTHORITY_DEFAULT_DEVELOPER_ROLES.stream()
            .map(a -> new SimpleGrantedAuthority(a.getAuthority()))
            .collect(Collectors.toList());

    public static final List<GrantedAuthority> AUTHORITY_DEFAULT_DATA_STEWARD_AUTHORITIES = AUTHORITY_DEFAULT_DATA_STEWARD_ROLES.stream()
            .map(a -> new SimpleGrantedAuthority(a.getAuthority()))
            .collect(Collectors.toList());

    public static final UUID REALM_DBREPO_ID = UUID.fromString("6264bf7b-d1d3-4562-9c07-ce4364a8f9d3");
    public static final String REALM_DBREPO_NAME = "dbrepo";
    public static final Boolean REALM_DBREPO_ENABLED = true;

    public static final UUID ROLE_DEFAULT_REALM_DBREPO_ROLES_ID = UUID.fromString("c74cbbe7-3ab1-4472-9211-cc904567268");
    public static final String ROLE_DEFAULT_REALM_DBREPO_ROLES_NAME = "default-dbrepo-roles";
    public static final UUID ROLE_DEFAULT_REALM_DBREPO_ROLES_REALM_ID = REALM_DBREPO_ID;

    public static final UUID ROLE_DEFAULT_RESEARCHER_ROLES_ID = UUID.fromString("c74cbbe7-3ab1-4472-9211-cc9045672682");
    public static final String ROLE_DEFAULT_RESEARCHER_ROLES_NAME = "default-researcher-roles";
    public static final UUID ROLE_DEFAULT_RESEARCHER_ROLES_REALM_ID = REALM_DBREPO_ID;

    public static final CreateAccessDto UPDATE_DATABASE_ACCESS_READ_DTO = CreateAccessDto.builder()
            .type(AccessTypeDto.READ)
            .build();

    public static final CreateAccessDto UPDATE_DATABASE_ACCESS_WRITE_OWN_DTO = CreateAccessDto.builder()
            .type(AccessTypeDto.WRITE_OWN)
            .build();

    public static final CreateAccessDto UPDATE_DATABASE_ACCESS_WRITE_ALL_DTO = CreateAccessDto.builder()
            .type(AccessTypeDto.WRITE_ALL)
            .build();

    public static final String TOKEN_ACCESS_TOKEN = "ey.yee.skrr";
    public static final String TOKEN_ACCESS_SCOPE = "openid";

    public static final TokenDto TOKEN_DTO = TokenDto.builder()
            .accessToken(TOKEN_ACCESS_TOKEN)
            .scope(TOKEN_ACCESS_SCOPE)
            .build();

    public static final UUID CONCEPT_1_ID = UUID.fromString("8cabc011-4bdf-44d4-9d33-b2648e2ddbf1");
    public static final String CONCEPT_1_NAME = "precipitation";
    public static final String CONCEPT_1_URI = "http://www.wikidata.org/entity/Q25257";
    public static final String CONCEPT_1_DESCRIPTION = null;
    public static final Instant CONCEPT_1_CREATED = Instant.ofEpochSecond(1701976048L) /* 2023-12-07 19:07:27 (UTC) */;

    public static final ConceptSaveDto CONCEPT_1_SAVE_DTO = ConceptSaveDto.builder()
            .uri(CONCEPT_1_URI)
            .name(CONCEPT_1_NAME)
            .description(CONCEPT_1_DESCRIPTION)
            .build();

    public static final ConceptDto CONCEPT_1_DTO = ConceptDto.builder()
            .id(CONCEPT_1_ID)
            .uri(CONCEPT_1_URI)
            .name(CONCEPT_1_NAME)
            .description(CONCEPT_1_DESCRIPTION)
            .build();

    public static final ConceptBriefDto CONCEPT_1_BRIEF_DTO = ConceptBriefDto.builder()
            .id(CONCEPT_1_ID)
            .uri(CONCEPT_1_URI)
            .name(CONCEPT_1_NAME)
            .description(CONCEPT_1_DESCRIPTION)
            .build();

    public static final TableColumnConcept CONCEPT_1 = TableColumnConcept.builder()
            .id(CONCEPT_1_ID)
            .uri(CONCEPT_1_URI)
            .name(CONCEPT_1_NAME)
            .description(CONCEPT_1_DESCRIPTION)
            .created(CONCEPT_1_CREATED)
            .build();

    public static final EntityDto CONCEPT_1_ENTITY_DTO = EntityDto.builder()
            .uri(CONCEPT_1_URI)
            .description(CONCEPT_1_DESCRIPTION)
            .label(CONCEPT_1_NAME)
            .build();

    public static final UUID CONCEPT_2_ID = UUID.fromString("c5cf9914-15c1-4813-af11-eb2a070d59a9");
    public static final String CONCEPT_2_NAME = "FAIR data";
    public static final String CONCEPT_2_URI = "http://www.wikidata.org/entity/Q29032648";
    public static final String CONCEPT_2_DESCRIPTION = "data compliant with the terms of the FAIR Data Principles";
    public static final Instant CONCEPT_2_CREATED = Instant.now();

    public static final ConceptSaveDto CONCEPT_2_SAVE_DTO = ConceptSaveDto.builder()
            .uri(CONCEPT_2_URI)
            .name(CONCEPT_2_NAME)
            .description(CONCEPT_2_DESCRIPTION)
            .build();

    public static final ConceptDto CONCEPT_2_DTO = ConceptDto.builder()
            .id(CONCEPT_2_ID)
            .uri(CONCEPT_2_URI)
            .name(CONCEPT_2_NAME)
            .description(CONCEPT_2_DESCRIPTION)
            .build();

    public static final ConceptBriefDto CONCEPT_2_BRIEF_DTO = ConceptBriefDto.builder()
            .id(CONCEPT_2_ID)
            .uri(CONCEPT_2_URI)
            .name(CONCEPT_2_NAME)
            .description(CONCEPT_2_DESCRIPTION)
            .build();

    public static final TableColumnConcept CONCEPT_2 = TableColumnConcept.builder()
            .id(CONCEPT_2_ID)
            .uri(CONCEPT_2_URI)
            .name(CONCEPT_2_NAME)
            .description(CONCEPT_2_DESCRIPTION)
            .created(CONCEPT_2_CREATED)
            .build();

    public static final UUID UNIT_1_ID = UUID.fromString("1fee60e4-42f8-4883-85a8-e282fddf6a62");
    public static final String UNIT_1_NAME = "millimetre";
    public static final String UNIT_1_URI = "http://www.ontology-of-units-of-measure.org/resource/om-2/millimetre";
    public static final String UNIT_1_DESCRIPTION = "The millimetre is a unit of length defined as 1.0e-3 metre.";
    public static final Instant UNIT_1_CREATED = Instant.ofEpochSecond(1701976282L) /* 2023-12-07 19:11:22 */;

    public static final UnitSaveDto UNIT_1_SAVE_DTO = UnitSaveDto.builder()
            .uri(UNIT_1_URI)
            .name(UNIT_1_NAME)
            .description(UNIT_1_DESCRIPTION)
            .build();

    public static final UnitDto UNIT_1_DTO = UnitDto.builder()
            .id(UNIT_1_ID)
            .uri(UNIT_1_URI)
            .name(UNIT_1_NAME)
            .description(UNIT_1_DESCRIPTION)
            .build();

    public static final UnitBriefDto UNIT_1_BRIEF_DTO = UnitBriefDto.builder()
            .id(UNIT_1_ID)
            .uri(UNIT_1_URI)
            .name(UNIT_1_NAME)
            .description(UNIT_1_DESCRIPTION)
            .build();

    public static final TableColumnUnit UNIT_1 = TableColumnUnit.builder()
            .id(UNIT_1_ID)
            .uri(UNIT_1_URI)
            .name(UNIT_1_NAME)
            .description(UNIT_1_DESCRIPTION)
            .created(UNIT_1_CREATED)
            .build();

    public static final EntityDto UNIT_1_ENTITY_DTO = EntityDto.builder()
            .uri(UNIT_1_URI)
            .description(UNIT_1_DESCRIPTION)
            .label(UNIT_1_NAME)
            .build();

    public static final UUID UNIT_2_ID = UUID.fromString("d88591a9-5171-4b12-8381-bcff1cfe7442");
    public static final String UNIT_2_NAME = "tonne";
    public static final String UNIT_2_URI = "http://www.ontology-of-units-of-measure.org/resource/om-2/tonne";
    public static final String UNIT_2_DESCRIPTION = "The tonne is a unit of mass defined as 1000 kilogram.";
    public static final Instant UNIT_2_CREATED = Instant.ofEpochSecond(1701976462L) /* 2023-12-07 19:14:22 */;

    public static final UnitSaveDto UNIT_2_SAVE_DTO = UnitSaveDto.builder()
            .uri(UNIT_2_URI)
            .name(UNIT_2_NAME)
            .description(UNIT_2_DESCRIPTION)
            .build();

    public static final UnitDto UNIT_2_DTO = UnitDto.builder()
            .id(UNIT_2_ID)
            .uri(UNIT_2_URI)
            .name(UNIT_2_NAME)
            .description(UNIT_2_DESCRIPTION)
            .build();

    public static final UnitBriefDto UNIT_2_BRIEF_DTO = UnitBriefDto.builder()
            .id(UNIT_2_ID)
            .uri(UNIT_2_URI)
            .name(UNIT_2_NAME)
            .description(UNIT_2_DESCRIPTION)
            .build();

    public static final TableColumnUnit UNIT_2 = TableColumnUnit.builder()
            .id(UNIT_2_ID)
            .uri(UNIT_2_URI)
            .name(UNIT_2_NAME)
            .description(UNIT_2_DESCRIPTION)
            .created(UNIT_2_CREATED)
            .build();

    public static final String USER_BROKER_USERNAME = "guest";
    @SuppressWarnings("java:S2068")
    public static final String USER_BROKER_PASSWORD = "guest";

    public static final UUID USER_LOCAL_ADMIN_ID = UUID.fromString("a54dcb2e-a644-4e82-87e7-05a96413983d");
    public static final UUID USER_LOCAL_ADMIN_KEYCLOAK_ID = UUID.fromString("703c2ca0-8fc3-4c03-9bc5-4dae6b211e78");
    public static final String USER_LOCAL_ADMIN_USERNAME = "admin";
    @SuppressWarnings("java:S2068")
    public static final String USER_LOCAL_ADMIN_PASSWORD = "admin";
    public static final String USER_LOCAL_ADMIN_THEME = "dark";
    public static final Boolean USER_LOCAL_ADMIN_IS_INTERNAL = true;
    public static final Boolean USER_LOCAL_ADMIN_ENABLED = true;
    @SuppressWarnings("java:S2068")
    public static final String USER_LOCAL_ADMIN_MARIADB_PASSWORD = "*440BA4FD1A87A0999647DB67C0EE258198B247BA";

    public static final UserDetails USER_LOCAL_ADMIN_DETAILS = UserDetailsDto.builder()
            .id(USER_LOCAL_ADMIN_ID.toString())
            .username(USER_LOCAL_ADMIN_USERNAME)
            .password(USER_LOCAL_ADMIN_PASSWORD)
            .authorities(AUTHORITY_DEFAULT_LOCAL_ADMIN_AUTHORITIES)
            .build();

    public static final User USER_LOCAL = User.builder()
            .id(USER_LOCAL_ADMIN_ID)
            .keycloakId(USER_LOCAL_ADMIN_KEYCLOAK_ID)
            .username(USER_LOCAL_ADMIN_USERNAME)
            .mariadbPassword(USER_LOCAL_ADMIN_MARIADB_PASSWORD)
            .theme(USER_LOCAL_ADMIN_THEME)
            .isInternal(USER_LOCAL_ADMIN_IS_INTERNAL)
            .build();

    public static final Principal USER_LOCAL_ADMIN_PRINCIPAL = new UsernamePasswordAuthenticationToken(USER_LOCAL_ADMIN_DETAILS,
            USER_LOCAL_ADMIN_PASSWORD, USER_LOCAL_ADMIN_DETAILS.getAuthorities());

    public static final UUID USER_1_ID = UUID.fromString("cd5bab0d-7799-4069-85fb-c5d738572a0b");
    public static final UUID USER_1_KEYCLOAK_ID = UUID.fromString("cd5bab0d-7799-4069-85fb-c5d738572a0b");
    public static final String USER_1_USERNAME = "junit1";
    @SuppressWarnings("java:S2068")
    public static final String USER_1_PASSWORD = "junit1";
    @SuppressWarnings("java:S2068")
    public static final String USER_1_DATABASE_PASSWORD = "*440BA4FD1A87A0999647DB67C0EE258198B247BA" /* junit1 */;
    public static final String USER_1_FIRSTNAME = "John";
    public static final String USER_1_LASTNAME = "Doe";
    public static final String USER_1_QUALIFIED_NAME = USER_1_FIRSTNAME + " " + USER_1_LASTNAME + " — @" + USER_1_USERNAME;
    public static final String USER_1_NAME = "John Doe";
    public static final String USER_1_AFFILIATION = "TU Graz";
    public static final String USER_1_ORCID_URL = "https://orcid.org/0000-0003-4216-302X";
    public static final Boolean USER_1_ENABLED = true;
    public static final Boolean USER_1_IS_INTERNAL = false;
    public static final String USER_1_THEME = "light";
    public static final String USER_1_LANGUAGE = "en";
    public static final Instant USER_1_CREATED = Instant.ofEpochSecond(1677399441L) /* 2023-02-26 08:17:21 (UTC) */;

    public static final UpdateUserPasswordDto USER_1_UPDATE_PASSWORD_DTO = UpdateUserPasswordDto.builder()
            .username(USER_1_USERNAME)
            .password(USER_1_PASSWORD)
            .build();

    public static final UserAttributesDto USER_1_ATTRIBUTES_DTO = UserAttributesDto.builder()
            .theme(USER_1_THEME)
            .orcid(USER_1_ORCID_URL)
            .affiliation(USER_1_AFFILIATION)
            .mariadbPassword(USER_1_DATABASE_PASSWORD)
            .language(USER_1_LANGUAGE)
            .build();

    public static final CredentialDto USER_1_KEYCLOAK_CREDENTIAL_1 = CredentialDto.builder()
            .type(CredentialTypeDto.PASSWORD)
            .temporary(false)
            .value(USER_1_PASSWORD)
            .build();

    public static final CredentialDto USER_LOCAL_KEYCLOAK_CREDENTIAL_1 = CredentialDto.builder()
            .type(CredentialTypeDto.PASSWORD)
            .temporary(false)
            .value(USER_LOCAL_ADMIN_PASSWORD)
            .build();

    public static final UserCreateDto USER_1_KEYCLOAK_SIGNUP_REQUEST = UserCreateDto.builder()
            .username(USER_1_USERNAME)
            .enabled(USER_1_ENABLED)
            .credentials(new LinkedList<>(List.of(USER_1_KEYCLOAK_CREDENTIAL_1)))
            .attributes(UserCreateAttributesDto.builder()
                    .ldapId(String.valueOf(USER_1_ID))
                    .build())
            .build();

    public static final UserCreateDto USER_LOCAL_KEYCLOAK_SIGNUP_REQUEST = UserCreateDto.builder()
            .username(USER_LOCAL_ADMIN_USERNAME)
            .enabled(USER_LOCAL_ADMIN_ENABLED)
            .credentials(new LinkedList<>(List.of(USER_LOCAL_KEYCLOAK_CREDENTIAL_1)))
            .groups(new LinkedList<>(List.of("system")))
            .attributes(UserCreateAttributesDto.builder()
                    .ldapId(String.valueOf(USER_LOCAL_ADMIN_ID))
                    .build())
            .build();

    public static final User USER_1 = User.builder()
            .id(USER_1_ID)
            .keycloakId(USER_1_KEYCLOAK_ID)
            .username(USER_1_USERNAME)
            .firstname(USER_1_FIRSTNAME)
            .lastname(USER_1_LASTNAME)
            .affiliation(USER_1_AFFILIATION)
            .orcid(USER_1_ORCID_URL)
            .theme(USER_1_THEME)
            .mariadbPassword(USER_1_DATABASE_PASSWORD)
            .language(USER_1_LANGUAGE)
            .isInternal(USER_1_IS_INTERNAL)
            .build();

    public static final UserDto USER_1_DTO = UserDto.builder()
            .id(USER_1_ID)
            .username(USER_1_USERNAME)
            .firstname(USER_1_FIRSTNAME)
            .lastname(USER_1_LASTNAME)
            .attributes(USER_1_ATTRIBUTES_DTO)
            .name(USER_1_NAME)
            .qualifiedName(USER_1_QUALIFIED_NAME)
            .build();

    public static final CreateUserDto USER_1_CREATE_USER_DTO = CreateUserDto.builder()
            .id(USER_1_KEYCLOAK_ID)
            .ldapId(USER_1_ID)
            .givenName(USER_1_FIRSTNAME)
            .familyName(USER_1_LASTNAME)
            .username(USER_1_USERNAME)
            .build();

    public static final UserUpdateDto USER_1_UPDATE_DTO = UserUpdateDto.builder()
            .firstname(USER_1_FIRSTNAME)
            .lastname(USER_1_LASTNAME)
            .affiliation(USER_1_AFFILIATION)
            .orcid(USER_1_ORCID_URL)
            .theme(USER_1_THEME)
            .language(USER_1_LANGUAGE)
            .build();

    public static final UserPasswordDto USER_1_PASSWORD_DTO = UserPasswordDto.builder()
            .password(USER_1_PASSWORD)
            .build();

    public static final UserBriefDto USER_1_BRIEF_DTO = UserBriefDto.builder()
            .id(USER_1_ID)
            .username(USER_1_USERNAME)
            .firstname(USER_1_FIRSTNAME)
            .lastname(USER_1_LASTNAME)
            .name(USER_1_NAME)
            .qualifiedName(USER_1_QUALIFIED_NAME)
            .orcid(USER_1_ORCID_URL)
            .build();

    public static final UserDetails USER_1_DETAILS = UserDetailsDto.builder()
            .id(USER_1_ID.toString())
            .username(USER_1_USERNAME)
            .password(USER_1_PASSWORD)
            .authorities(AUTHORITY_DEFAULT_RESEARCHER_AUTHORITIES)
            .build();

    public static final Principal USER_1_PRINCIPAL = new UsernamePasswordAuthenticationToken(USER_1_DETAILS,
            USER_1_PASSWORD, USER_1_DETAILS.getAuthorities());

    public static final UUID USER_2_ID = UUID.fromString("eeb9a51b-4cd8-4039-90bf-e24f17372f7c");
    public static final UUID USER_2_KEYCLOAK_ID = UUID.fromString("eeb9a51b-4cd8-4039-90bf-e24f17372f7c");
    public static final String USER_2_USERNAME = "junit2";
    public static final String USER_2_FIRSTNAME = "Jane";
    public static final String USER_2_LASTNAME = "Doe";
    public static final String USER_2_NAME = "Jane Doe";
    public static final String USER_2_AFFILIATION = "TU Wien";
    public static final String USER_2_ORCID_URL = "https://orcid.org/0000-0002-9272-6225";
    @SuppressWarnings("java:S2068")
    public static final String USER_2_PASSWORD = "junit2";
    @SuppressWarnings("java:S2068")
    public static final String USER_2_DATABASE_PASSWORD = "*9AA70A8B0EEFAFCB5BED5BDEF6EE264D5DA915AE" /* junit2 */;
    public static final String USER_2_QUALIFIED_NAME = USER_2_FIRSTNAME + " " + USER_2_LASTNAME + " — @" + USER_2_USERNAME;
    public static final Boolean USER_2_IS_INTERNAL = false;
    public static final String USER_2_THEME = "light";
    public static final String USER_2_LANGUAGE = "de";

    public static final UserAttributesDto USER_2_ATTRIBUTES_DTO = UserAttributesDto.builder()
            .theme(USER_2_THEME)
            .orcid(USER_2_ORCID_URL)
            .affiliation(USER_2_AFFILIATION)
            .mariadbPassword(USER_2_DATABASE_PASSWORD)
            .language(USER_2_LANGUAGE)
            .build();

    public static final User USER_2 = User.builder()
            .id(USER_2_ID)
            .keycloakId(USER_2_KEYCLOAK_ID)
            .username(USER_2_USERNAME)
            .firstname(USER_2_FIRSTNAME)
            .lastname(USER_2_LASTNAME)
            .affiliation(USER_2_AFFILIATION)
            .orcid(USER_2_ORCID_URL)
            .theme(USER_2_THEME)
            .mariadbPassword(USER_2_DATABASE_PASSWORD)
            .language(USER_2_LANGUAGE)
            .isInternal(USER_2_IS_INTERNAL)
            .build();

    public static final UserDto USER_2_DTO = UserDto.builder()
            .id(USER_2_ID)
            .username(USER_2_USERNAME)
            .firstname(USER_2_FIRSTNAME)
            .lastname(USER_2_LASTNAME)
            .name(USER_2_NAME)
            .qualifiedName(USER_2_QUALIFIED_NAME)
            .attributes(USER_2_ATTRIBUTES_DTO)
            .build();

    public static final UserBriefDto USER_2_BRIEF_DTO = UserBriefDto.builder()
            .id(USER_2_ID)
            .username(USER_2_USERNAME)
            .firstname(USER_2_FIRSTNAME)
            .lastname(USER_2_LASTNAME)
            .name(USER_2_NAME)
            .orcid(USER_2_ORCID_URL)
            .qualifiedName(USER_2_QUALIFIED_NAME)
            .build();

    public static final UserDetails USER_2_DETAILS = UserDetailsDto.builder()
            .id(USER_2_ID.toString())
            .username(USER_2_USERNAME)
            .password(USER_2_PASSWORD)
            .authorities(AUTHORITY_DEFAULT_RESEARCHER_AUTHORITIES)
            .build();

    public static final at.tuwien.api.amqp.UserDetailsDto USER_2_DETAILS_DTO = at.tuwien.api.amqp.UserDetailsDto.builder()
            .name(USER_2_USERNAME)
            .tags(new String[]{})
            .build();

    public static final Principal USER_2_PRINCIPAL = new UsernamePasswordAuthenticationToken(USER_2_DETAILS,
            USER_2_PASSWORD, USER_2_DETAILS.getAuthorities());

    public static final UUID USER_3_ID = UUID.fromString("7b080e33-d8db-4276-9d53-47208e657006");
    public static final UUID USER_3_KEYCLOAK_ID = UUID.fromString("b0108bc3-95aa-4a3f-8868-dc301286aeca");
    public static final String USER_3_USERNAME = "junit3";
    public static final String USER_3_FIRSTNAME = "System";
    public static final String USER_3_LASTNAME = "System";
    public static final String USER_3_NAME = "System System";
    public static final String USER_3_AFFILIATION = "TU Wien";
    public static final String USER_3_ORCID_URL = null;
    public static final String USER_3_ORCID_UNCOMPRESSED = null;
    @SuppressWarnings("java:S2068")
    public static final String USER_3_PASSWORD = "password";
    @SuppressWarnings("java:S2068")
    public static final String USER_3_DATABASE_PASSWORD = "*D65FCA043964B63E849DD6334699ECB065905DA4" /* junit3 */;
    public static final String USER_3_QUALIFIED_NAME = USER_3_FIRSTNAME + " " + USER_3_LASTNAME + " — @" + USER_3_USERNAME;
    public static final Boolean USER_3_IS_INTERNAL = false;
    public static final String USER_3_THEME = "light";

    public static final UserAttributesDto USER_3_ATTRIBUTES_DTO = UserAttributesDto.builder()
            .theme(USER_3_THEME)
            .orcid(USER_3_ORCID_UNCOMPRESSED)
            .affiliation(USER_3_AFFILIATION)
            .mariadbPassword(USER_3_DATABASE_PASSWORD)
            .build();

    public static final User USER_3 = User.builder()
            .id(USER_3_ID)
            .keycloakId(USER_3_KEYCLOAK_ID)
            .username(USER_3_USERNAME)
            .firstname(USER_3_FIRSTNAME)
            .lastname(USER_3_LASTNAME)
            .affiliation(USER_3_AFFILIATION)
            .orcid(USER_3_ORCID_URL)
            .theme(USER_3_THEME)
            .mariadbPassword(USER_3_DATABASE_PASSWORD)
            .isInternal(USER_3_IS_INTERNAL)
            .build();

    public static final UserDto USER_3_DTO = UserDto.builder()
            .id(USER_3_ID)
            .username(USER_3_USERNAME)
            .firstname(USER_3_FIRSTNAME)
            .lastname(USER_3_LASTNAME)
            .name(USER_3_NAME)
            .qualifiedName(USER_3_QUALIFIED_NAME)
            .attributes(USER_3_ATTRIBUTES_DTO)
            .build();

    public static final UserBriefDto USER_3_BRIEF_DTO = UserBriefDto.builder()
            .id(USER_3_ID)
            .username(USER_3_USERNAME)
            .firstname(USER_3_FIRSTNAME)
            .lastname(USER_3_LASTNAME)
            .name(USER_3_NAME)
            .qualifiedName(USER_3_QUALIFIED_NAME)
            .build();

    public static final UserDetails USER_3_DETAILS = UserDetailsDto.builder()
            .id(USER_3_ID.toString())
            .username(USER_3_USERNAME)
            .password(USER_3_PASSWORD)
            .authorities(AUTHORITY_DEFAULT_RESEARCHER_AUTHORITIES)
            .build();

    public static final Principal USER_3_PRINCIPAL = new UsernamePasswordAuthenticationToken(USER_3_DETAILS,
            USER_3_PASSWORD, USER_3_DETAILS.getAuthorities());

    public static final at.tuwien.api.amqp.UserDetailsDto USER_3_DETAILS_DTO = at.tuwien.api.amqp.UserDetailsDto.builder()
            .name(USER_3_USERNAME)
            .tags(new String[]{})
            .build();

    public static final UUID USER_4_ID = UUID.fromString("791d58c5-bfab-4520-b4fc-b44d4ab9feb0");
    public static final UUID USER_4_KEYCLOAK_ID = UUID.fromString("25040ad3-6d57-4052-b357-6b4c8a6e7f4d");
    public static final String USER_4_USERNAME = "junit4";
    public static final String USER_4_FIRSTNAME = "JUnit";
    public static final String USER_4_LASTNAME = "4";
    public static final String USER_4_NAME = "JUnit 4";
    public static final String USER_4_AFFILIATION = "TU Wien";
    public static final String USER_4_ORCID_URL = null;
    @SuppressWarnings("java:S2068")
    public static final String USER_4_PASSWORD = "junit4";
    @SuppressWarnings("java:S2068")
    public static final String USER_4_DATABASE_PASSWORD = "*C20EF5C6875857DEFA9BE6E9B62DD76AAAE51882" /* junit4 */;
    public static final String USER_4_QUALIFIED_NAME = USER_4_FIRSTNAME + " " + USER_4_LASTNAME + " — @" + USER_4_USERNAME;
    public static final Boolean USER_4_IS_INTERNAL = false;
    public static final String USER_4_THEME = "light";

    public static final UserAttributesDto USER_4_ATTRIBUTES_DTO = UserAttributesDto.builder()
            .theme(USER_4_THEME)
            .orcid(USER_4_ORCID_URL)
            .affiliation(USER_4_AFFILIATION)
            .mariadbPassword(USER_4_DATABASE_PASSWORD)
            .build();

    public static final User USER_4 = User.builder()
            .id(USER_4_ID)
            .keycloakId(USER_4_KEYCLOAK_ID)
            .username(USER_4_USERNAME)
            .firstname(USER_4_FIRSTNAME)
            .lastname(USER_4_LASTNAME)
            .affiliation(USER_4_AFFILIATION)
            .orcid(USER_4_ORCID_URL)
            .theme(USER_4_THEME)
            .mariadbPassword(USER_4_DATABASE_PASSWORD)
            .isInternal(USER_4_IS_INTERNAL)
            .build();

    public static final UserDto USER_4_DTO = UserDto.builder()
            .id(USER_4_ID)
            .username(USER_4_USERNAME)
            .firstname(USER_4_FIRSTNAME)
            .lastname(USER_4_LASTNAME)
            .name(USER_4_NAME)
            .attributes(USER_4_ATTRIBUTES_DTO)
            .qualifiedName(USER_4_QUALIFIED_NAME)
            .build();

    public static final UserBriefDto USER_4_BRIEF_DTO = UserBriefDto.builder()
            .id(USER_4_ID)
            .username(USER_4_USERNAME)
            .firstname(USER_4_FIRSTNAME)
            .lastname(USER_4_LASTNAME)
            .name(USER_4_NAME)
            .qualifiedName(USER_4_QUALIFIED_NAME)
            .build();

    public static final UserDetails USER_4_DETAILS = UserDetailsDto.builder()
            .id(USER_4_ID.toString())
            .username(USER_4_USERNAME)
            .password(USER_4_PASSWORD)
            .authorities(new LinkedList<>())
            .build();

    public static final Principal USER_4_PRINCIPAL = new UsernamePasswordAuthenticationToken(USER_4_DETAILS,
            USER_4_PASSWORD, USER_4_DETAILS.getAuthorities());

    public static final UUID USER_5_ID = UUID.fromString("28ff851d-d7bc-4422-959c-edd7a5b15630");
    public static final UUID USER_5_KEYCLOAK_ID = UUID.fromString("28ff851d-d7bc-4422-959c-edd7a5b15630");
    public static final String USER_5_USERNAME = "nobody";
    public static final String USER_5_FIRSTNAME = "No";
    public static final String USER_5_LASTNAME = "Body";
    public static final String USER_5_NAME = "No Body";
    public static final String USER_5_AFFILIATION = "TU Wien";
    @SuppressWarnings("java:S2068")
    public static final String USER_5_PASSWORD = "junit5";
    @SuppressWarnings("java:S2068")
    public static final String USER_5_DATABASE_PASSWORD = "*C20EF5C6875857DEFA9BE6E9B62DD76AAAE51882" /* junit5 */;
    public static final String USER_5_QUALIFIED_NAME = USER_5_FIRSTNAME + " " + USER_5_LASTNAME + " — @" + USER_5_USERNAME;
    public static final Boolean USER_5_IS_INTERNAL = false;
    public static final String USER_5_THEME = "dark";

    public static final UserAttributesDto USER_5_ATTRIBUTES_DTO = UserAttributesDto.builder()
            .theme(USER_5_THEME)
            .affiliation(USER_5_AFFILIATION)
            .mariadbPassword(USER_5_DATABASE_PASSWORD)
            .build();

    public static final UserDto USER_5_DTO = UserDto.builder()
            .id(USER_5_ID)
            .username(USER_5_USERNAME)
            .firstname(USER_5_FIRSTNAME)
            .lastname(USER_5_LASTNAME)
            .name(USER_5_NAME)
            .qualifiedName(USER_5_QUALIFIED_NAME)
            .attributes(USER_5_ATTRIBUTES_DTO)
            .build();

    public static final UserBriefDto USER_5_BRIEF_DTO = UserBriefDto.builder()
            .id(USER_5_ID)
            .username(USER_5_USERNAME)
            .firstname(USER_5_FIRSTNAME)
            .lastname(USER_5_LASTNAME)
            .qualifiedName(USER_5_QUALIFIED_NAME)
            .build();

    public static final UserDetails USER_5_DETAILS = UserDetailsDto.builder()
            .id(USER_5_ID.toString())
            .username(USER_5_USERNAME)
            .password(USER_5_PASSWORD)
            .authorities(AUTHORITY_DEFAULT_DEVELOPER_AUTHORITIES)
            .build();

    public static final Principal USER_5_PRINCIPAL = new UsernamePasswordAuthenticationToken(USER_5_DETAILS,
            USER_5_PASSWORD, USER_5_DETAILS.getAuthorities());

    public static final User USER_5 = User.builder()
            .id(USER_5_ID)
            .keycloakId(USER_5_KEYCLOAK_ID)
            .username(USER_5_USERNAME)
            .firstname(USER_5_FIRSTNAME)
            .lastname(USER_5_LASTNAME)
            .affiliation(USER_5_AFFILIATION)
            .theme(USER_5_THEME)
            .mariadbPassword(USER_5_DATABASE_PASSWORD)
            .isInternal(USER_5_IS_INTERNAL)
            .build();

    public static final UUID USER_6_ID = UUID.fromString("28ff851d-d7bc-4422-959c-edd7a5b15630");
    public static final String USER_6_USERNAME = "system";
    public static final String USER_6_FIRSTNAME = "System";
    public static final String USER_6_LASTNAME = "System";
    public static final String USER_6_NAME = "System System";
    public static final String USER_6_AFFILIATION = "TU Wien";
    public static final String USER_6_ORCID = null;
    @SuppressWarnings("java:S2068")
    public static final String USER_6_PASSWORD = "junit5";
    @SuppressWarnings("java:S2068")
    public static final String USER_6_DATABASE_PASSWORD = "*C20EF5C6875857DEFA9BE6E9B62DD76AAAE51882" /* junit5 */;
    public static final Boolean USER_6_VERIFIED = true;
    public static final Boolean USER_6_ENABLED = true;
    public static final Boolean USER_6_IS_INTERNAL = false;
    public static final Boolean USER_6_THEME_DARK = false;
    public static final Instant USER_6_CREATED = Instant.ofEpochSecond(1677399592L) /* 2023-02-26 08:19:52 (UTC) */;
    public static final UUID USER_6_REALM_ID = REALM_DBREPO_ID;

    public static final UserDto USER_6_DTO = UserDto.builder()
            .id(USER_6_ID)
            .username(USER_6_USERNAME)
            .firstname(USER_6_FIRSTNAME)
            .lastname(USER_6_LASTNAME)
            .build();

    public static final UserDetails USER_6_DETAILS = UserDetailsDto.builder()
            .id(USER_6_ID.toString())
            .username(USER_6_USERNAME)
            .password(USER_6_PASSWORD)
            .authorities(AUTHORITY_DEFAULT_RESEARCHER_AUTHORITIES)
            .build();

    public static final Principal USER_6_PRINCIPAL = new UsernamePasswordAuthenticationToken(USER_6_DETAILS,
            USER_6_PASSWORD, USER_6_DETAILS.getAuthorities());

    public static final UUID IMAGE_1_ID = UUID.fromString("e5449ade-acc1-4ba4-8858-e3496cdecd9c");
    public static final String IMAGE_1_REGISTRY = "docker.io";
    public static final String IMAGE_1_NAME = "mariadb";
    public static final String IMAGE_1_VERSION = "11.1.3";
    public static final String IMAGE_1_DIALECT = "org.hibernate.dialect.MariaDBDialect";
    public static final String IMAGE_1_DRIVER = "org.mariadb.jdbc.Driver";
    public static final String IMAGE_1_JDBC = "mariadb";
    public static final Integer IMAGE_1_PORT = 3306;
    public static final Boolean IMAGE_1_IS_DEFAULT = true;

    public static final ImageCreateDto IMAGE_1_CREATE_DTO = ImageCreateDto.builder()
            .registry(IMAGE_1_REGISTRY)
            .name(IMAGE_1_NAME)
            .version(IMAGE_1_VERSION)
            .dialect(IMAGE_1_DIALECT)
            .jdbcMethod(IMAGE_1_JDBC)
            .driverClass(IMAGE_1_DRIVER)
            .defaultPort(IMAGE_1_PORT)
            .build();

    public static final ImageChangeDto IMAGE_1_CHANGE_DTO = ImageChangeDto.builder()
            .registry(IMAGE_1_REGISTRY)
            .dialect(IMAGE_1_DIALECT)
            .jdbcMethod(IMAGE_1_JDBC)
            .driverClass(IMAGE_1_DRIVER)
            .defaultPort(IMAGE_1_PORT)
            .build();

    public static final ContainerImage IMAGE_1 = ContainerImage.builder()
            .id(IMAGE_1_ID)
            .name(IMAGE_1_NAME)
            .registry(IMAGE_1_REGISTRY)
            .version(IMAGE_1_VERSION)
            .dialect(IMAGE_1_DIALECT)
            .jdbcMethod(IMAGE_1_JDBC)
            .driverClass(IMAGE_1_DRIVER)
            .defaultPort(IMAGE_1_PORT)
            .isDefault(IMAGE_1_IS_DEFAULT)
            .operators(new LinkedList<>()) /* IMAGE_1_OPERATORS */
            .build();

    public static final ImageDto IMAGE_1_DTO = ImageDto.builder()
            .id(IMAGE_1_ID)
            .name(IMAGE_1_NAME)
            .version(IMAGE_1_VERSION)
            .isDefault(IMAGE_1_IS_DEFAULT)
            .jdbcMethod(IMAGE_1_JDBC)
            .operators(null) /* IMAGE_1_OPERATORS_DTO */
            .build();

    public static final ImageBriefDto IMAGE_1_BRIEF_DTO = ImageBriefDto.builder()
            .id(IMAGE_1_ID)
            .name(IMAGE_1_NAME)
            .version(IMAGE_1_VERSION)
            .isDefault(IMAGE_1_IS_DEFAULT)
            .build();

    public static final UUID IMAGE_1_OPERATORS_1_ID = UUID.fromString("42a56348-38bd-4aba-b0f2-ac813d5d2da1");
    public static final String IMAGE_1_OPERATORS_1_DISPLAY_NAME = "XOR";
    public static final String IMAGE_1_OPERATORS_1_VALUE = "XOR";
    public static final String IMAGE_1_OPERATORS_1_DOCUMENTATION = "https://mariadb.com/kb/en/xor/";

    public static final UUID IMAGE_1_OPERATORS_2_ID = UUID.fromString("42a56348-38bd-4aba-b0f2-ac813d5d2da2");
    public static final String IMAGE_1_OPERATORS_2_DISPLAY_NAME = "=";
    public static final String IMAGE_1_OPERATORS_2_VALUE = "=";
    public static final String IMAGE_1_OPERATORS_2_DOCUMENTATION = "https://mariadb.com/kb/en/equal/";

    public static final List<Operator> IMAGE_1_OPERATORS = new LinkedList<>(List.of(
            Operator.builder()
                    .id(IMAGE_1_OPERATORS_1_ID)
                    .image(IMAGE_1)
                    .displayName(IMAGE_1_OPERATORS_1_DISPLAY_NAME)
                    .value(IMAGE_1_OPERATORS_1_VALUE)
                    .documentation(IMAGE_1_OPERATORS_1_DOCUMENTATION)
                    .build(),
            Operator.builder()
                    .id(IMAGE_1_OPERATORS_2_ID)
                    .image(IMAGE_1)
                    .displayName(IMAGE_1_OPERATORS_2_DISPLAY_NAME)
                    .value(IMAGE_1_OPERATORS_2_VALUE)
                    .documentation(IMAGE_1_OPERATORS_2_DOCUMENTATION)
                    .build()));

    public static final List<OperatorDto> IMAGE_1_OPERATORS_DTO = new LinkedList<>(List.of(
            OperatorDto.builder()
                    .id(IMAGE_1_OPERATORS_1_ID)
                    .displayName(IMAGE_1_OPERATORS_1_DISPLAY_NAME)
                    .value(IMAGE_1_OPERATORS_1_VALUE)
                    .documentation(IMAGE_1_OPERATORS_1_DOCUMENTATION)
                    .build(),
            OperatorDto.builder()
                    .id(IMAGE_1_OPERATORS_2_ID)
                    .displayName(IMAGE_1_OPERATORS_2_DISPLAY_NAME)
                    .value(IMAGE_1_OPERATORS_2_VALUE)
                    .documentation(IMAGE_1_OPERATORS_2_DOCUMENTATION)
                    .build()));

    public static final UUID CONTAINER_1_ID = UUID.fromString("7ddb7e87-b965-43a2-9a24-4fa406d998f4");
    public static final String CONTAINER_1_NAME = "u01";
    public static final String CONTAINER_1_INTERNALNAME = "dbrepo-userdb-u01";
    public static final String CONTAINER_1_UI_HOST = "localhost";
    public static final Integer CONTAINER_1_UI_PORT = 3306;
    public static final String CONTAINER_1_UI_ADDITIONAL_FLAGS = "?sslMode=disable";
    public static final Integer CONTAINER_1_QUOTA = 4;
    public static final Integer CONTAINER_1_COUNT = 3;
    public static final String CONTAINER_1_HOST = "localhost";
    public static final Integer CONTAINER_1_PORT = 3308;
    public static final String CONTAINER_1_PRIVILEGED_USERNAME = "root";
    @SuppressWarnings("java:S2068")
    public static final String CONTAINER_1_PRIVILEGED_PASSWORD = "dbrepo";
    public static final Instant CONTAINER_1_CREATED = Instant.ofEpochSecond(1677399629L) /* 2023-02-26 08:20:29 (UTC) */;

    public static final Container CONTAINER_1 = Container.builder()
            .id(CONTAINER_1_ID)
            .name(CONTAINER_1_NAME)
            .internalName(CONTAINER_1_INTERNALNAME)
            .image(IMAGE_1)
            .created(CONTAINER_1_CREATED)
            .host(CONTAINER_1_HOST)
            .port(CONTAINER_1_PORT)
            .uiHost(CONTAINER_1_UI_HOST)
            .uiPort(CONTAINER_1_UI_PORT)
            .quota(CONTAINER_1_QUOTA)
            .uiAdditionalFlags(CONTAINER_1_UI_ADDITIONAL_FLAGS)
            .privilegedUsername(CONTAINER_1_PRIVILEGED_USERNAME)
            .privilegedPassword(CONTAINER_1_PRIVILEGED_PASSWORD)
            .databases(null) /* DATABASE_1, DATABASE_2, DATABASE_3 */
            .build();

    public static final ContainerDto CONTAINER_1_DTO = ContainerDto.builder()
            .id(CONTAINER_1_ID)
            .name(CONTAINER_1_NAME)
            .internalName(CONTAINER_1_INTERNALNAME)
            .image(IMAGE_1_DTO)
            .host(CONTAINER_1_HOST)
            .port(CONTAINER_1_PORT)
            .build();

    public static final ContainerBriefDto CONTAINER_1_BRIEF_DTO = ContainerBriefDto.builder()
            .id(CONTAINER_1_ID)
            .name(CONTAINER_1_NAME)
            .internalName(CONTAINER_1_INTERNALNAME)
            .quota(CONTAINER_1_QUOTA)
            .count(CONTAINER_1_COUNT)
            .image(IMAGE_1_BRIEF_DTO)
            .build();

    public static final ContainerDto CONTAINER_1_PRIVILEGED_DTO = ContainerDto.builder()
            .id(CONTAINER_1_ID)
            .name(CONTAINER_1_NAME)
            .internalName(CONTAINER_1_INTERNALNAME)
            .image(IMAGE_1_DTO)
            .host(CONTAINER_1_HOST)
            .port(CONTAINER_1_PORT)
            .lastRetrieved(Instant.now())
            .username(CONTAINER_1_PRIVILEGED_USERNAME)
            .password(CONTAINER_1_PRIVILEGED_PASSWORD)
            .build();

    public static final UUID CONTAINER_2_ID = UUID.fromString("c2ec601e-2bfb-4be8-8891-0cb804a08d4a");
    public static final ContainerImage CONTAINER_2_IMAGE = IMAGE_1;
    public static final ImageDto CONTAINER_2_IMAGE_DTO = IMAGE_1_DTO;
    public static final String CONTAINER_2_NAME = "u02";
    public static final String CONTAINER_2_INTERNALNAME = "dbrepo-userdb-u02";
    public static final String CONTAINER_2_HOST = "localhost";
    public static final Integer CONTAINER_2_PORT = 3309;
    public static final Integer CONTAINER_2_QUOTA = 3;
    public static final Integer CONTAINER_2_COUNT = 3;
    public static final String CONTAINER_2_PRIVILEGED_USERNAME = "root";
    @SuppressWarnings("java:S2068")
    public static final String CONTAINER_2_PRIVILEGED_PASSWORD = "dbrepo";
    public static final Instant CONTAINER_2_CREATED = Instant.ofEpochSecond(1677399655L) /* 2023-02-26 08:20:55 (UTC) */;

    public static final Container CONTAINER_2 = Container.builder()
            .id(CONTAINER_2_ID)
            .name(CONTAINER_2_NAME)
            .internalName(CONTAINER_2_INTERNALNAME)
            .image(CONTAINER_2_IMAGE)
            .created(CONTAINER_2_CREATED)
            .host(CONTAINER_2_HOST)
            .port(CONTAINER_2_PORT)
            .quota(CONTAINER_2_QUOTA)
            .databases(new LinkedList<>(List.of()))
            .privilegedUsername(CONTAINER_2_PRIVILEGED_USERNAME)
            .privilegedPassword(CONTAINER_2_PRIVILEGED_PASSWORD)
            .build();

    public static final ContainerDto CONTAINER_2_DTO = ContainerDto.builder()
            .id(CONTAINER_2_ID)
            .name(CONTAINER_2_NAME)
            .internalName(CONTAINER_2_INTERNALNAME)
            .image(CONTAINER_2_IMAGE_DTO)
            .host(CONTAINER_2_HOST)
            .port(CONTAINER_2_PORT)
            .build();

    public static final ContainerBriefDto CONTAINER_2_DTO_BRIEF = ContainerBriefDto.builder()
            .id(CONTAINER_2_ID)
            .name(CONTAINER_2_NAME)
            .internalName(CONTAINER_2_INTERNALNAME)
            .quota(CONTAINER_2_QUOTA)
            .build();

    public static final ContainerDto CONTAINER_2_PRIVILEGED_DTO = ContainerDto.builder()
            .id(CONTAINER_2_ID)
            .name(CONTAINER_2_NAME)
            .internalName(CONTAINER_2_INTERNALNAME)
            .image(CONTAINER_2_IMAGE_DTO)
            .host(CONTAINER_2_HOST)
            .port(CONTAINER_2_PORT)
            .lastRetrieved(Instant.now())
            .username(CONTAINER_2_PRIVILEGED_USERNAME)
            .password(CONTAINER_2_PRIVILEGED_PASSWORD)
            .build();

    public static final UUID CONTAINER_3_ID = UUID.fromString("1731c7d2-8bd1-4392-85bc-18a3be99e01d");
    public static final ContainerImage CONTAINER_3_IMAGE = IMAGE_1;
    public static final String CONTAINER_3_NAME = "u03";
    public static final String CONTAINER_3_INTERNALNAME = "dbrepo-userdb-u03";
    public static final String CONTAINER_3_HOST = "localhost";
    public static final Integer CONTAINER_3_PORT = 3310;
    public static final Integer CONTAINER_3_QUOTA = 20;
    public static final String CONTAINER_3_PRIVILEGED_USERNAME = "root";
    @SuppressWarnings("java:S2068")
    public static final String CONTAINER_3_PRIVILEGED_PASSWORD = "dbrepo";
    public static final Instant CONTAINER_3_CREATED = Instant.ofEpochSecond(1677399672L) /* 2023-02-26 08:21:12 (UTC) */;

    public static final Container CONTAINER_3 = Container.builder()
            .id(CONTAINER_3_ID)
            .name(CONTAINER_3_NAME)
            .internalName(CONTAINER_3_INTERNALNAME)
            .image(CONTAINER_3_IMAGE)
            .created(CONTAINER_3_CREATED)
            .host(CONTAINER_3_HOST)
            .port(CONTAINER_3_PORT)
            .quota(CONTAINER_3_QUOTA)
            .databases(new LinkedList<>(List.of()))
            .privilegedUsername(CONTAINER_3_PRIVILEGED_USERNAME)
            .privilegedPassword(CONTAINER_3_PRIVILEGED_PASSWORD)
            .build();

    public static final UUID CONTAINER_4_ID = UUID.fromString("67aee75c-791c-410b-abbb-175c11ddd252");
    public static final ContainerImage CONTAINER_4_IMAGE = IMAGE_1;
    public static final String CONTAINER_4_NAME = "u04";
    public static final String CONTAINER_4_INTERNALNAME = "dbrepo-userdb-u04";
    public static final String CONTAINER_4_HOST = "localhost";
    public static final Integer CONTAINER_4_PORT = 3311;
    public static final Integer CONTAINER_4_QUOTA = 0;
    public static final String CONTAINER_4_PRIVILEGED_USERNAME = "root";
    @SuppressWarnings("java:S2068")
    public static final String CONTAINER_4_PRIVILEGED_PASSWORD = "dbrepo";
    public static final Instant CONTAINER_4_CREATED = Instant.ofEpochSecond(1677399688L) /* 2023-02-26 08:21:28 (UTC) */;

    public static final Container CONTAINER_4 = Container.builder()
            .id(CONTAINER_4_ID)
            .name(CONTAINER_4_NAME)
            .internalName(CONTAINER_4_INTERNALNAME)
            .image(CONTAINER_4_IMAGE)
            .created(CONTAINER_4_CREATED)
            .host(CONTAINER_4_HOST)
            .port(CONTAINER_4_PORT)
            .quota(CONTAINER_4_QUOTA)
            .privilegedUsername(CONTAINER_4_PRIVILEGED_USERNAME)
            .privilegedPassword(CONTAINER_4_PRIVILEGED_PASSWORD)
            .databases(null) /* DATABASE_4 */
            .build();

    public static final String EXCHANGE_DBREPO_NAME = "dbrepo";
    public static final Boolean EXCHANGE_DBREPO_AUTO_DELETE = true;
    public static final Boolean EXCHANGE_DBREPO_DURABLE = true;
    public static final Boolean EXCHANGE_DBREPO_INTERNAL = true;
    public static final String EXCHANGE_DBREPO_TYPE = "topic";
    public static final String EXCHANGE_DBREPO_VHOST = "dbrepo";

    public static final ExchangeDto EXCHANGE_DBREPO_DTO = ExchangeDto.builder()
            .autoDelete(EXCHANGE_DBREPO_AUTO_DELETE)
            .type(EXCHANGE_DBREPO_TYPE)
            .name(EXCHANGE_DBREPO_NAME)
            .durable(EXCHANGE_DBREPO_DURABLE)
            .vhost(EXCHANGE_DBREPO_VHOST)
            .internal(EXCHANGE_DBREPO_INTERNAL)
            .build();

    public static final UUID DATABASE_1_ID = UUID.fromString("b3bcb5bf-4f88-40e2-9726-9b0d2ee2b425");
    public static final String DATABASE_1_NAME = "Weather";
    public static final String DATABASE_1_DESCRIPTION = "Weather in Australia";
    public static final String DATABASE_1_INTERNALNAME = "weather";
    public static final Boolean DATABASE_1_PUBLIC = false;
    public static final Boolean DATABASE_1_SCHEMA_PUBLIC = false;
    public static final String DATABASE_1_EXCHANGE = "dbrepo";
    public static final Instant DATABASE_1_CREATED = Instant.ofEpochSecond(1677399741L) /* 2023-02-26 08:22:21 (UTC) */;
    public static final Instant DATABASE_1_LAST_MODIFIED = Instant.ofEpochSecond(1677399741L) /* 2023-02-26 08:22:21 (UTC) */;
    public static final UUID DATABASE_1_CREATED_BY = USER_1_ID;

    public static final CreateDatabaseDto DATABASE_1_CREATE = CreateDatabaseDto.builder()
            .name(DATABASE_1_NAME)
            .isPublic(DATABASE_1_PUBLIC)
            .cid(CONTAINER_1_ID)
            .build();

    public static final at.tuwien.api.database.internal.CreateDatabaseDto DATABASE_1_CREATE_INTERNAL = at.tuwien.api.database.internal.CreateDatabaseDto.builder()
            .internalName(DATABASE_1_INTERNALNAME)
            .containerId(CONTAINER_1_ID)
            .username(USER_1_USERNAME)
            .password(USER_1_PASSWORD)
            .userId(USER_1_ID)
            .privilegedUsername(CONTAINER_1_PRIVILEGED_USERNAME)
            .privilegedPassword(CONTAINER_1_PRIVILEGED_PASSWORD)
            .build();

    public static final UUID DATABASE_2_ID = UUID.fromString("dd9dfee2-9fbd-46b0-92d5-98f0f8866ffe");
    public static final String DATABASE_2_NAME = "Zoo";
    public static final String DATABASE_2_DESCRIPTION = "Zoo data";
    public static final String DATABASE_2_INTERNALNAME = "zoo";
    public static final Boolean DATABASE_2_PUBLIC = false;
    public static final Boolean DATABASE_2_SCHEMA_PUBLIC = true;
    public static final String DATABASE_2_EXCHANGE = "dbrepo";
    public static final Instant DATABASE_2_CREATED = Instant.ofEpochSecond(1677399772L) /* 2023-02-26 08:22:52 (UTC) */;
    public static final Instant DATABASE_2_LAST_MODIFIED = Instant.ofEpochSecond(1677399772L) /* 2023-02-26 08:22:52 (UTC) */;
    public static final UUID DATABASE_2_OWNER = USER_2_ID;
    public static final UUID DATABASE_2_CREATOR = USER_2_ID;

    public static final CreateDatabaseDto DATABASE_2_CREATE = CreateDatabaseDto.builder()
            .name(DATABASE_2_NAME)
            .isPublic(DATABASE_2_PUBLIC)
            .cid(CONTAINER_1_ID)
            .build();

    public static final UUID DATABASE_3_ID = UUID.fromString("9d8cb9a9-9468-4801-a2e0-2dac8bc67c31");
    public static final String DATABASE_3_NAME = "Musicology";
    public static final String DATABASE_3_DESCRIPTION = "Musicology data";
    public static final String DATABASE_3_INTERNALNAME = "musicology";
    public static final Boolean DATABASE_3_PUBLIC = true;
    public static final Boolean DATABASE_3_SCHEMA_PUBLIC = false;
    public static final String DATABASE_3_EXCHANGE = "dbrepo";
    public static final Instant DATABASE_3_CREATED = Instant.ofEpochSecond(1677399792L) /* 2023-02-26 08:23:12 (UTC) */;
    public static final Instant DATABASE_3_LAST_MODIFIED = Instant.ofEpochSecond(1677399792L) /* 2023-02-26 08:23:12 (UTC) */;
    public static final UUID DATABASE_3_OWNER = USER_3_ID;

    public static final DatabaseDto DATABASE_3_DTO = DatabaseDto.builder()
            .id(DATABASE_3_ID)
            .isPublic(DATABASE_3_PUBLIC)
            .isSchemaPublic(DATABASE_3_SCHEMA_PUBLIC)
            .name(DATABASE_3_NAME)
            .internalName(DATABASE_3_INTERNALNAME)
            .owner(USER_3_BRIEF_DTO)
            .container(CONTAINER_1_DTO)
            .exchangeName(DATABASE_3_EXCHANGE)
            .tables(new LinkedList<>()) /* TABLE_8_DTO */
            .views(new LinkedList<>()) /* VIEW_5_DTO */
            .identifiers(new LinkedList<>()) /* IDENTIFIER_6_DTO */
            .build();

    public static final DatabaseDto DATABASE_3_PRIVILEGED_DTO = DatabaseDto.builder()
            .id(DATABASE_3_ID)
            .isPublic(DATABASE_3_PUBLIC)
            .isSchemaPublic(DATABASE_3_SCHEMA_PUBLIC)
            .name(DATABASE_3_NAME)
            .internalName(DATABASE_3_INTERNALNAME)
            .owner(USER_3_BRIEF_DTO)
            .container(CONTAINER_1_PRIVILEGED_DTO)
            .exchangeName(DATABASE_3_EXCHANGE)
            .tables(new LinkedList<>()) /* TABLE_8_DTO */
            .views(new LinkedList<>()) /* VIEW_5_DTO */
            .identifiers(new LinkedList<>()) /* IDENTIFIER_6_DTO */
            .lastRetrieved(Instant.now())
            .build();

    public static final DatabaseBriefDto DATABASE_3_PRIVILEGED_BRIEF_DTO = DatabaseBriefDto.builder()
            .id(DATABASE_3_ID)
            .isPublic(DATABASE_3_PUBLIC)
            .isSchemaPublic(DATABASE_3_SCHEMA_PUBLIC)
            .name(DATABASE_3_NAME)
            .internalName(DATABASE_3_INTERNALNAME)
            .ownerId(USER_3_ID)
            .identifiers(new LinkedList<>()) /* IDENTIFIER_6_DTO */
            .build();

    public static final DatabaseBriefDto DATABASE_3_BRIEF_DTO = DatabaseBriefDto.builder()
            .id(DATABASE_3_ID)
            .isPublic(DATABASE_3_PUBLIC)
            .isSchemaPublic(DATABASE_3_SCHEMA_PUBLIC)
            .name(DATABASE_3_NAME)
            .internalName(DATABASE_3_INTERNALNAME)
            .ownerId(USER_3_ID)
            .identifiers(new LinkedList<>())
            .build();

    public static final CreateDatabaseDto DATABASE_3_CREATE = CreateDatabaseDto.builder()
            .name(DATABASE_3_NAME)
            .isPublic(DATABASE_3_PUBLIC)
            .cid(CONTAINER_1_ID)
            .build();

    public static final UUID DATABASE_4_ID = UUID.fromString("c503d7f3-5952-4d97-b26a-da86bea4c20d");
    public static final String DATABASE_4_NAME = "Weather AT";
    public static final String DATABASE_4_DESCRIPTION = "Weather data";
    public static final Boolean DATABASE_4_PUBLIC = true;
    public static final Boolean DATABASE_4_SCHEMA_PUBLIC = true;
    public static final String DATABASE_4_INTERNALNAME = "weather_at";
    public static final String DATABASE_4_EXCHANGE = "dbrepo";
    public static final Instant DATABASE_4_CREATED = Instant.ofEpochSecond(1677399813L) /* 2023-02-26 08:23:33 (UTC) */;
    public static final Instant DATABASE_4_LAST_MODIFIED = Instant.ofEpochSecond(1677399813L) /* 2023-02-26 08:23:33 (UTC) */;
    public static final UUID DATABASE_4_OWNER = USER_4_ID;
    public static final UUID DATABASE_4_CREATOR = USER_4_ID;

    public static final DatabaseBriefDto DATABASE_4_BRIEF_DTO = DatabaseBriefDto.builder()
            .id(DATABASE_4_ID)
            .isPublic(DATABASE_4_PUBLIC)
            .isSchemaPublic(DATABASE_4_SCHEMA_PUBLIC)
            .name(DATABASE_4_NAME)
            .description(DATABASE_4_DESCRIPTION)
            .internalName(DATABASE_4_INTERNALNAME)
            .ownerId(USER_4_ID)
            .identifiers(new LinkedList<>())
            .build();

    public static final DatabaseDto DATABASE_4_DTO = DatabaseDto.builder()
            .id(DATABASE_4_ID)
            .isPublic(DATABASE_4_PUBLIC)
            .isSchemaPublic(DATABASE_4_SCHEMA_PUBLIC)
            .name(DATABASE_4_NAME)
            .container(CONTAINER_2_DTO)
            .description(DATABASE_4_DESCRIPTION)
            .internalName(DATABASE_4_INTERNALNAME)
            .exchangeName(DATABASE_4_EXCHANGE)
            .owner(USER_4_BRIEF_DTO)
            .tables(new LinkedList<>()) /* TABLE_9_DTO */
            .views(new LinkedList<>())
            .identifiers(new LinkedList<>()) /* IDENTIFIER_7_DTO */
            .build();

    public static final DatabaseDto DATABASE_4_PRIVILEGED_DTO = DatabaseDto.builder()
            .id(DATABASE_4_ID)
            .isPublic(DATABASE_4_PUBLIC)
            .isSchemaPublic(DATABASE_4_SCHEMA_PUBLIC)
            .name(DATABASE_4_NAME)
            .container(CONTAINER_2_PRIVILEGED_DTO)
            .description(DATABASE_4_DESCRIPTION)
            .internalName(DATABASE_4_INTERNALNAME)
            .exchangeName(DATABASE_4_EXCHANGE)
            .owner(USER_4_BRIEF_DTO)
            .tables(new LinkedList<>()) /* TABLE_9_DTO */
            .views(new LinkedList<>())
            .identifiers(new LinkedList<>()) /* IDENTIFIER_7_DTO */
            .lastRetrieved(Instant.now())
            .build();

    public static final CreateTableDto TABLE_0_CREATE_DTO = CreateTableDto.builder()
            .name("full")
            .description("full example")
            .constraints(CreateTableConstraintsDto.builder()
                    .uniques(new LinkedList<>())
                    .foreignKeys(new LinkedList<>())
                    .build())
            .columns(List.of(CreateTableColumnDto.builder()
                            .name("col1a")
                            .type(ColumnTypeDto.CHAR)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col1b")
                            .type(ColumnTypeDto.CHAR)
                            .nullAllowed(true)
                            .size(50L)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col2a")
                            .type(ColumnTypeDto.VARCHAR)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col2b")
                            .type(ColumnTypeDto.VARCHAR)
                            .nullAllowed(true)
                            .size(1024L)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col3")
                            .type(ColumnTypeDto.BINARY)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col4")
                            .type(ColumnTypeDto.VARBINARY)
                            .nullAllowed(true)
                            .size(200L)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col5")
                            .type(ColumnTypeDto.TINYBLOB)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col6")
                            .type(ColumnTypeDto.TINYTEXT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col7")
                            .type(ColumnTypeDto.TEXT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col8")
                            .type(ColumnTypeDto.BLOB)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col9")
                            .type(ColumnTypeDto.MEDIUMTEXT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col10")
                            .type(ColumnTypeDto.MEDIUMBLOB)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col11")
                            .type(ColumnTypeDto.LONGTEXT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col12")
                            .type(ColumnTypeDto.LONGBLOB)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col13")
                            .type(ColumnTypeDto.ENUM)
                            .nullAllowed(true)
                            .enums(new LinkedList<>(List.of("val1", "val2")))
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col14")
                            .type(ColumnTypeDto.SET)
                            .nullAllowed(true)
                            .sets(new LinkedList<>(List.of("val1", "val2")))
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col15")
                            .type(ColumnTypeDto.BIT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col16")
                            .type(ColumnTypeDto.TINYINT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col17")
                            .type(ColumnTypeDto.BOOL)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col18")
                            .type(ColumnTypeDto.SMALLINT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col19")
                            .type(ColumnTypeDto.MEDIUMINT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col20")
                            .type(ColumnTypeDto.INT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col21")
                            .type(ColumnTypeDto.BIGINT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col22")
                            .type(ColumnTypeDto.FLOAT)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col23")
                            .type(ColumnTypeDto.DOUBLE)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col24")
                            .type(ColumnTypeDto.DECIMAL)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col25")
                            .type(ColumnTypeDto.DATE)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col26")
                            .type(ColumnTypeDto.DATETIME)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col27")
                            .type(ColumnTypeDto.TIMESTAMP)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col28")
                            .type(ColumnTypeDto.TIME)
                            .nullAllowed(true)
                            .build(),
                    CreateTableColumnDto.builder()
                            .name("col29")
                            .type(ColumnTypeDto.YEAR)
                            .nullAllowed(true)
                            .build()))
            .build();

    public static final UUID TABLE_1_ID = UUID.fromString("666d0b6b-f017-4f7c-80d8-a47174d8b539");
    public static final String TABLE_1_NAME = "Weather AUS";
    public static final String TABLE_1_INTERNAL_NAME = "weather_aus";
    public static final Boolean TABLE_1_VERSIONED = true;
    public static final Boolean TABLE_1_IS_PUBLIC = false;
    public static final Boolean TABLE_1_SCHEMA_PUBLIC = false;
    public static final Boolean TABLE_1_PROCESSED_CONSTRAINTS = true;
    public static final String TABLE_1_DESCRIPTION = "Weather in Australia";
    public static final String TABLE_1_QUEUE_NAME = TABLE_1_INTERNAL_NAME;
    public static final String TABLE_1_ROUTING_KEY = "dbrepo." + DATABASE_1_ID + "." + TABLE_1_ID;
    public static final Long TABLE_1_AVG_ROW_LENGTH = 3L;
    public static final Long TABLE_1_NUM_ROWS = 3L;
    public static final Long TABLE_1_DATA_LENGTH = 2000L;
    public static final Long TABLE_1_MAX_DATA_LENGTH = Long.MAX_VALUE;
    public static final Instant TABLE_1_CREATED = Instant.ofEpochSecond(1677399975L) /* 2023-02-26 08:26:15 (UTC) */;
    public static final Instant TABLE_1_LAST_MODIFIED = Instant.ofEpochSecond(1677399975L) /* 2023-02-26 08:26:15 (UTC) */;

    public static final Table TABLE_1 = Table.builder()
            .id(TABLE_1_ID)
            .tdbid(DATABASE_1_ID)
            .database(null /* DATABASE_1 */)
            .created(TABLE_1_CREATED)
            .internalName(TABLE_1_INTERNAL_NAME)
            .isVersioned(TABLE_1_VERSIONED)
            .isPublic(TABLE_1_IS_PUBLIC)
            .isSchemaPublic(TABLE_1_SCHEMA_PUBLIC)
            .description(TABLE_1_DESCRIPTION)
            .name(TABLE_1_NAME)
            .queueName(TABLE_1_QUEUE_NAME)
            .identifiers(new LinkedList<>())
            .columns(new LinkedList<>() /* TABLE_1_COLUMNS */)
            .constraints(null) /* TABLE_1_CONSTRAINTS */
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .lastModified(TABLE_1_LAST_MODIFIED)
            .avgRowLength(TABLE_1_AVG_ROW_LENGTH)
            .numRows(TABLE_1_NUM_ROWS)
            .dataLength(TABLE_1_DATA_LENGTH)
            .maxDataLength(TABLE_1_MAX_DATA_LENGTH)
            .build();

    public static final TableDto TABLE_1_DTO = TableDto.builder()
            .id(TABLE_1_ID)
            .databaseId(DATABASE_1_ID)
            .internalName(TABLE_1_INTERNAL_NAME)
            .isVersioned(TABLE_1_VERSIONED)
            .isPublic(TABLE_1_IS_PUBLIC)
            .isSchemaPublic(TABLE_1_SCHEMA_PUBLIC)
            .description(TABLE_1_DESCRIPTION)
            .name(TABLE_1_NAME)
            .queueName(TABLE_1_QUEUE_NAME)
            .routingKey(TABLE_1_ROUTING_KEY)
            .identifiers(new LinkedList<>())
            .columns(new LinkedList<>() /* TABLE_1_COLUMNS_DTO */)
            .constraints(null) /* TABLE_1_CONSTRAINTS_DTO */
            .owner(USER_1_BRIEF_DTO)
            .avgRowLength(TABLE_1_AVG_ROW_LENGTH)
            .numRows(TABLE_1_NUM_ROWS)
            .dataLength(TABLE_1_DATA_LENGTH)
            .maxDataLength(TABLE_1_MAX_DATA_LENGTH)
            .build();

    public static final UUID COLUMN_1_1_ID = UUID.fromString("377c0a6e-938e-458c-ad2b-bbbd75d46412");

    public static final UUID COLUMN_1_2_ID = UUID.fromString("dbca4821-3023-479b-a25a-c08eb0ec02ce");

    public static final UUID COLUMN_1_3_ID = UUID.fromString("8ff0351e-4882-4948-94af-598e4b264b25");

    public static final UUID COLUMN_1_4_ID = UUID.fromString("9ab256eb-3324-4e76-af3b-e3e2a58ce161");

    public static final UUID COLUMN_1_5_ID = UUID.fromString("619e9355-51aa-438f-8579-80cec30f35cb");

    public static final List<ColumnDto> TABLE_1_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_1_1_ID)
                    .tableId(TABLE_1_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(0)
                    .name("id")
                    .internalName("id")
                    .columnType(ColumnTypeDto.SERIAL)
                    .isNullAllowed(false)
                    .enums(null)
                    .sets(null)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_1_2_ID)
                    .tableId(TABLE_1_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(1)
                    .name("Date")
                    .internalName("date")
                    .columnType(ColumnTypeDto.DATE)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_1_3_ID)
                    .tableId(TABLE_1_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(2)
                    .name("Location")
                    .internalName("location")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .size(255L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_1_4_ID)
                    .tableId(TABLE_1_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(3)
                    .name("MinTemp")
                    .internalName("mintemp")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_1_5_ID)
                    .tableId(TABLE_1_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(4)
                    .name("Rainfall")
                    .internalName("rainfall")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .concept(CONCEPT_1_BRIEF_DTO)
                    .unit(UNIT_1_BRIEF_DTO)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build());

    public static final TableBriefDto TABLE_1_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_1_ID)
            .databaseId(DATABASE_1_ID)
            .internalName(TABLE_1_INTERNAL_NAME)
            .isVersioned(TABLE_1_VERSIONED)
            .isPublic(TABLE_1_IS_PUBLIC)
            .isSchemaPublic(TABLE_1_SCHEMA_PUBLIC)
            .description(TABLE_1_DESCRIPTION)
            .name(TABLE_1_NAME)
            .ownedBy(USER_1_ID)
            .build();

    public static final Long TABLE_1_DATA_COUNT = 3L;
    @SuppressWarnings("java:S3599")
    public static final List<Map<String, Object>> TABLE_1_DATA_DTO = new LinkedList<>(List.of(
            new HashMap<>() {{
                put("id", BigInteger.valueOf(1L));
                put("date", LocalDate.of(2008, 12, 1).atStartOfDay().toInstant(ZoneOffset.UTC));
                put("location", "Albury");
                put("mintemp", 13.4);
                put("rainfall", 0.6);
            }},
            new HashMap<>() {{
                put("id", BigInteger.valueOf(2L));
                put("date", LocalDate.of(2008, 12, 2).atStartOfDay().toInstant(ZoneOffset.UTC));
                put("location", "Albury");
                put("mintemp", 7.4);
                put("rainfall", 0);
            }},
            new HashMap<>() {{
                put("id", BigInteger.valueOf(3L));
                put("date", LocalDate.of(2008, 12, 3).atStartOfDay().toInstant(ZoneOffset.UTC));
                put("location", "Albury");
                put("mintemp", 12.9);
                put("rainfall", 0);
            }}
    ));

    public static final UUID TABLE_2_ID = UUID.fromString("0cc067b6-4e81-4871-b47e-17a38228a574");
    public static final String TABLE_2_NAME = "Weather Location";
    public static final String TABLE_2_INTERNALNAME = "weather_location";
    public static final Boolean TABLE_2_VERSIONED = true;
    public static final Boolean TABLE_2_IS_PUBLIC = false;
    public static final Boolean TABLE_2_SCHEMA_PUBLIC = true;
    public static final Boolean TABLE_2_PROCESSED_CONSTRAINTS = true;
    public static final String TABLE_2_DESCRIPTION = "Weather location";
    public static final String TABLE_2_QUEUE_NAME = TABLE_2_INTERNALNAME;
    public static final String TABLE_2_ROUTING_KEY = "dbrepo." + DATABASE_1_ID + "." + TABLE_2_ID;
    public static final Instant TABLE_2_CREATED = Instant.ofEpochSecond(1677400007L) /* 2023-02-26 08:26:47 (UTC) */;
    public static final Instant TABLE_2_LAST_MODIFIED = Instant.ofEpochSecond(1677400007L) /* 2023-02-26 08:26:47 (UTC) */;
    public static final Long TABLE_2_AVG_ROW_LENGTH = 3L;
    public static final Long TABLE_2_NUM_ROWS = 3L;
    public static final Long TABLE_2_DATA_LENGTH = 2000L;
    public static final Long TABLE_2_MAX_DATA_LENGTH = Long.MAX_VALUE;

    public static final Table TABLE_2 = Table.builder()
            .id(TABLE_2_ID)
            .tdbid(DATABASE_1_ID)
            .database(null /* DATABASE_1 */)
            .created(TABLE_2_CREATED)
            .internalName(TABLE_2_INTERNALNAME)
            .isVersioned(TABLE_2_VERSIONED)
            .isPublic(TABLE_2_IS_PUBLIC)
            .isSchemaPublic(TABLE_2_SCHEMA_PUBLIC)
            .description(TABLE_2_DESCRIPTION)
            .name(TABLE_2_NAME)
            .lastModified(TABLE_2_LAST_MODIFIED)
            .queueName(TABLE_2_QUEUE_NAME)
            .columns(new LinkedList<>() /* TABLE_2_COLUMNS */)
            .constraints(null) /* TABLE_2_CONSTRAINTS */
            .owner(USER_2)
            .ownedBy(USER_2_ID)
            .avgRowLength(TABLE_2_AVG_ROW_LENGTH)
            .numRows(TABLE_2_NUM_ROWS)
            .dataLength(TABLE_2_DATA_LENGTH)
            .maxDataLength(TABLE_2_MAX_DATA_LENGTH)
            .build();

    public static final TableDto TABLE_2_DTO = TableDto.builder()
            .id(TABLE_2_ID)
            .databaseId(DATABASE_1_ID)
            .internalName(TABLE_2_INTERNALNAME)
            .isVersioned(TABLE_2_VERSIONED)
            .isPublic(TABLE_2_IS_PUBLIC)
            .isSchemaPublic(TABLE_2_SCHEMA_PUBLIC)
            .description(TABLE_2_DESCRIPTION)
            .name(TABLE_2_NAME)
            .queueName(TABLE_2_QUEUE_NAME)
            .routingKey(TABLE_2_ROUTING_KEY)
            .columns(new LinkedList<>() /* TABLE_2_COLUMNS_DTO */)
            .constraints(null) /* TABLE_2_CONSTRAINTS_DTO */
            .owner(USER_2_BRIEF_DTO)
            .avgRowLength(TABLE_2_AVG_ROW_LENGTH)
            .numRows(TABLE_2_NUM_ROWS)
            .dataLength(TABLE_2_DATA_LENGTH)
            .maxDataLength(TABLE_2_MAX_DATA_LENGTH)
            .build();

    public static final TableBriefDto TABLE_2_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_2_ID)
            .databaseId(DATABASE_1_ID)
            .internalName(TABLE_2_INTERNALNAME)
            .isVersioned(TABLE_2_VERSIONED)
            .isPublic(TABLE_2_IS_PUBLIC)
            .isSchemaPublic(TABLE_2_SCHEMA_PUBLIC)
            .description(TABLE_2_DESCRIPTION)
            .name(TABLE_2_NAME)
            .ownedBy(USER_2_ID)
            .build();

    public static final UUID TABLE_3_ID = UUID.fromString("a94ee518-c235-496b-8613-b0c643bc1b11");
    public static final String TABLE_3_NAME = "Sensor";
    public static final String TABLE_3_INTERNALNAME = "sensor";
    public static final Boolean TABLE_3_VERSIONED = true;
    public static final Boolean TABLE_3_IS_PUBLIC = false;
    public static final Boolean TABLE_3_SCHEMA_PUBLIC = false;
    public static final Boolean TABLE_3_PROCESSED_CONSTRAINTS = true;
    public static final String TABLE_3_DESCRIPTION = "Some sensor data";
    public static final String TABLE_3_QUEUE_NAME = TABLE_3_INTERNALNAME;
    public static final String TABLE_3_ROUTING_KEY = "dbrepo." + DATABASE_1_ID + "." + TABLE_3_ID;
    public static final Instant TABLE_3_CREATED = Instant.ofEpochSecond(1677400031L) /* 2023-02-26 08:27:11 (UTC) */;
    public static final Instant TABLE_3_LAST_MODIFIED = Instant.ofEpochSecond(1677400031L) /* 2023-02-26 08:27:11 (UTC) */;
    public static final Long TABLE_3_AVG_ROW_LENGTH = 6L;
    public static final Long TABLE_3_NUM_ROWS = 6L;
    public static final Long TABLE_3_DATA_LENGTH = 1800L;
    public static final Long TABLE_3_MAX_DATA_LENGTH = Long.MAX_VALUE;

    public static final Table TABLE_3 = Table.builder()
            .id(TABLE_3_ID)
            .tdbid(DATABASE_1_ID)
            .database(null /* DATABASE_1 */)
            .created(TABLE_3_CREATED)
            .internalName(TABLE_3_INTERNALNAME)
            .isVersioned(TABLE_3_VERSIONED)
            .isPublic(TABLE_3_IS_PUBLIC)
            .isSchemaPublic(TABLE_3_SCHEMA_PUBLIC)
            .description(TABLE_3_DESCRIPTION)
            .name(TABLE_3_NAME)
            .lastModified(TABLE_3_LAST_MODIFIED)
            .queueName(TABLE_3_QUEUE_NAME)
            .columns(new LinkedList<>() /* TABLE_3_COLUMNS */)
            .constraints(null) /* TABLE_3_CONSTRAINTS */
            .owner(USER_3)
            .ownedBy(USER_3_ID)
            .avgRowLength(TABLE_3_AVG_ROW_LENGTH)
            .numRows(TABLE_3_NUM_ROWS)
            .dataLength(TABLE_3_DATA_LENGTH)
            .maxDataLength(TABLE_3_MAX_DATA_LENGTH)
            .build();

    public static final TableDto TABLE_3_DTO = TableDto.builder()
            .id(TABLE_3_ID)
            .databaseId(DATABASE_1_ID)
            .internalName(TABLE_3_INTERNALNAME)
            .isVersioned(TABLE_3_VERSIONED)
            .isPublic(TABLE_3_IS_PUBLIC)
            .isSchemaPublic(TABLE_3_SCHEMA_PUBLIC)
            .description(TABLE_3_DESCRIPTION)
            .name(TABLE_3_NAME)
            .queueName(TABLE_3_QUEUE_NAME)
            .routingKey(TABLE_3_ROUTING_KEY)
            .columns(new LinkedList<>() /* TABLE_3_COLUMNS_DTO */)
            .constraints(null) /* TABLE_3_CONSTRAINTS_DTO */
            .owner(USER_3_BRIEF_DTO)
            .avgRowLength(TABLE_3_AVG_ROW_LENGTH)
            .numRows(TABLE_3_NUM_ROWS)
            .dataLength(TABLE_3_DATA_LENGTH)
            .maxDataLength(TABLE_3_MAX_DATA_LENGTH)
            .build();

    public static final TableBriefDto TABLE_3_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_3_ID)
            .databaseId(DATABASE_1_ID)
            .internalName(TABLE_3_INTERNALNAME)
            .isVersioned(TABLE_3_VERSIONED)
            .isPublic(TABLE_3_IS_PUBLIC)
            .isSchemaPublic(TABLE_3_SCHEMA_PUBLIC)
            .description(TABLE_3_DESCRIPTION)
            .name(TABLE_3_NAME)
            .ownedBy(USER_3_ID)
            .build();

    public static final CreateTableConstraintsDto TABLE_3_CONSTRAINTS_CREATE_DTO = CreateTableConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .primaryKey(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .build();

    public static final CreateTableConstraintsDto TABLE_3_CONSTRAINTS_INVALID_CREATE_DTO = CreateTableConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .primaryKey(new LinkedHashSet<>()) // <<<<
            .uniques(new LinkedList<>())
            .foreignKeys(List.of(CreateForeignKeyDto.builder()
                    .referencedTable("weather_location")
                    .columns(new LinkedList<>(List.of("fahrzeug")))
                    .referencedColumns(new LinkedList<>(List.of("doesnotexist")))
                    .build()))
            .build();

    public static final CreateTableDto TABLE_3_CREATE_DTO = CreateTableDto.builder()
            .name(TABLE_3_NAME)
            .description(TABLE_3_DESCRIPTION)
            .columns(new LinkedList<>())
            .constraints(TABLE_3_CONSTRAINTS_CREATE_DTO)
            .build();

    public static final CreateTableDto TABLE_3_INVALID_CREATE_DTO = CreateTableDto.builder()
            .name(TABLE_3_NAME)
            .description(TABLE_3_DESCRIPTION)
            .columns(new LinkedList<>())
            .constraints(TABLE_3_CONSTRAINTS_INVALID_CREATE_DTO)
            .build();

    public static final UUID TABLE_5_ID = UUID.fromString("91306cbd-c51f-47d3-8722-debfdbd8a77e");
    public static final String TABLE_5_NAME = "zoo";
    public static final String TABLE_5_INTERNALNAME = "zoo";
    public static final Boolean TABLE_5_VERSIONED = true;
    public static final Boolean TABLE_5_IS_PUBLIC = true;
    public static final Boolean TABLE_5_SCHEMA_PUBLIC = true;
    public static final Boolean TABLE_5_PROCESSED_CONSTRAINTS = true;
    public static final String TABLE_5_DESCRIPTION = "Some Kaggle dataset";
    public static final String TABLE_5_QUEUE_NAME = TABLE_5_INTERNALNAME;
    public static final String TABLE_5_ROUTING_KEY = "dbrepo." + DATABASE_2_ID + "." + TABLE_5_ID;
    public static final Instant TABLE_5_CREATED = Instant.ofEpochSecond(1677400067L) /* 2023-02-26 08:27:47 (UTC) */;
    public static final Instant TABLE_5_LAST_MODIFIED = Instant.ofEpochSecond(1677400067L) /* 2023-02-26 08:27:47 (UTC) */;
    public static final Long TABLE_5_AVG_ROW_LENGTH = 1080L;
    public static final Long TABLE_5_NUM_ROWS = 101L;
    public static final Long TABLE_5_DATA_LENGTH = 15200L;
    public static final Long TABLE_5_MAX_DATA_LENGTH = Long.MAX_VALUE;

    public static final Table TABLE_5 = Table.builder()
            .id(TABLE_5_ID)
            .tdbid(DATABASE_2_ID)
            .created(Instant.now())
            .internalName(TABLE_5_INTERNALNAME)
            .isVersioned(TABLE_5_VERSIONED)
            .isPublic(TABLE_5_IS_PUBLIC)
            .isSchemaPublic(TABLE_5_SCHEMA_PUBLIC)
            .description(TABLE_5_DESCRIPTION)
            .name(TABLE_5_NAME)
            .lastModified(TABLE_5_LAST_MODIFIED)
            .queueName(TABLE_5_QUEUE_NAME)
            .columns(new LinkedList<>()) /* TABLE_5_COLUMNS */
            .constraints(null) /* TABLE_5_CONSTRAINTS */
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .build();

    public static final TableDto TABLE_5_DTO = TableDto.builder()
            .id(TABLE_5_ID)
            .databaseId(DATABASE_2_ID)
            .internalName(TABLE_5_INTERNALNAME)
            .isVersioned(TABLE_5_VERSIONED)
            .isPublic(TABLE_5_IS_PUBLIC)
            .isSchemaPublic(TABLE_5_SCHEMA_PUBLIC)
            .description(TABLE_5_DESCRIPTION)
            .name(TABLE_5_NAME)
            .queueName(TABLE_5_QUEUE_NAME)
            .routingKey(TABLE_5_ROUTING_KEY)
            .columns(new LinkedList<>()) /* TABLE_5_COLUMNS_DTO */
            .constraints(null) /* TABLE_5_CONSTRAINTS_DTO */
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final TableBriefDto TABLE_5_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_5_ID)
            .databaseId(DATABASE_2_ID)
            .internalName(TABLE_5_INTERNALNAME)
            .isVersioned(TABLE_5_VERSIONED)
            .isPublic(TABLE_5_IS_PUBLIC)
            .isSchemaPublic(TABLE_5_SCHEMA_PUBLIC)
            .description(TABLE_5_DESCRIPTION)
            .name(TABLE_5_NAME)
            .ownedBy(USER_1_ID)
            .build();

    public static final UUID TABLE_6_ID = UUID.fromString("ae84d169-d36c-4f5a-a390-153d090f9574");
    public static final String TABLE_6_NAME = "names";
    public static final String TABLE_6_INTERNALNAME = "names";
    public static final Boolean TABLE_6_VERSIONED = true;
    public static final Boolean TABLE_6_IS_PUBLIC = true;
    public static final Boolean TABLE_6_SCHEMA_PUBLIC = false;
    public static final Boolean TABLE_6_PROCESSED_CONSTRAINTS = true;
    public static final String TABLE_6_DESCRIPTION = "Some names dataset";
    public static final String TABLE_6_QUEUE_NAME = TABLE_6_INTERNALNAME;
    public static final String TABLE_6_ROUTING_KEY = "dbrepo." + DATABASE_2_ID + "." + TABLE_6_ID;
    public static final Instant TABLE_6_CREATED = Instant.ofEpochSecond(1677400117L) /* 2023-02-26 08:28:37 (UTC) */;
    public static final Instant TABLE_6_LAST_MODIFIED = Instant.ofEpochSecond(1677400117L) /* 2023-02-26 08:28:37 (UTC) */;

    public static final Table TABLE_6 = Table.builder()
            .id(TABLE_6_ID)
            .tdbid(DATABASE_2_ID)
            .created(TABLE_6_CREATED)
            .internalName(TABLE_6_INTERNALNAME)
            .isVersioned(TABLE_6_VERSIONED)
            .isPublic(TABLE_6_IS_PUBLIC)
            .isSchemaPublic(TABLE_6_SCHEMA_PUBLIC)
            .description(TABLE_6_DESCRIPTION)
            .name(TABLE_6_NAME)
            .lastModified(TABLE_6_LAST_MODIFIED)
            .queueName(TABLE_6_QUEUE_NAME)
            .columns(new LinkedList<>()) /* TABLE_6_COLUMNS */
            .constraints(null) /* TABLE_6_CONSTRAINTS */
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .created(TABLE_6_CREATED)
            .build();

    public static final TableDto TABLE_6_DTO = TableDto.builder()
            .id(TABLE_6_ID)
            .databaseId(DATABASE_2_ID)
            .internalName(TABLE_6_INTERNALNAME)
            .isVersioned(TABLE_6_VERSIONED)
            .isPublic(TABLE_6_IS_PUBLIC)
            .isSchemaPublic(TABLE_6_SCHEMA_PUBLIC)
            .description(TABLE_6_DESCRIPTION)
            .name(TABLE_6_NAME)
            .queueName(TABLE_6_QUEUE_NAME)
            .routingKey(TABLE_6_ROUTING_KEY)
            .columns(new LinkedList<>()) /* TABLE_6_COLUMNS_DTO */
            .constraints(null) /* TABLE_6_CONSTRAINTS_DTO */
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final TableBriefDto TABLE_6_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_6_ID)
            .databaseId(DATABASE_2_ID)
            .internalName(TABLE_6_INTERNALNAME)
            .isVersioned(TABLE_6_VERSIONED)
            .isPublic(TABLE_6_IS_PUBLIC)
            .isSchemaPublic(TABLE_6_SCHEMA_PUBLIC)
            .description(TABLE_6_DESCRIPTION)
            .name(TABLE_6_NAME)
            .ownedBy(USER_1_ID)
            .build();

    public static final UUID TABLE_7_ID = UUID.fromString("e5d10200-3e4f-45f4-9f36-ff3ca39c6c29");
    public static final String TABLE_7_NAME = "likes";
    public static final String TABLE_7_INTERNAL_NAME = "likes";
    public static final Boolean TABLE_7_VERSIONED = true;
    public static final Boolean TABLE_7_IS_PUBLIC = true;
    public static final Boolean TABLE_7_SCHEMA_PUBLIC = true;
    public static final Boolean TABLE_7_PROCESSED_CONSTRAINTS = true;
    public static final String TABLE_7_DESCRIPTION = "Some likes dataset";
    public static final String TABLE_7_QUEUE_NAME = TABLE_7_INTERNAL_NAME;
    public static final String TABLE_7_ROUTING_KEY = "dbrepo." + DATABASE_2_ID + "." + TABLE_7_ID;
    public static final Instant TABLE_7_CREATED = Instant.ofEpochSecond(1677400147L) /* 2023-02-26 08:29:07 (UTC) */;
    public static final Instant TABLE_7_LAST_MODIFIED = Instant.ofEpochSecond(1677400147L) /* 2023-02-26 08:29:07 (UTC) */;

    public static final Table TABLE_7 = Table.builder()
            .id(TABLE_7_ID)
            .tdbid(DATABASE_2_ID)
            .created(TABLE_7_CREATED)
            .internalName(TABLE_7_INTERNAL_NAME)
            .isVersioned(TABLE_7_VERSIONED)
            .isPublic(TABLE_7_IS_PUBLIC)
            .isSchemaPublic(TABLE_7_SCHEMA_PUBLIC)
            .description(TABLE_7_DESCRIPTION)
            .name(TABLE_7_NAME)
            .lastModified(TABLE_7_LAST_MODIFIED)
            .queueName(TABLE_7_QUEUE_NAME)
            .columns(new LinkedList<>()) /* TABLE_7_COLUMNS */
            .constraints(null) /* TABLE_7_CONSTRAINTS */
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .created(TABLE_7_CREATED)
            .build();

    public static final TableDto TABLE_7_DTO = TableDto.builder()
            .id(TABLE_7_ID)
            .databaseId(DATABASE_2_ID)
            .internalName(TABLE_7_INTERNAL_NAME)
            .isVersioned(TABLE_7_VERSIONED)
            .isPublic(TABLE_7_IS_PUBLIC)
            .isSchemaPublic(TABLE_7_SCHEMA_PUBLIC)
            .description(TABLE_7_DESCRIPTION)
            .name(TABLE_7_NAME)
            .queueName(TABLE_7_QUEUE_NAME)
            .routingKey(TABLE_7_ROUTING_KEY)
            .columns(new LinkedList<>()) /* TABLE_7_COLUMNS_DTO */
            .constraints(null) /* TABLE_7_CONSTRAINTS_DTO */
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final TableBriefDto TABLE_7_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_7_ID)
            .databaseId(DATABASE_2_ID)
            .internalName(TABLE_7_INTERNAL_NAME)
            .isVersioned(TABLE_7_VERSIONED)
            .isPublic(TABLE_7_IS_PUBLIC)
            .isSchemaPublic(TABLE_7_SCHEMA_PUBLIC)
            .description(TABLE_7_DESCRIPTION)
            .name(TABLE_7_NAME)
            .ownedBy(USER_1_ID)
            .build();

    public static final UUID TABLE_4_ID = UUID.fromString("6c87cbcf-5043-404f-9bf1-b09ddbac25a2");
    public static final String TABLE_4_NAME = "Sensor 2";
    public static final String TABLE_4_INTERNALNAME = "sensor_2";
    public static final Boolean TABLE_4_VERSIONED = true;
    public static final Boolean TABLE_4_IS_PUBLIC = true;
    public static final Boolean TABLE_4_SCHEMA_PUBLIC = false;
    public static final String TABLE_4_DESCRIPTION = "Hello sensor";
    public static final String TABLE_4_QUEUE_NAME = TABLE_4_INTERNALNAME;
    public static final String TABLE_4_ROUTING_KEY = "dbrepo." + DATABASE_1_ID + "." + TABLE_4_ID;
    public static final Instant TABLE_4_CREATED = Instant.ofEpochSecond(1677400175L) /* 2023-02-26 08:29:35 (UTC) */;
    public static final Instant TABLE_4_LAST_MODIFIED = Instant.ofEpochSecond(1677400175L) /* 2023-02-26 08:29:35 (UTC) */;
    public static final Long TABLE_4_AVG_ROW_LENGTH = 0L;
    public static final Long TABLE_4_NUM_ROWS = 0L;
    public static final Long TABLE_4_DATA_LENGTH = 1000L;
    public static final Long TABLE_4_MAX_DATA_LENGTH = Long.MAX_VALUE;

    public static final Table TABLE_4 = Table.builder()
            .id(TABLE_4_ID)
            .tdbid(DATABASE_1_ID)
            .internalName(TABLE_4_INTERNALNAME)
            .description(TABLE_4_DESCRIPTION)
            .database(null /* DATABASE_1 */)
            .name(TABLE_4_NAME)
            .queueName(TABLE_4_QUEUE_NAME)
            .columns(new LinkedList<>()) /* TABLE_4_COLUMNS */
            .constraints(null) /* TABLE_4_CONSTRAINTS */
            .isVersioned(TABLE_4_VERSIONED)
            .isPublic(TABLE_4_IS_PUBLIC)
            .isSchemaPublic(TABLE_4_SCHEMA_PUBLIC)
            .owner(USER_1)
            .ownedBy(USER_1_ID)
            .created(TABLE_4_CREATED)
            .lastModified(TABLE_4_LAST_MODIFIED)
            .avgRowLength(TABLE_4_AVG_ROW_LENGTH)
            .numRows(TABLE_4_NUM_ROWS)
            .dataLength(TABLE_4_DATA_LENGTH)
            .maxDataLength(TABLE_4_MAX_DATA_LENGTH)
            .build();

    public static final TableDto TABLE_4_DTO = TableDto.builder()
            .id(TABLE_4_ID)
            .databaseId(DATABASE_1_ID)
            .internalName(TABLE_4_INTERNALNAME)
            .description(TABLE_4_DESCRIPTION)
            .name(TABLE_4_NAME)
            .queueName(TABLE_4_QUEUE_NAME)
            .routingKey(TABLE_4_ROUTING_KEY)
            .columns(new LinkedList<>()) /* TABLE_4_COLUMNS_DTO */
            .constraints(null) /* TABLE_4_CONSTRAINTS_DTO */
            .isVersioned(TABLE_4_VERSIONED)
            .isPublic(TABLE_4_IS_PUBLIC)
            .isSchemaPublic(TABLE_4_SCHEMA_PUBLIC)
            .owner(USER_1_BRIEF_DTO)
            .avgRowLength(TABLE_4_AVG_ROW_LENGTH)
            .numRows(TABLE_4_NUM_ROWS)
            .dataLength(TABLE_4_DATA_LENGTH)
            .maxDataLength(TABLE_4_MAX_DATA_LENGTH)
            .build();

    public static final TableBriefDto TABLE_4_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_4_ID)
            .databaseId(DATABASE_1_ID)
            .internalName(TABLE_4_INTERNALNAME)
            .description(TABLE_4_DESCRIPTION)
            .name(TABLE_4_NAME)
            .isVersioned(TABLE_4_VERSIONED)
            .isPublic(TABLE_4_IS_PUBLIC)
            .isSchemaPublic(TABLE_4_SCHEMA_PUBLIC)
            .ownedBy(USER_1_ID)
            .build();

    public static final ColumnBriefDto TABLE_4_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(UUID.fromString("360f02be-6dfb-48ea-9d1e-1da488b0e324"))
            .name("Timestamp")
            .internalName("timestamp")
            .columnType(ColumnTypeDto.TIMESTAMP)
            .build();

    public static final UUID COLUMN_4_1_ID = UUID.fromString("c8ec8a56-dea1-4316-895f-56e6d289cbf7");

    public static final UUID COLUMN_4_2_ID = UUID.fromString("d06956ae-aabd-474f-a47d-47af1ba043d1");

    public static final List<TableColumn> TABLE_4_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_4_1_ID)
                    .ordinalPosition(0)
                    .table(TABLE_4)
                    .name("Timestamp")
                    .internalName("timestamp")
                    .columnType(TableColumnType.TIMESTAMP)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_4_2_ID)
                    .ordinalPosition(1)
                    .table(TABLE_4)
                    .name("Value")
                    .internalName("value")
                    .columnType(TableColumnType.DECIMAL)
                    .isNullAllowed(true)
                    .build());

    public static final List<CreateTableColumnDto> TABLE_4_COLUMNS_CREATE_DTO = List.of(CreateTableColumnDto.builder()
                    .name("Timestamp")
                    .type(ColumnTypeDto.TIMESTAMP)
                    .nullAllowed(false)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Value")
                    .type(ColumnTypeDto.DECIMAL)
                    .nullAllowed(true)
                    .size(10L)
                    .d(10L)
                    .build());

    public static final CreateTableConstraintsDto TABLE_4_CONSTRAINTS_CREATE_DTO = CreateTableConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .primaryKey(new LinkedHashSet<>(Set.of("Timestamp")))
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>(List.of(List.of("Timestamp"))))
            .build();

    public static final CreateTableDto TABLE_4_CREATE_DTO = CreateTableDto.builder()
            .name(TABLE_4_NAME)
            .description(TABLE_4_DESCRIPTION)
            .columns(TABLE_4_COLUMNS_CREATE_DTO)
            .constraints(TABLE_4_CONSTRAINTS_CREATE_DTO)
            .build();

    public static final at.tuwien.api.database.table.internal.TableCreateDto TABLE_4_CREATE_INTERNAL_DTO = at.tuwien.api.database.table.internal.TableCreateDto.builder()
            .name(TABLE_4_NAME)
            .description(TABLE_4_DESCRIPTION)
            .columns(TABLE_4_COLUMNS_CREATE_DTO)
            .constraints(TABLE_4_CONSTRAINTS_CREATE_DTO)
            .build();

    public static final List<ColumnDto> TABLE_4_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_4_1_ID)
                    .databaseId(DATABASE_1_ID)
                    .tableId(TABLE_4_ID)
                    .name("Timestamp")
                    .internalName("timestamp")
                    .columnType(ColumnTypeDto.TIMESTAMP)
                    .isNullAllowed(false)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_4_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .tableId(TABLE_4_ID)
                    .name("Value")
                    .internalName("value")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .isNullAllowed(true)
                    .build());

    public static final UUID TABLE_8_ID = UUID.fromString("2e039d0d-3257-4083-8b32-76d7cfa1f7fd");
    public static final String TABLE_8_NAME = "location";
    public static final String TABLE_8_INTERNAL_NAME = "mfcc";
    public static final Boolean TABLE_8_VERSIONED = true;
    public static final Boolean TABLE_8_IS_PUBLIC = false;
    public static final Boolean TABLE_8_SCHEMA_PUBLIC = false;
    public static final String TABLE_8_DESCRIPTION = "Hello mfcc";
    public static final String TABLE_8_QUEUE_NAME = TABLE_8_INTERNAL_NAME;
    public static final String TABLE_8_ROUTING_KEY = "dbrepo." + DATABASE_3_ID + "." + TABLE_8_ID;
    public static final Instant TABLE_8_CREATED = Instant.ofEpochSecond(1688400185L) /* 2023-02-26 08:29:35 (UTC) */;
    public static final Instant TABLE_8_LAST_MODIFIED = Instant.ofEpochSecond(1688400185L) /* 2023-02-26 08:29:35 (UTC) */;

    public static final Table TABLE_8 = Table.builder()
            .id(TABLE_8_ID)
            .tdbid(DATABASE_3_ID)
            .internalName(TABLE_8_INTERNAL_NAME)
            .description(TABLE_8_DESCRIPTION)
            .isVersioned(TABLE_8_VERSIONED)
            .isPublic(TABLE_8_IS_PUBLIC)
            .isSchemaPublic(TABLE_8_SCHEMA_PUBLIC)
            .database(null /* DATABASE_1 */)
            .name(TABLE_8_NAME)
            .queueName(TABLE_8_QUEUE_NAME)
            .columns(new LinkedList<>()) /* TABLE_8_COLUMNS */
            .constraints(null) /* TABLE_8_CONSTRAINTS */
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .created(TABLE_8_CREATED)
            .lastModified(TABLE_8_LAST_MODIFIED)
            .build();

    public static final TableDto TABLE_8_DTO = TableDto.builder()
            .id(TABLE_8_ID)
            .databaseId(DATABASE_3_ID)
            .internalName(TABLE_8_INTERNAL_NAME)
            .description(TABLE_8_DESCRIPTION)
            .isVersioned(TABLE_8_VERSIONED)
            .isPublic(TABLE_8_IS_PUBLIC)
            .isSchemaPublic(TABLE_8_SCHEMA_PUBLIC)
            .name(TABLE_8_NAME)
            .queueName(TABLE_8_QUEUE_NAME)
            .columns(new LinkedList<>()) /* TABLE_8_COLUMNS_DTO */
            .constraints(null) /* TABLE_8_CONSTRAINTS_DTO */
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final TableUpdateDto TABLE_8_UPDATE_DTO = TableUpdateDto.builder()
            .description(null)
            .isPublic(true)
            .isSchemaPublic(true)
            .build();

    public static final TableBriefDto TABLE_8_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_8_ID)
            .databaseId(DATABASE_3_ID)
            .internalName(TABLE_8_INTERNAL_NAME)
            .description(TABLE_8_DESCRIPTION)
            .isVersioned(TABLE_8_VERSIONED)
            .isPublic(TABLE_8_IS_PUBLIC)
            .isSchemaPublic(TABLE_8_SCHEMA_PUBLIC)
            .name(TABLE_8_NAME)
            .ownedBy(USER_1_ID)
            .build();

    public static final UUID TABLE_9_ID = UUID.fromString("9314294f-04fc-4354-8b1f-2a8aeb566453");
    public static final String TABLE_9_NAME = "Weather Location";
    public static final String TABLE_9_INTERNAL_NAME = "weather_location";
    public static final Boolean TABLE_9_VERSIONED = true;
    public static final Boolean TABLE_9_IS_PUBLIC = false;
    public static final Boolean TABLE_9_SCHEMA_PUBLIC = true;
    public static final Boolean TABLE_9_PROCESSED_CONSTRAINTS = true;
    public static final String TABLE_9_DESCRIPTION = "Location";
    public static final String TABLE_9_QUEUE_NAME = TABLE_9_INTERNAL_NAME;
    public static final String TABLE_9_ROUTING_KEY = "dbrepo." + DATABASE_4_ID + "." + TABLE_9_ID;
    public static final Instant TABLE_9_CREATED = Instant.ofEpochSecond(1688400185L) /* 2023-02-26 08:29:35 (UTC) */;
    public static final Instant TABLE_9_LAST_MODIFIED = Instant.ofEpochSecond(1688400185L) /* 2023-02-26 08:29:35 (UTC) */;

    public static final Table TABLE_9 = Table.builder()
            .id(TABLE_9_ID)
            .tdbid(DATABASE_4_ID)
            .internalName(TABLE_9_INTERNAL_NAME)
            .description(TABLE_9_DESCRIPTION)
            .isVersioned(TABLE_9_VERSIONED)
            .isPublic(TABLE_9_IS_PUBLIC)
            .isSchemaPublic(TABLE_9_SCHEMA_PUBLIC)
            .database(null /* DATABASE_1 */)
            .name(TABLE_9_NAME)
            .queueName(TABLE_9_QUEUE_NAME)
            .columns(new LinkedList<>()) /* TABLE_9_COLUMNS */
            .constraints(null) /* TABLE_9_CONSTRAINTS */
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .created(TABLE_9_CREATED)
            .lastModified(TABLE_9_LAST_MODIFIED)
            .build();

    public static final TableDto TABLE_9_DTO = TableDto.builder()
            .id(TABLE_9_ID)
            .databaseId(DATABASE_4_ID)
            .internalName(TABLE_9_INTERNAL_NAME)
            .description(TABLE_9_DESCRIPTION)
            .isVersioned(TABLE_9_VERSIONED)
            .isPublic(TABLE_9_IS_PUBLIC)
            .isSchemaPublic(TABLE_9_SCHEMA_PUBLIC)
            .name(TABLE_9_NAME)
            .queueName(TABLE_9_QUEUE_NAME)
            .columns(new LinkedList<>()) /* TABLE_9_COLUMNS_DTO */
            .constraints(null) /* TABLE_9_CONSTRAINTS_DTO */
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final TableBriefDto TABLE_9_BRIEF_DTO = TableBriefDto.builder()
            .id(TABLE_9_ID)
            .databaseId(DATABASE_4_ID)
            .internalName(TABLE_9_INTERNAL_NAME)
            .description(TABLE_9_DESCRIPTION)
            .isVersioned(TABLE_9_VERSIONED)
            .isPublic(TABLE_9_IS_PUBLIC)
            .isSchemaPublic(TABLE_9_SCHEMA_PUBLIC)
            .name(TABLE_9_NAME)
            .ownedBy(USER_1_ID)
            .build();

    public static final UUID COLUMN_9_1_ID = UUID.fromString("e03c7578-2d1a-4599-9b11-7174f40efc0a");
    public static final String COLUMN_9_1_NAME = "location";
    public static final String COLUMN_9_1_INTERNAL_NAME = "location";

    public static final ColumnBriefDto TABLE_9_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(COLUMN_9_1_ID)
            .name(COLUMN_9_1_NAME)
            .internalName(COLUMN_9_1_INTERNAL_NAME)
            .columnType(ColumnTypeDto.BIGINT)
            .build();

    public static final UUID COLUMN_9_2_ID = UUID.fromString("03c07223-17e1-4af5-b1ae-ef9ab434fe2d");

    public static final UUID COLUMN_9_3_ID = UUID.fromString("ee6590db-923b-4234-beb8-3120da055cf6");

    public static final List<TableColumn> TABLE_9_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_9_1_ID)
                    .ordinalPosition(0)
                    .table(TABLE_9)
                    .name(COLUMN_9_1_NAME)
                    .internalName(COLUMN_9_1_INTERNAL_NAME)
                    .columnType(TableColumnType.VARCHAR)
                    .size(255L)
                    .isNullAllowed(false)
                    .enums(null)
                    .sets(null)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_9_2_ID)
                    .ordinalPosition(1)
                    .table(TABLE_9)
                    .name("lat")
                    .internalName("lat")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_9_3_ID)
                    .ordinalPosition(2)
                    .table(TABLE_9)
                    .name("lng")
                    .internalName("lng")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build());

    public static final List<ColumnDto> TABLE_9_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_9_1_ID)
                    .ordinalPosition(0)
                    .name(COLUMN_9_1_NAME)
                    .internalName(COLUMN_9_1_INTERNAL_NAME)
                    .columnType(ColumnTypeDto.VARCHAR)
                    .size(255L)
                    .isNullAllowed(false)
                    .enums(null)
                    .sets(null)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_9_2_ID)
                    .ordinalPosition(1)
                    .name("lat")
                    .internalName("lat")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_9_3_ID)
                    .ordinalPosition(2)
                    .name("lng")
                    .internalName("lng")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build());

    public static final Constraints TABLE_9_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_9)
                    .column(TABLE_9_COLUMNS.get(0))
                    .id(COLUMN_9_1_ID)
                    .build())))
            .build();

    public static final ConstraintsDto TABLE_9_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .table(TABLE_9_BRIEF_DTO)
                    .column(TABLE_9_COLUMNS_BRIEF_0_DTO)
                    .id(COLUMN_9_1_ID)
                    .build())))
            .build();

    public static final UUID QUERY_9_ID = UUID.fromString("df34f0b9-b64c-406c-9109-7a031f4a7f27");
    public static final String QUERY_9_STATEMENT = "SELECT `lat`, `lng` FROM `mfcc` WHERE `location` = 'Fuji'";
    public static final String QUERY_9_QUERY_HASH = "dfcdec827b2ea74d89415f8d1ce39354f59ef304444ba4e12e4f3d9d3f35abe3";
    public static final String QUERY_9_RESULT_HASH = "f0aba070a1fd29e96230d12d7c0b4d08b89820b3cc2dda0575680492010016e7";
    public static final Instant QUERY_9_CREATED = Instant.now().minus(5, MINUTES);
    public static final Instant QUERY_9_EXECUTION = Instant.now().minus(1, MINUTES);
    public static final Instant QUERY_9_LAST_MODIFIED = Instant.ofEpochSecond(1551588555L);
    public static final Long QUERY_9_RESULT_NUMBER = 6L;
    public static final Boolean QUERY_9_PERSISTED = true;

    public static final QueryDto QUERY_9_DTO = QueryDto.builder()
            .id(QUERY_9_ID)
            .databaseId(DATABASE_3_ID)
            .query(QUERY_9_STATEMENT)
            .queryNormalized(QUERY_9_STATEMENT)
            .resultNumber(QUERY_9_RESULT_NUMBER)
            .resultHash(QUERY_9_RESULT_HASH)
            .queryHash(QUERY_9_QUERY_HASH)
            .execution(QUERY_9_EXECUTION)
            .isPersisted(QUERY_9_PERSISTED)
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final SubsetDto QUERY_9_SUBSET_DTO = SubsetDto.builder()
            .tableId(TABLE_9_ID)
            .columns(new LinkedList<>(List.of(COLUMN_9_2_ID, COLUMN_9_3_ID)))
            .filter(new LinkedList<>(List.of(FilterDto.builder()
                    .columnId(COLUMN_9_1_ID)
                    .operatorId(IMAGE_1_OPERATORS_2_ID)
                    .value("Fuji")
                    .type(FilterTypeDto.WHERE)
                    .build())))
            .build();

    public static final ViewDto QUERY_9_VIEW_DTO = ViewDto.builder()
            .query(QUERY_9_STATEMENT)
            .queryHash(QUERY_9_QUERY_HASH)
            .owner(USER_1_BRIEF_DTO)
            .columns(new LinkedList<>(List.of(ViewColumnDto.builder()
                            .name("lat")
                            .internalName("lat")
                            .build(),
                    ViewColumnDto.builder()
                            .name("lng")
                            .internalName("lng")
                            .build())))
            .build();

    public static final String QUEUE_NAME = "dbrepo";
    public static final String QUEUE_VHOST = "dbrepo";
    public static final Boolean QUEUE_AUTO_DELETE = false;
    public static final Boolean QUEUE_DURABLE = true;
    public static final Boolean QUEUE_EXCLUSIVE = false;
    public static final String QUEUE_TYPE = "quorum";

    public static final QueueDto QUEUE_DTO = QueueDto.builder()
            .name(QUEUE_NAME)
            .vhost(QUEUE_VHOST)
            .autoDelete(QUEUE_AUTO_DELETE)
            .durable(QUEUE_DURABLE)
            .exclusive(QUEUE_EXCLUSIVE)
            .type(QUEUE_TYPE)
            .build();

    public static final UUID ONTOLOGY_1_ID = UUID.fromString("dc195d01-0a45-4583-aa83-fd270b874353");
    public static final String ONTOLOGY_1_PREFIX = "om2";
    public static final String ONTOLOGY_1_NEW_PREFIX = "om-2";
    public static final String ONTOLOGY_1_URI = "http://www.ontology-of-units-of-measure.org/resource/om-2/";
    public static final String ONTOLOGY_1_URI_PATTERN = "http://www.ontology-of-units-of-measure.org/resource/om-2/.*";
    public static final String ONTOLOGY_1_SPARQL_ENDPOINT = null;
    public static final Boolean ONTOLOGY_1_SPARQL = false;
    public static final String ONTOLOGY_1_RDF_PATH = "rdf/om-2.0.rdf";
    public static final Boolean ONTOLOGY_1_RDF = true;
    public static final UUID ONTOLOGY_1_CREATED_BY = USER_1_ID;

    public static final Ontology ONTOLOGY_1 = Ontology.builder()
            .id(ONTOLOGY_1_ID)
            .prefix(ONTOLOGY_1_PREFIX)
            .uri(ONTOLOGY_1_URI)
            .uriPattern(ONTOLOGY_1_URI_PATTERN)
            .sparqlEndpoint(ONTOLOGY_1_SPARQL_ENDPOINT)
            .rdfPath(ONTOLOGY_1_RDF_PATH)
            .build();

    public static final OntologyDto ONTOLOGY_1_DTO = OntologyDto.builder()
            .id(ONTOLOGY_1_ID)
            .prefix(ONTOLOGY_1_PREFIX)
            .uri(ONTOLOGY_1_URI)
            .uriPattern(ONTOLOGY_1_URI_PATTERN)
            .sparqlEndpoint(ONTOLOGY_1_SPARQL_ENDPOINT)
            .sparql(ONTOLOGY_1_SPARQL)
            .rdfPath(ONTOLOGY_1_RDF_PATH)
            .rdf(ONTOLOGY_1_RDF)
            .build();

    public static final OntologyBriefDto ONTOLOGY_1_BRIEF_DTO = OntologyBriefDto.builder()
            .id(ONTOLOGY_1_ID)
            .prefix(ONTOLOGY_1_PREFIX)
            .uri(ONTOLOGY_1_URI)
            .uriPattern(ONTOLOGY_1_URI_PATTERN)
            .sparql(ONTOLOGY_1_SPARQL)
            .rdf(ONTOLOGY_1_RDF)
            .build();

    public static final OntologyCreateDto ONTOLOGY_1_CREATE_DTO = OntologyCreateDto.builder()
            .prefix(ONTOLOGY_1_PREFIX)
            .uri(ONTOLOGY_1_URI)
            .sparqlEndpoint(ONTOLOGY_1_SPARQL_ENDPOINT)
            .build();

    public static final OntologyModifyDto ONTOLOGY_1_MODIFY_DTO = OntologyModifyDto.builder()
            .prefix(ONTOLOGY_1_NEW_PREFIX)
            .uri(ONTOLOGY_1_URI)
            .sparqlEndpoint(ONTOLOGY_1_SPARQL_ENDPOINT)
            .build();

    public static final UUID ONTOLOGY_2_ID = UUID.fromString("41d902a1-f9f8-4d51-ad64-618b72acf5ed");
    public static final String ONTOLOGY_2_PREFIX = "wd";
    public static final String ONTOLOGY_2_URI = "http://www.wikidata.org/";
    public static final String ONTOLOGY_2_SPARQL_ENDPOINT = "https://query.wikidata.org/sparql";
    public static final UUID ONTOLOGY_2_CREATED_BY = USER_1_ID;

    public static final Ontology ONTOLOGY_2 = Ontology.builder()
            .id(ONTOLOGY_2_ID)
            .prefix(ONTOLOGY_2_PREFIX)
            .uri(ONTOLOGY_2_URI)
            .sparqlEndpoint(ONTOLOGY_2_SPARQL_ENDPOINT)
            .build();

    public static final OntologyCreateDto ONTOLOGY_2_CREATE_DTO = OntologyCreateDto.builder()
            .prefix(ONTOLOGY_2_PREFIX)
            .uri(ONTOLOGY_2_URI)
            .sparqlEndpoint(ONTOLOGY_2_SPARQL_ENDPOINT)
            .build();

    public static final UUID ONTOLOGY_3_ID = UUID.fromString("5b41390b-d2d2-45c6-8038-1258c4b2725f");
    public static final String ONTOLOGY_3_PREFIX = "rdfs";
    public static final String ONTOLOGY_3_URI = "http://www.w3.org/2000/01/rdf-schema#";
    public static final String ONTOLOGY_3_SPARQL_ENDPOINT = null;
    public static final UUID ONTOLOGY_3_CREATED_BY = USER_1_ID;

    public static final Ontology ONTOLOGY_3 = Ontology.builder()
            .id(ONTOLOGY_3_ID)
            .prefix(ONTOLOGY_3_PREFIX)
            .uri(ONTOLOGY_3_URI)
            .sparqlEndpoint(ONTOLOGY_3_SPARQL_ENDPOINT)
            .build();

    public static final OntologyCreateDto ONTOLOGY_3_CREATE_DTO = OntologyCreateDto.builder()
            .prefix(ONTOLOGY_3_PREFIX)
            .uri(ONTOLOGY_3_URI)
            .sparqlEndpoint(ONTOLOGY_3_SPARQL_ENDPOINT)
            .build();

    public static final UUID ONTOLOGY_4_ID = UUID.fromString("d6992475-9b71-4a4a-a6eb-bc1fe6a34443");
    public static final String ONTOLOGY_4_PREFIX = "schema";
    public static final String ONTOLOGY_4_URI = "http://schema.org/";
    public static final String ONTOLOGY_4_SPARQL_ENDPOINT = null;
    public static final UUID ONTOLOGY_4_CREATED_BY = USER_1_ID;

    public static final Ontology ONTOLOGY_4 = Ontology.builder()
            .id(ONTOLOGY_4_ID)
            .prefix(ONTOLOGY_4_PREFIX)
            .uri(ONTOLOGY_4_URI)
            .sparqlEndpoint(ONTOLOGY_4_SPARQL_ENDPOINT)
            .build();

    public static final OntologyCreateDto ONTOLOGY_4_CREATE_DTO = OntologyCreateDto.builder()
            .prefix(ONTOLOGY_4_PREFIX)
            .uri(ONTOLOGY_4_URI)
            .sparqlEndpoint(ONTOLOGY_4_SPARQL_ENDPOINT)
            .build();

    public static final UUID ONTOLOGY_5_ID = UUID.fromString("f95d1330-762e-4f5a-875a-3c64da5808a1");
    public static final String ONTOLOGY_5_PREFIX = "db";
    public static final String ONTOLOGY_5_URI = "http://dbpedia.org";
    public static final String ONTOLOGY_5_SPARQL_ENDPOINT = "http://dbpedia.org/sparql";
    public static final UUID ONTOLOGY_5_CREATED_BY = USER_1_ID;

    public static final Ontology ONTOLOGY_5 = Ontology.builder()
            .id(ONTOLOGY_5_ID)
            .prefix(ONTOLOGY_5_PREFIX)
            .uri(ONTOLOGY_5_URI)
            .sparqlEndpoint(ONTOLOGY_5_SPARQL_ENDPOINT)
            .build();

    public static final OntologyCreateDto ONTOLOGY_5_CREATE_DTO = OntologyCreateDto.builder()
            .prefix(ONTOLOGY_5_PREFIX)
            .uri(ONTOLOGY_5_URI)
            .sparqlEndpoint(ONTOLOGY_5_SPARQL_ENDPOINT)
            .build();

    public static final UUID COLUMN_8_1_ID = UUID.fromString("af362ac6-5dbb-4ede-83ea-5d94b39641c8");
    public static final Integer COLUMN_8_1_ORDINALPOS = 0;
    public static final String COLUMN_8_1_NAME = "ID";
    public static final String COLUMN_8_1_INTERNAL_NAME = "id";
    public static final TableColumnType COLUMN_8_1_TYPE = TableColumnType.BIGINT;
    public static final ColumnTypeDto COLUMN_8_1_TYPE_DTO = ColumnTypeDto.BIGINT;
    public static final Boolean COLUMN_8_1_NULL = false;
    public static final Boolean COLUMN_8_1_AUTO_GENERATED = true;

    public static final UUID COLUMN_8_2_ID = UUID.fromString("7ada597b-0766-4612-9ace-67eeee94e2da");
    public static final Integer COLUMN_8_2_ORDINALPOS = 1;
    public static final String COLUMN_8_2_NAME = "Value";
    public static final String COLUMN_8_2_INTERNAL_NAME = "value";
    public static final TableColumnType COLUMN_8_2_TYPE = TableColumnType.DECIMAL;
    public static final ColumnTypeDto COLUMN_8_2_TYPE_DTO = ColumnTypeDto.DECIMAL;
    public static final Long COLUMN_8_2_SIZE = 10L;
    public static final Long COLUMN_8_2_D = 10L;
    public static final Boolean COLUMN_8_2_NULL = false;
    public static final Boolean COLUMN_8_2_AUTO_GENERATED = false;

    public static final UUID COLUMN_8_3_ID = UUID.fromString("8bcd9ef8-f7b8-4730-acc1-a3d43ba69a56");
    public static final Integer COLUMN_8_3_ORDINALPOS = 2;
    public static final String COLUMN_8_3_NAME = "raw";
    public static final String COLUMN_8_3_INTERNAL_NAME = "raw";
    public static final TableColumnType COLUMN_8_3_TYPE = TableColumnType.LONGBLOB;
    public static final ColumnTypeDto COLUMN_8_3_TYPE_DTO = ColumnTypeDto.LONGBLOB;
    public static final Boolean COLUMN_8_3_NULL = true;
    public static final Boolean COLUMN_8_3_AUTO_GENERATED = false;

    public static final ColumnBriefDto TABLE_8_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(COLUMN_8_1_ID)
            .name(COLUMN_8_1_NAME)
            .internalName(COLUMN_8_1_INTERNAL_NAME)
            .columnType(ColumnTypeDto.BIGINT)
            .build();

    public static final List<TableColumn> TABLE_8_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_8_1_ID)
                    .ordinalPosition(COLUMN_8_1_ORDINALPOS)
                    .table(TABLE_8)
                    .name(COLUMN_8_1_NAME)
                    .internalName(COLUMN_8_1_INTERNAL_NAME)
                    .columnType(COLUMN_8_1_TYPE)
                    .isNullAllowed(COLUMN_8_1_NULL)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_8_2_ID)
                    .ordinalPosition(COLUMN_8_2_ORDINALPOS)
                    .table(TABLE_8)
                    .name(COLUMN_8_2_NAME)
                    .internalName(COLUMN_8_2_INTERNAL_NAME)
                    .columnType(COLUMN_8_2_TYPE)
                    .isNullAllowed(COLUMN_8_2_NULL)
                    .size(COLUMN_8_2_SIZE)
                    .d(COLUMN_8_2_D)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_8_3_ID)
                    .ordinalPosition(COLUMN_8_3_ORDINALPOS)
                    .table(TABLE_8)
                    .name(COLUMN_8_3_NAME)
                    .internalName(COLUMN_8_3_INTERNAL_NAME)
                    .columnType(COLUMN_8_3_TYPE)
                    .isNullAllowed(COLUMN_8_3_NULL)
                    .build());

    public static final List<ColumnDto> TABLE_8_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_8_1_ID)
                    .ordinalPosition(COLUMN_8_1_ORDINALPOS)
                    .name(COLUMN_8_1_NAME)
                    .internalName(COLUMN_8_1_INTERNAL_NAME)
                    .columnType(COLUMN_8_1_TYPE_DTO)
                    .isNullAllowed(COLUMN_8_1_NULL)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_8_2_ID)
                    .ordinalPosition(COLUMN_8_2_ORDINALPOS)
                    .name(COLUMN_8_2_NAME)
                    .internalName(COLUMN_8_2_INTERNAL_NAME)
                    .columnType(COLUMN_8_2_TYPE_DTO)
                    .isNullAllowed(COLUMN_8_2_NULL)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_8_3_ID)
                    .ordinalPosition(COLUMN_8_3_ORDINALPOS)
                    .name(COLUMN_8_3_NAME)
                    .internalName(COLUMN_8_3_INTERNAL_NAME)
                    .columnType(COLUMN_8_3_TYPE_DTO)
                    .isNullAllowed(COLUMN_8_3_NULL)
                    .build());

    public static final Long TABLE_8_DATA_COUNT = 6L;
    @SuppressWarnings("java:S3599")
    public static final List<Map<String, Object>> TABLE_8_DATA_DTO = new LinkedList<>(List.of(
            new HashMap<>() {{
                put(COLUMN_8_1_INTERNAL_NAME, BigInteger.valueOf(1L));
                put(COLUMN_8_2_INTERNAL_NAME, 11.2);
                put(COLUMN_8_3_INTERNAL_NAME, null);
            }},
            new HashMap<>() {{
                put(COLUMN_8_1_INTERNAL_NAME, BigInteger.valueOf(2L));
                put(COLUMN_8_2_INTERNAL_NAME, 11.3);
                put(COLUMN_8_3_INTERNAL_NAME, null);
            }},
            new HashMap<>() {{
                put(COLUMN_8_1_INTERNAL_NAME, BigInteger.valueOf(3L));
                put(COLUMN_8_2_INTERNAL_NAME, 11.4);
                put(COLUMN_8_3_INTERNAL_NAME, null);
            }},
            new HashMap<>() {{
                put(COLUMN_8_1_INTERNAL_NAME, BigInteger.valueOf(4L));
                put(COLUMN_8_2_INTERNAL_NAME, 11.9);
                put(COLUMN_8_3_INTERNAL_NAME, null);
            }},
            new HashMap<>() {{
                put(COLUMN_8_1_INTERNAL_NAME, BigInteger.valueOf(5L));
                put(COLUMN_8_2_INTERNAL_NAME, 12.3);
                put(COLUMN_8_3_INTERNAL_NAME, null);
            }},
            new HashMap<>() {{
                put(COLUMN_8_1_INTERNAL_NAME, BigInteger.valueOf(6L));
                put(COLUMN_8_2_INTERNAL_NAME, 23.1);
                put(COLUMN_8_3_INTERNAL_NAME, null);
            }}
    ));

    @SuppressWarnings("java:S3599")
    public static final TableStatisticDto TABLE_8_STATISTIC_DTO = TableStatisticDto.builder()
            .columns(new HashMap<>() {{
                put(COLUMN_8_1_INTERNAL_NAME, ColumnStatisticDto.builder()
                        .min(BigDecimal.valueOf(11.2))
                        .max(BigDecimal.valueOf(23.1))
                        .mean(BigDecimal.valueOf(13.5333))
                        .median(BigDecimal.valueOf(11.4))
                        .stdDev(BigDecimal.valueOf(4.2952))
                        .build());
            }})
            .build();

    public static final UUID QUERY_1_ID = UUID.fromString("60494137-f000-459e-acd3-4fcadbdf14ca");
    public static final String QUERY_1_STATEMENT = "SELECT `id`, `date`, `location`, `mintemp`, `rainfall` FROM `weather_aus` ORDER BY id ASC";
    public static final String QUERY_1_DOI = null;
    public static final Long QUERY_1_RESULT_NUMBER = 2L;
    public static final String QUERY_1_QUERY_HASH = "a3b8ac39e38167d14cf3a9c20a69e4b6954d049525390b973a2c23064953a992";
    public static final String QUERY_1_RESULT_HASH = "8358c8ade4849d2094ab5bb29127afdae57e6bb5acb1db7af603813d406c467a";
    public static final Instant QUERY_1_CREATED = Instant.ofEpochSecond(1677648377L);
    public static final Instant QUERY_1_EXECUTION = Instant.now();
    public static final Boolean QUERY_1_PERSISTED = true;

    public static final SubsetDto QUERY_1_SUBSET_DTO = SubsetDto.builder()
            .tableId(TABLE_1_ID)
            .columns(new LinkedList<UUID>(List.of(COLUMN_1_1_ID, COLUMN_1_2_ID, COLUMN_1_3_ID, COLUMN_1_4_ID, COLUMN_1_5_ID)))
            .order(new LinkedList<OrderDto>(List.of(OrderDto.builder()
                    .columnId(COLUMN_1_1_ID)
                    .direction(OrderTypeDto.ASC)
                    .build())))
            .build();

    public static final ViewDto QUERY_1_VIEW_DTO = ViewDto.builder()
            .query(QUERY_1_STATEMENT)
            .queryHash(QUERY_1_QUERY_HASH)
            .owner(USER_1_BRIEF_DTO)
            .columns(new LinkedList<>(List.of(ViewColumnDto.builder()
                            .name("id")
                            .internalName("id")
                            .build(),
                    ViewColumnDto.builder()
                            .name("date")
                            .internalName("date")
                            .build(),
                    ViewColumnDto.builder()
                            .name("location")
                            .internalName("location")
                            .build(),
                    ViewColumnDto.builder()
                            .name("mintemp")
                            .internalName("mintemp")
                            .build(),
                    ViewColumnDto.builder()
                            .name("rainfall")
                            .internalName("rainfall")
                            .build())))
            .build();

    public static final QueryBriefDto QUERY_1_BRIEF_DTO = QueryBriefDto.builder()
            .id(QUERY_1_ID)
            .databaseId(DATABASE_1_ID)
            .query(QUERY_1_STATEMENT)
            .queryHash(QUERY_1_QUERY_HASH)
            .resultHash(QUERY_1_RESULT_HASH)
            .execution(QUERY_1_EXECUTION)
            .owner(USER_1_BRIEF_DTO)
            .isPersisted(QUERY_1_PERSISTED)
            .resultNumber(3L)
            .build();

    public static final UUID QUERY_2_ID = UUID.fromString("4e0ac92a-7cb3-4222-9b85-0498c73e0afd");
    public static final String QUERY_2_STATEMENT = "SELECT `location` FROM `weather_aus`";
    public static final String QUERY_2_QUERY_HASH = "a2d2dd94ebc7653bb5a3b55dd8ed5e91d3d13c225c6855a1eb4eb7ca14c36ced";
    public static final Long QUERY_2_RESULT_NUMBER = 2L;
    public static final String QUERY_2_RESULT_HASH = "ff3f7cbe1b96d296957f6e39e55b8b1b577fa3d205d4795af99594cfd20cb80d";
    public static final Instant QUERY_2_CREATED = Instant.now().minus(2, MINUTES);
    public static final Instant QUERY_2_EXECUTION = Instant.now().minus(1, MINUTES);
    public static final Instant QUERY_2_LAST_MODIFIED = Instant.ofEpochSecond(1541588352L);
    public static final Boolean QUERY_2_PERSISTED = false;

    public static final UUID QUERY_3_ID = UUID.fromString("a9849020-45a7-40a8-9a19-d4ae2b28dd46");
    public static final String QUERY_3_STATEMENT = "SELECT `location`, `mintemp` FROM `weather_aus` WHERE `mintemp` > 10";
    public static final String QUERY_3_QUERY_HASH = "a3d3dd94ebc7653bb5a3b55dd8ed5e91d3d13c335c6855a1eb4eb7ca14c36ced";
    public static final String QUERY_3_RESULT_HASH = "ff3f7cbe1b96d396957f6e39e55b8b1b577fa3d305d4795af99594cfd30cb80d";
    public static final Instant QUERY_3_CREATED = Instant.now().minus(3, MINUTES);
    public static final Instant QUERY_3_EXECUTION = Instant.now().minus(1, MINUTES);
    public static final Instant QUERY_3_LAST_MODIFIED = Instant.ofEpochSecond(1541588353L);
    public static final Long QUERY_3_RESULT_NUMBER = 2L;
    public static final Boolean QUERY_3_PERSISTED = true;

    public static final UUID QUERY_7_ID = UUID.fromString("fe73a325-30a0-444c-b74f-23ce1533e55f");
    public static final String QUERY_7_STATEMENT = "SELECT id, date, a.location, lat, lng FROM weather_aus a JOIN weather_location l on a.location = l.location WHERE date = '2008-12-01'";
    public static final String QUERY_7_QUERY_HASH = "df7da3801dfb5c191ff6711d79ce6455f3c09ec8323ce1ff7208ab85387263f5";
    public static final String QUERY_7_RESULT_HASH = "ff4f7cbe1b96d496957f6e49e55b8b1b577fa4d405d4795af99594cfd40cb80d";
    public static final Instant QUERY_7_CREATED = Instant.now().minus(4, MINUTES);
    public static final Instant QUERY_7_EXECUTION = Instant.now().minus(1, MINUTES);
    public static final Instant QUERY_7_LAST_MODIFIED = Instant.ofEpochSecond(1541588454L);
    public static final Long QUERY_7_RESULT_NUMBER = 6L;
    public static final Long QUERY_7_RESULT_ID = 4L;
    public static final Boolean QUERY_7_PERSISTED = false;

    public static final UUID QUERY_4_ID = UUID.fromString("18a98197-51ff-4011-9f40-914a11675a6d");
    public static final String QUERY_4_STATEMENT = "SELECT `id`, `value` FROM `mfcc`";
    public static final String QUERY_4_QUERY_HASH = "df7da3801dfb5c191ff6711d79ce6455f3c09ec8323ce1ff7208ab85387263f5";
    public static final String QUERY_4_RESULT_HASH = "ff4f7cbe1b96d496957f6e49e55b8b1b577fa4d405d4795af99594cfd40cb80d";
    public static final Instant QUERY_4_CREATED = Instant.now().minus(4, MINUTES);
    public static final Instant QUERY_4_EXECUTION = Instant.now().minus(1, MINUTES);
    public static final Instant QUERY_4_LAST_MODIFIED = Instant.ofEpochSecond(1541588454L);
    public static final Long QUERY_4_RESULT_NUMBER = 6L;
    public static final Long QUERY_4_RESULT_ID = 4L;
    public static final Boolean QUERY_4_PERSISTED = false;

    public static final List<Map<String, Object>> QUERY_4_RESULT_DTO = new LinkedList<>(List.of(
            new HashMap<>() {{
                put("id", BigInteger.valueOf(1L));
                put("value", 11.2);
            }}, new HashMap<>() {{
                put("id", BigInteger.valueOf(2L));
                put("value", 11.3);
            }}, new HashMap<>() {{
                put("id", BigInteger.valueOf(3L));
                put("value", 11.4);
            }}, new HashMap<>() {{
                put("id", BigInteger.valueOf(4L));
                put("value", 11.9);
            }}, new HashMap<>() {{
                put("id", BigInteger.valueOf(5L));
                put("value", 12.3);
            }}, new HashMap<>() {{
                put("id", BigInteger.valueOf(6L));
                put("value", 23.1);
            }}));

    public static final QueryDto QUERY_4_DTO = QueryDto.builder()
            .id(QUERY_4_ID)
            .databaseId(DATABASE_3_ID)
            .query(QUERY_4_STATEMENT)
            .queryNormalized(QUERY_4_STATEMENT)
            .resultNumber(QUERY_4_RESULT_NUMBER)
            .resultHash(QUERY_4_RESULT_HASH)
            .queryHash(QUERY_4_QUERY_HASH)
            .execution(QUERY_4_EXECUTION)
            .isPersisted(QUERY_4_PERSISTED)
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final UUID QUERY_5_ID = UUID.fromString("1a39f775-e3d5-4865-b4f5-dbbb5693b637");
    public static final String QUERY_5_STATEMENT = "SELECT `id`, `value` FROM `mfcc` WHERE `value` > 0";
    public static final String QUERY_5_QUERY_HASH = "6d6dc48b12cdfd959d39a62887334a6bbd529b93eed4f211f3f671bd9e7d6225";
    public static final String QUERY_5_RESULT_HASH = "ff5f7cbe1b96d596957f6e59e55b8b1b577fa5d505d5795af99595cfd50cb80d";
    public static final Instant QUERY_5_CREATED = Instant.now().minus(5, MINUTES);
    public static final Instant QUERY_5_EXECUTION = Instant.now().minus(1, MINUTES);
    public static final Instant QUERY_5_LAST_MODIFIED = Instant.ofEpochSecond(1551588555L);
    public static final Long QUERY_5_RESULT_NUMBER = 6L;
    public static final Boolean QUERY_5_PERSISTED = true;

    public static final QueryDto QUERY_5_DTO = QueryDto.builder()
            .id(QUERY_5_ID)
            .databaseId(DATABASE_3_ID)
            .query(QUERY_5_STATEMENT)
            .queryNormalized(QUERY_5_STATEMENT)
            .resultNumber(QUERY_5_RESULT_NUMBER)
            .resultHash(QUERY_5_RESULT_HASH)
            .queryHash(QUERY_5_QUERY_HASH)
            .execution(QUERY_5_EXECUTION)
            .isPersisted(QUERY_5_PERSISTED)
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final SubsetDto QUERY_5_SUBSET_DTO = SubsetDto.builder()
            .tableId(TABLE_8_ID)
            .columns(new LinkedList<>(List.of(COLUMN_8_1_ID, COLUMN_8_2_ID)))
            .filter(new LinkedList<>(List.of(FilterDto.builder()
                    .columnId(COLUMN_8_2_ID)
                    .operatorId(IMAGE_1_OPERATORS_2_ID)
                    .value("0")
                    .type(FilterTypeDto.WHERE)
                    .build())))
            .build();

    public static final ViewDto QUERY_5_VIEW_DTO = ViewDto.builder()
            .query(QUERY_5_STATEMENT)
            .queryHash(QUERY_5_QUERY_HASH)
            .owner(USER_1_BRIEF_DTO)
            .columns(new LinkedList<>(List.of(ViewColumnDto.builder()
                            .name("id")
                            .internalName("id")
                            .build(),
                    ViewColumnDto.builder()
                            .name("value")
                            .internalName("value")
                            .build())))
            .build();

    public static final List<Map<String, Object>> QUERY_5_RESULT_DTO = new LinkedList<>(List.of(
            Map.of("id", BigInteger.valueOf(1L), "value", 11.2),
            Map.of("id", BigInteger.valueOf(2L), "value", 11.3),
            Map.of("id", BigInteger.valueOf(3L), "value", 11.4),
            Map.of("id", BigInteger.valueOf(4L), "value", 11.9),
            Map.of("id", BigInteger.valueOf(5L), "value", 12.3),
            Map.of("id", BigInteger.valueOf(6L), "value", 23.1)
    ));

    public static final UUID QUERY_6_ID = UUID.fromString("7463412a-20c4-4fc1-8a33-948aea026f49");
    public static final String QUERY_6_STATEMENT = "SELECT `location` FROM `weather_aus` WHERE `id` = 1";
    public static final String QUERY_6_QUERY_HASH = "6d6dc48b12cdfd959d39a62887334a6bbd529b93eed4f211f3f671bd9e7d6225";
    public static final String QUERY_6_RESULT_HASH = "ff5f7cbe1b96d596957f6e59e55b8b1b577fa5d505d5795af99595cfd50cb80d";
    public static final Instant QUERY_6_CREATED = Instant.now().minus(5, MINUTES);
    public static final Instant QUERY_6_EXECUTION = Instant.now().minus(1, MINUTES);
    public static final Instant QUERY_6_LAST_MODIFIED = Instant.ofEpochSecond(1551588555L);
    public static final Long QUERY_6_RESULT_NUMBER = 1L;
    public static final Boolean QUERY_6_PERSISTED = true;

    public static final List<TableColumn> TABLE_1_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_1_1_ID)
                    .ordinalPosition(0)
                    .table(TABLE_1)
                    .name("id")
                    .internalName("id")
                    .columnType(TableColumnType.SERIAL)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_1_2_ID)
                    .ordinalPosition(1)
                    .table(TABLE_1)
                    .name("Date")
                    .internalName("date")
                    .columnType(TableColumnType.DATE)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_1_3_ID)
                    .ordinalPosition(2)
                    .table(TABLE_1)
                    .name("Location")
                    .internalName("location")
                    .columnType(TableColumnType.VARCHAR)
                    .size(255L)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_1_4_ID)
                    .ordinalPosition(3)
                    .table(TABLE_1)
                    .name("MinTemp")
                    .internalName("mintemp")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_1_5_ID)
                    .ordinalPosition(4)
                    .table(TABLE_1)
                    .name("Rainfall")
                    .internalName("rainfall")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .concept(CONCEPT_1)
                    .unit(UNIT_1)
                    .isNullAllowed(true)
                    .build());

    public static final ColumnBriefDto TABLE_1_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(COLUMN_1_1_ID)
            .name("id")
            .internalName("id")
            .columnType(ColumnTypeDto.BIGINT)
            .build();

    public static final List<CreateTableColumnDto> TABLE_1_COLUMNS_CREATE_DTO = List.of(CreateTableColumnDto.builder()
                    .name("id")
                    .type(ColumnTypeDto.BIGINT)
                    .nullAllowed(false)
                    .enums(null)
                    .sets(null)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Date")
                    .type(ColumnTypeDto.DATE)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Location")
                    .type(ColumnTypeDto.VARCHAR)
                    .size(255L)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("MinTemp")
                    .type(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Rainfall")
                    .type(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .nullAllowed(true)
                    .conceptUri(CONCEPT_1_URI)
                    .unitUri(UNIT_1_URI)
                    .build());

    public static final CreateTableConstraintsDto TABLE_1_CONSTRAINTS_CREATE_DTO = CreateTableConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .primaryKey(new LinkedHashSet<>(List.of("id")))
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>(List.of(List.of("date"))))
            .build();

    public static final CreateTableConstraintsDto TABLE_1_CONSTRAINTS_CREATE_INVALID_DTO = CreateTableConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .primaryKey(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>(List.of(List.of("date"))))
            .build();

    public static final CreateTableDto TABLE_1_CREATE_DTO = CreateTableDto.builder()
            .name(TABLE_1_NAME)
            .description(TABLE_1_DESCRIPTION)
            .columns(TABLE_1_COLUMNS_CREATE_DTO)
            .constraints(TABLE_1_CONSTRAINTS_CREATE_DTO)
            .build();

    public static final at.tuwien.api.database.table.internal.TableCreateDto TABLE_1_CREATE_INTERNAL_DTO = at.tuwien.api.database.table.internal.TableCreateDto.builder()
            .name(TABLE_1_NAME)
            .description(TABLE_1_DESCRIPTION)
            .columns(TABLE_1_COLUMNS_CREATE_DTO)
            .constraints(TABLE_1_CONSTRAINTS_CREATE_DTO)
            .build();

    public static final at.tuwien.api.database.table.internal.TableCreateDto TABLE_1_CREATE_INTERNAL_INVALID_DTO = at.tuwien.api.database.table.internal.TableCreateDto.builder()
            .name(TABLE_1_NAME)
            .description(TABLE_1_DESCRIPTION)
            .columns(TABLE_1_COLUMNS_CREATE_DTO)
            .constraints(TABLE_1_CONSTRAINTS_CREATE_INVALID_DTO)
            .build();

    public static final UUID COLUMN_2_1_ID = UUID.fromString("795faa78-7ebb-4dd5-9eb1-e54a9192d0b5");

    public static final UUID COLUMN_2_2_ID = UUID.fromString("f316ced5-7774-4656-aa7f-a874622d99b3");

    public static final UUID COLUMN_2_3_ID = UUID.fromString("11cb1aa2-8582-45ef-a3bb-7056aa94cdf1");

    public static final List<TableColumn> TABLE_2_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_2_1_ID)
                    .ordinalPosition(0)
                    .table(TABLE_2)
                    .name("location")
                    .internalName("location")
                    .columnType(TableColumnType.VARCHAR)
                    .size(255L)
                    .isNullAllowed(false)
                    .enums(null)
                    .sets(null)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_2_2_ID)
                    .ordinalPosition(1)
                    .table(TABLE_2)
                    .name("lat")
                    .internalName("lat")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_2_3_ID)
                    .ordinalPosition(2)
                    .table(TABLE_2)
                    .name("lng")
                    .internalName("lng")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build());

    public static final ColumnBriefDto TABLE_2_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(COLUMN_2_1_ID)
            .name("location")
            .internalName("location")
            .columnType(ColumnTypeDto.VARCHAR)
            .build();

    public static final ColumnBriefDto TABLE_2_COLUMNS_BRIEF_2_DTO = ColumnBriefDto.builder()
            .id(COLUMN_2_3_ID)
            .name("lng")
            .internalName("lng")
            .columnType(ColumnTypeDto.DECIMAL)
            .build();

    public static final List<ColumnDto> TABLE_2_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_2_1_ID)
                    .tableId(TABLE_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(0)
                    .name("location")
                    .internalName("location")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .size(255L)
                    .isNullAllowed(false)
                    .enums(null)
                    .sets(null)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_2_2_ID)
                    .tableId(TABLE_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(1)
                    .name("lat")
                    .internalName("lat")
                    .columnType(ColumnTypeDto.DOUBLE)
                    .size(22L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_2_3_ID)
                    .tableId(TABLE_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(2)
                    .name("lng")
                    .internalName("lng")
                    .columnType(ColumnTypeDto.DOUBLE)
                    .size(22L)
                    .isNullAllowed(true)
                    .enums(null)
                    .sets(null)
                    .build());

    public static final List<ColumnBriefDto> TABLE_2_COLUMNS_BRIEF_DTO = List.of(ColumnBriefDto.builder()
                    .id(COLUMN_2_1_ID)
                    .tableId(TABLE_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .name("location")
                    .internalName("location")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .build(),
            ColumnBriefDto.builder()
                    .id(COLUMN_2_2_ID)
                    .tableId(TABLE_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .name("lat")
                    .internalName("lat")
                    .columnType(ColumnTypeDto.DOUBLE)
                    .build(),
            ColumnBriefDto.builder()
                    .id(COLUMN_2_3_ID)
                    .tableId(TABLE_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .name("lng")
                    .internalName("lng")
                    .columnType(ColumnTypeDto.DOUBLE)
                    .build());

    public static final UUID COLUMN_3_1_ID = UUID.fromString("49cc2735-ba75-4e12-8ac7-8aec87ed7724");

    public static final UUID COLUMN_3_2_ID = UUID.fromString("2c240d64-3052-4a74-b696-e7490fdff3ea");

    public static final UUID COLUMN_3_3_ID = UUID.fromString("6fbb0a56-f23a-4aa4-b158-c614a0a30f86");

    public static final UUID COLUMN_3_4_ID = UUID.fromString("9b01f925-93ee-4f28-bf31-9902900a7099");

    public static final UUID COLUMN_3_5_ID = UUID.fromString("9bbd66f1-0d94-401c-b7f7-6e329bb9ee21");

    public static final UUID COLUMN_3_6_ID = UUID.fromString("19ad93d7-b298-495b-9678-9aac80678ff9");

    public static final UUID COLUMN_3_7_ID = UUID.fromString("4d27d9f4-645f-4222-b5a8-4a91fa6e4275");

    public static final UUID COLUMN_3_8_ID = UUID.fromString("b4f8fcf8-5824-45ec-8c58-43f20e6dffc5");

    public static final UUID COLUMN_3_9_ID = UUID.fromString("87247218-369e-484a-9a8f-d758478d8dfc");

    public static final UUID COLUMN_3_10_ID = UUID.fromString("6e191b97-189a-4d88-901e-888ca889e280");

    public static final UUID COLUMN_3_11_ID = UUID.fromString("6ac356ff-9be5-4259-9b62-83b6707be7fe");

    public static final UUID COLUMN_3_12_ID = UUID.fromString("0665b384-c824-4358-b6c5-f17706d46ea4");

    public static final UUID COLUMN_3_13_ID = UUID.fromString("22d3676e-d28e-4075-b223-91a7ac767bcf");

    public static final UUID COLUMN_3_14_ID = UUID.fromString("673326e3-ee2b-4c2f-902f-982e2abce1c2");

    public static final UUID COLUMN_3_15_ID = UUID.fromString("8dcacf4a-736b-4e67-9618-74998cba8940");

    public static final UUID COLUMN_3_16_ID = UUID.fromString("2b2f5359-76d3-4763-a53f-d18ca6b793fb");

    public static final UUID COLUMN_3_17_ID = UUID.fromString("674b6120-06cf-4624-b006-1ed48898bd69");

    public static final UUID COLUMN_3_18_ID = UUID.fromString("13edd7c9-6c88-44d7-b206-34774e49c5af");

    public static final UUID COLUMN_3_19_ID = UUID.fromString("6977bb3f-4ae2-43ea-bb82-c7f68454c538");

    public static final UUID COLUMN_3_20_ID = UUID.fromString("c03d2429-53e1-42eb-a1f5-ce342fa23336");

    public static final UUID COLUMN_3_21_ID = UUID.fromString("06edd332-750e-4aa1-b61b-e757fb2312c3");

    public static final UUID COLUMN_3_22_ID = UUID.fromString("b6b8631d-f283-49da-8d5e-4bb24def2a40");

    public static final UUID COLUMN_3_23_ID = UUID.fromString("0393ee00-31ba-44ab-9e82-1f5034a9f57b");

    public static final UUID COLUMN_3_24_ID = UUID.fromString("a63784ea-f70d-4bda-ace6-1c6a88edf831");

    public static final UUID COLUMN_3_25_ID = UUID.fromString("720fe829-802c-420b-8e41-bdbb636db43c");

    public static final UUID COLUMN_3_26_ID = UUID.fromString("5bce38ef-7d49-43b5-9054-068750684b5f");

    public static final UUID COLUMN_3_27_ID = UUID.fromString("92097c02-3dd3-40ea-bd03-a9135f45a557");

    public static final UUID COLUMN_3_28_ID = UUID.fromString("7361a38a-828b-495e-8a57-b36cca17d7db");

    public static final UUID COLUMN_3_29_ID = UUID.fromString("a06812db-03b7-484c-92a6-45d94eef3bb9");

    public static final UUID COLUMN_3_30_ID = UUID.fromString("05614d89-9216-47ea-96f0-acffc4674acf");

    public static final UUID COLUMN_3_31_ID = UUID.fromString("05ada13d-361a-48e7-9a0f-1191499509f1");

    public static final UUID COLUMN_3_32_ID = UUID.fromString("b3f259f6-700a-4b60-8eac-dceaa0dcda9d");

    public static final UUID COLUMN_3_33_ID = UUID.fromString("9160af06-e168-4b10-a7f9-520f41ae7955");

    public static final UUID COLUMN_3_34_ID = UUID.fromString("fde20c99-ed9c-4a60-8c18-f46e8603ebb5");

    public static final UUID COLUMN_3_35_ID = UUID.fromString("071c7f27-1cdd-4af9-b4d6-f932c27c7287");

    public static final ColumnBriefDto TABLE_3_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(COLUMN_3_1_ID)
            .columnType(ColumnTypeDto.BIGINT)
            .name("id")
            .internalName("id")
            .build();

    public static final List<TableColumn> TABLE_3_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_3_1_ID)
                    .table(TABLE_3)
                    .ordinalPosition(0)
                    .columnType(TableColumnType.BIGINT)
                    .name("id")
                    .internalName("id")
                    .isNullAllowed(false)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_2_ID)
                    .table(TABLE_3)
                    .ordinalPosition(1)
                    .columnType(TableColumnType.INT)
                    .name("linie")
                    .internalName("linie")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_3_ID)
                    .table(TABLE_3)
                    .ordinalPosition(2)
                    .columnType(TableColumnType.INT)
                    .name("richtung")
                    .internalName("richtung")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_4_ID)
                    .table(TABLE_3)
                    .ordinalPosition(3)
                    .columnType(TableColumnType.DATE)
                    .name("betriebsdatum")
                    .internalName("betriebsdatum")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_5_ID)
                    .table(TABLE_3)
                    .ordinalPosition(4)
                    .columnType(TableColumnType.INT)
                    .name("fahrzeug")
                    .internalName("fahrzeug")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_6_ID)
                    .table(TABLE_3)
                    .ordinalPosition(5)
                    .columnType(TableColumnType.INT)
                    .name("kurs")
                    .internalName("kurs")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_7_ID)
                    .table(TABLE_3)
                    .ordinalPosition(6)
                    .columnType(TableColumnType.INT)
                    .name("seq_von")
                    .internalName("seq_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_8_ID)
                    .table(TABLE_3)
                    .ordinalPosition(7)
                    .columnType(TableColumnType.INT)
                    .name("halt_diva_von")
                    .internalName("halt_diva_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_9_ID)
                    .table(TABLE_3)
                    .ordinalPosition(8)
                    .columnType(TableColumnType.INT)
                    .name("halt_punkt_diva_von")
                    .internalName("halt_punkt_diva_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_10_ID)
                    .table(TABLE_3)
                    .ordinalPosition(9)
                    .columnType(TableColumnType.INT)
                    .name("halt_kurz_von1")
                    .internalName("halt_kurz_von1")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_11_ID)
                    .table(TABLE_3)
                    .ordinalPosition(10)
                    .columnType(TableColumnType.DATE)
                    .name("datum_von")
                    .internalName("datum_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_12_ID)
                    .table(TABLE_3)
                    .ordinalPosition(11)
                    .columnType(TableColumnType.INT)
                    .name("soll_an_von")
                    .internalName("soll_an_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_13_ID)
                    .table(TABLE_3)
                    .ordinalPosition(12)
                    .columnType(TableColumnType.INT)
                    .name("ist_an_von")
                    .internalName("ist_an_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_14_ID)
                    .table(TABLE_3)
                    .ordinalPosition(13)
                    .columnType(TableColumnType.INT)
                    .name("soll_ab_von")
                    .internalName("soll_ab_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_15_ID)
                    .table(TABLE_3)
                    .ordinalPosition(14)
                    .columnType(TableColumnType.INT)
                    .name("ist_ab_von")
                    .internalName("ist_ab_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_16_ID)
                    .table(TABLE_3)
                    .ordinalPosition(15)
                    .columnType(TableColumnType.INT)
                    .name("seq_nach")
                    .internalName("seq_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_17_ID)
                    .table(TABLE_3)
                    .ordinalPosition(16)
                    .columnType(TableColumnType.INT)
                    .name("halt_diva_nach")
                    .internalName("halt_diva_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_18_ID)
                    .table(TABLE_3)
                    .ordinalPosition(17)
                    .columnType(TableColumnType.INT)
                    .name("halt_punkt_diva_nach")
                    .internalName("halt_punkt_diva_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_19_ID)
                    .table(TABLE_3)
                    .ordinalPosition(18)
                    .columnType(TableColumnType.INT)
                    .name("halt_kurz_nach1")
                    .internalName("halt_kurz_nach1")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_20_ID)
                    .table(TABLE_3)
                    .ordinalPosition(19)
                    .columnType(TableColumnType.DATE)
                    .name("datum_nach")
                    .internalName("datum_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_21_ID)
                    .table(TABLE_3)
                    .ordinalPosition(20)
                    .columnType(TableColumnType.INT)
                    .name("soll_an_nach")
                    .internalName("soll_an_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_22_ID)
                    .table(TABLE_3)
                    .ordinalPosition(21)
                    .columnType(TableColumnType.INT)
                    .name("ist_an_nach1")
                    .internalName("ist_an_nach1")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_23_ID)
                    .table(TABLE_3)
                    .ordinalPosition(22)
                    .columnType(TableColumnType.INT)
                    .name("soll_ab_nach")
                    .internalName("soll_ab_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_24_ID)
                    .table(TABLE_3)
                    .ordinalPosition(23)
                    .columnType(TableColumnType.INT)
                    .name("ist_ab_nach")
                    .internalName("ist_ab_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_25_ID)
                    .table(TABLE_3)
                    .ordinalPosition(24)
                    .columnType(TableColumnType.INT)
                    .name("fahrt_id")
                    .internalName("fahrt_id")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_26_ID)
                    .table(TABLE_3)
                    .ordinalPosition(25)
                    .columnType(TableColumnType.INT)
                    .name("fahrweg_id")
                    .internalName("fahrweg_id")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_27_ID)
                    .table(TABLE_3)
                    .ordinalPosition(26)
                    .columnType(TableColumnType.INT)
                    .name("fw_no")
                    .internalName("fw_no")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_28_ID)
                    .table(TABLE_3)
                    .ordinalPosition(27)
                    .columnType(TableColumnType.INT)
                    .name("fw_typ")
                    .internalName("fw_typ")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_29_ID)
                    .table(TABLE_3)
                    .ordinalPosition(28)
                    .columnType(TableColumnType.INT)
                    .name("fw_kurz")
                    .internalName("fw_kurz")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_30_ID)
                    .table(TABLE_3)
                    .ordinalPosition(29)
                    .columnType(TableColumnType.INT)
                    .name("fw_lang")
                    .internalName("fw_lang")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_31_ID)
                    .table(TABLE_3)
                    .ordinalPosition(30)
                    .columnType(TableColumnType.INT)
                    .name("umlauf_von")
                    .internalName("umlauf_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_32_ID)
                    .table(TABLE_3)
                    .ordinalPosition(31)
                    .columnType(TableColumnType.INT)
                    .name("halt_id_von")
                    .internalName("halt_id_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_33_ID)
                    .table(TABLE_3)
                    .ordinalPosition(32)
                    .columnType(TableColumnType.INT)
                    .name("halt_id_nach")
                    .internalName("halt_id_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_34_ID)
                    .table(TABLE_3)
                    .ordinalPosition(33)
                    .columnType(TableColumnType.INT)
                    .name("halt_punkt_id_von")
                    .internalName("halt_punkt_id_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_3_35_ID)
                    .table(TABLE_3)
                    .ordinalPosition(34)
                    .columnType(TableColumnType.INT)
                    .name("halt_punkt_id_nach")
                    .internalName("halt_punkt_id_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build());

    public static final List<ColumnDto> TABLE_3_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_3_1_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.BIGINT)
                    .name("id")
                    .internalName("id")
                    .isNullAllowed(false)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_2_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("linie")
                    .internalName("linie")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_3_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("richtung")
                    .internalName("richtung")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_4_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.DATE)
                    .name("betriebsdatum")
                    .internalName("betriebsdatum")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_5_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("fahrzeug")
                    .internalName("fahrzeug")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_6_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("kurs")
                    .internalName("kurs")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_7_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("seq_von")
                    .internalName("seq_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_8_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_diva_von")
                    .internalName("halt_diva_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_9_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_punkt_diva_von")
                    .internalName("halt_punkt_diva_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_10_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_kurz_von1")
                    .internalName("halt_kurz_von1")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_11_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.DATE)
                    .name("datum_von")
                    .internalName("datum_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_12_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("soll_an_von")
                    .internalName("soll_an_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_13_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("ist_an_von")
                    .internalName("ist_an_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_14_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("soll_ab_von")
                    .internalName("soll_ab_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_15_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("ist_ab_von")
                    .internalName("ist_ab_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_16_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("seq_nach")
                    .internalName("seq_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_17_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_diva_nach")
                    .internalName("halt_diva_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_18_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_punkt_diva_nach")
                    .internalName("halt_punkt_diva_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_19_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_kurz_nach1")
                    .internalName("halt_kurz_nach1")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_20_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.DATE)
                    .name("datum_nach")
                    .internalName("datum_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_21_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("soll_an_nach")
                    .internalName("soll_an_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_22_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("ist_an_nach1")
                    .internalName("ist_an_nach1")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_23_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("soll_ab_nach")
                    .internalName("soll_ab_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_24_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("ist_ab_nach")
                    .internalName("ist_ab_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_25_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("fahrt_id")
                    .internalName("fahrt_id")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_26_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("fahrweg_id")
                    .internalName("fahrweg_id")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_27_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("fw_no")
                    .internalName("fw_no")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_28_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("fw_typ")
                    .internalName("fw_typ")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_29_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("fw_kurz")
                    .internalName("fw_kurz")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_30_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("fw_lang")
                    .internalName("fw_lang")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_31_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("umlauf_von")
                    .internalName("umlauf_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_32_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_id_von")
                    .internalName("halt_id_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_33_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_id_nach")
                    .internalName("halt_id_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_34_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_punkt_id_von")
                    .internalName("halt_punkt_id_von")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_3_35_ID)
                    .tableId(TABLE_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .columnType(ColumnTypeDto.INT)
                    .name("halt_punkt_id_nach")
                    .internalName("halt_punkt_id_nach")
                    .isNullAllowed(true)
                    .enums(new LinkedList<>())
                    .sets(new LinkedList<>())
                    .build());

    public static final UUID COLUMN_5_1_ID = UUID.fromString("4efd4cbb-ca2e-48e2-8f40-37514956aa67");

    public static final UUID COLUMN_5_2_ID = UUID.fromString("53061685-c1db-4df6-ad4e-8f384a200104");

    public static final UUID COLUMN_5_3_ID = UUID.fromString("643f9cda-8db1-47a4-bb08-c10e78e54c10");

    public static final UUID COLUMN_5_4_ID = UUID.fromString("efeacc15-3b31-4a9f-9dba-f07d62dcddd6");

    public static final UUID COLUMN_5_5_ID = UUID.fromString("0319db31-473a-47bc-bb9d-fa1edf82fcd5");

    public static final UUID COLUMN_5_6_ID = UUID.fromString("9ba789ca-59cf-4480-b9f6-3b957b1d7f5c");

    public static final UUID COLUMN_5_7_ID = UUID.fromString("81c42954-fd1a-4fef-adb1-bc4945469e26");

    public static final UUID COLUMN_5_8_ID = UUID.fromString("49a38905-52a2-4a9b-b7b9-5e1dcf799b2a");

    public static final UUID COLUMN_5_9_ID = UUID.fromString("1e1a9b6b-5aee-4773-b52d-ea56a5d1e2c8");

    public static final UUID COLUMN_5_10_ID = UUID.fromString("42ede62a-ae98-4a14-ba54-76b8ba1c580f");

    public static final UUID COLUMN_5_11_ID = UUID.fromString("0af0f84a-5a58-418a-8bbc-bde29ed0cda0");

    public static final UUID COLUMN_5_12_ID = UUID.fromString("d9cb30a2-1566-4bd1-899d-060a8ba47722");

    public static final UUID COLUMN_5_13_ID = UUID.fromString("e69f7f75-3731-4706-8193-0393aa0c08a7");

    public static final UUID COLUMN_5_14_ID = UUID.fromString("4441630e-7dfa-4046-8bc2-929860f1c66e");

    public static final UUID COLUMN_5_15_ID = UUID.fromString("f0a12be0-0b26-4686-bf7e-539cdc7e71b4");

    public static final UUID COLUMN_5_16_ID = UUID.fromString("b60abdcc-5786-40f8-a309-e4467f7d963c");

    public static final UUID COLUMN_5_17_ID = UUID.fromString("6d5877e2-daef-43d6-a1b6-1aff3ab1a9a2");

    public static final UUID COLUMN_5_18_ID = UUID.fromString("bb45455f-d449-496e-94f8-eac4d46ba9c0");

    public static final UUID COLUMN_5_19_ID = UUID.fromString("44c5484b-b57d-48a4-8f24-d2074de98e1a");

    public static final UUID COLUMN_5_20_ID = UUID.fromString("6475b937-71fc-4331-bc85-8ee71fa68d99");

    public static final UUID COLUMN_5_21_ID = UUID.fromString("92ff472f-e203-4c8e-b243-81640229ca19");

    public static final ColumnBriefDto TABLE_5_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(COLUMN_5_1_ID)
            .name("id")
            .internalName("id")
            .columnType(ColumnTypeDto.BIGINT)
            .build();

    public static final List<TableColumn> TABLE_5_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_5_1_ID)
                    .ordinalPosition(0)
                    .table(TABLE_5)
                    .name("id")
                    .internalName("id")
                    .columnType(TableColumnType.BIGINT)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_2_ID)
                    .ordinalPosition(1)
                    .table(TABLE_5)
                    .name("Animal Name")
                    .internalName("animal_name")
                    .columnType(TableColumnType.VARCHAR)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_3_ID)
                    .ordinalPosition(2)
                    .table(TABLE_5)
                    .name("Hair")
                    .internalName("hair")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_4_ID)
                    .ordinalPosition(3)
                    .table(TABLE_5)
                    .name("Feathers")
                    .internalName("feathers")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_5_ID)
                    .ordinalPosition(4)
                    .table(TABLE_5)
                    .name("Bread")
                    .internalName("bread")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_6_ID)
                    .ordinalPosition(5)
                    .table(TABLE_5)
                    .name("Eggs")
                    .internalName("eggs")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_7_ID)
                    .ordinalPosition(6)
                    .table(TABLE_5)
                    .name("Milk")
                    .internalName("milk")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_8_ID)
                    .ordinalPosition(7)
                    .table(TABLE_5)
                    .name("Water")
                    .internalName("water")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_9_ID)
                    .ordinalPosition(8)
                    .table(TABLE_5)
                    .name("Airborne")
                    .internalName("airborne")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_10_ID)
                    .ordinalPosition(9)
                    .table(TABLE_5)
                    .name("Waterborne")
                    .internalName("waterborne")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_11_ID)
                    .ordinalPosition(10)
                    .table(TABLE_5)
                    .name("Aquantic")
                    .internalName("aquantic")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_12_ID)
                    .ordinalPosition(11)
                    .table(TABLE_5)
                    .name("Predator")
                    .internalName("predator")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_13_ID)
                    .ordinalPosition(12)
                    .table(TABLE_5)
                    .name("Backbone")
                    .internalName("backbone")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_14_ID)
                    .ordinalPosition(13)
                    .table(TABLE_5)
                    .name("Breathes")
                    .internalName("breathes")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_15_ID)
                    .ordinalPosition(14)
                    .table(TABLE_5)
                    .name("Venomous")
                    .internalName("venomous")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_16_ID)
                    .ordinalPosition(15)
                    .table(TABLE_5)
                    .name("Fin")
                    .internalName("fin")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_17_ID)
                    .ordinalPosition(16)
                    .table(TABLE_5)
                    .name("Legs")
                    .internalName("legs")
                    .columnType(TableColumnType.INT)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_18_ID)
                    .ordinalPosition(17)
                    .table(TABLE_5)
                    .name("Tail")
                    .internalName("tail")
                    .columnType(TableColumnType.DECIMAL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_19_ID)
                    .ordinalPosition(18)
                    .table(TABLE_5)
                    .name("Domestic")
                    .internalName("domestic")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_20_ID)
                    .ordinalPosition(19)
                    .table(TABLE_5)
                    .name("Catsize")
                    .internalName("catsize")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_5_21_ID)
                    .ordinalPosition(20)
                    .table(TABLE_5)
                    .name("Class Type")
                    .internalName("class_type")
                    .columnType(TableColumnType.DECIMAL)
                    .isNullAllowed(true)
                    .build());

    public static final List<ColumnDto> TABLE_5_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_5_1_ID)
                    .ordinalPosition(0)
                    .tableId(TABLE_5_ID)
                    .name("id")
                    .internalName("id")
                    .columnType(ColumnTypeDto.BIGINT)
                    .isNullAllowed(false)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_2_ID)
                    .ordinalPosition(1)
                    .tableId(TABLE_5_ID)
                    .name("Animal Name")
                    .internalName("animal_name")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_3_ID)
                    .ordinalPosition(2)
                    .tableId(TABLE_5_ID)
                    .name("Hair")
                    .internalName("hair")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_4_ID)
                    .ordinalPosition(3)
                    .tableId(TABLE_5_ID)
                    .name("Feathers")
                    .internalName("feathers")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_5_ID)
                    .ordinalPosition(4)
                    .tableId(TABLE_5_ID)
                    .name("Bread")
                    .internalName("bread")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_6_ID)
                    .ordinalPosition(5)
                    .tableId(TABLE_5_ID)
                    .name("Eggs")
                    .internalName("eggs")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_7_ID)
                    .ordinalPosition(6)
                    .tableId(TABLE_5_ID)
                    .name("Milk")
                    .internalName("milk")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_8_ID)
                    .ordinalPosition(7)
                    .tableId(TABLE_5_ID)
                    .name("Water")
                    .internalName("water")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_9_ID)
                    .ordinalPosition(8)
                    .tableId(TABLE_5_ID)
                    .name("Airborne")
                    .internalName("airborne")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_10_ID)
                    .ordinalPosition(9)
                    .tableId(TABLE_5_ID)
                    .name("Waterborne")
                    .internalName("waterborne")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_11_ID)
                    .ordinalPosition(10)
                    .tableId(TABLE_5_ID)
                    .name("Aquantic")
                    .internalName("aquantic")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_12_ID)
                    .ordinalPosition(11)
                    .tableId(TABLE_5_ID)
                    .name("Predator")
                    .internalName("predator")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_13_ID)
                    .ordinalPosition(12)
                    .tableId(TABLE_5_ID)
                    .name("Backbone")
                    .internalName("backbone")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_14_ID)
                    .ordinalPosition(13)
                    .tableId(TABLE_5_ID)
                    .name("Breathes")
                    .internalName("breathes")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_15_ID)
                    .ordinalPosition(14)
                    .tableId(TABLE_5_ID)
                    .name("Venomous")
                    .internalName("venomous")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_16_ID)
                    .ordinalPosition(15)
                    .tableId(TABLE_5_ID)
                    .name("Fin")
                    .internalName("fin")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_17_ID)
                    .ordinalPosition(16)
                    .tableId(TABLE_5_ID)
                    .name("Legs")
                    .internalName("legs")
                    .columnType(ColumnTypeDto.INT)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_18_ID)
                    .ordinalPosition(17)
                    .tableId(TABLE_5_ID)
                    .name("Tail")
                    .internalName("tail")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_19_ID)
                    .ordinalPosition(18)
                    .tableId(TABLE_5_ID)
                    .name("Domestic")
                    .internalName("domestic")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_20_ID)
                    .ordinalPosition(19)
                    .tableId(TABLE_5_ID)
                    .name("Catsize")
                    .internalName("catsize")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_5_21_ID)
                    .ordinalPosition(20)
                    .tableId(TABLE_5_ID)
                    .name("Class Type")
                    .internalName("class_type")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .isNullAllowed(true)
                    .build());

    public static final List<CreateForeignKeyDto> TABLE_5_FOREIGN_KEYS_INVALID_CREATE = List.of(CreateForeignKeyDto.builder()
            .columns(new LinkedList<>(List.of("somecolumn")))
            .referencedTable("sometable")
            .referencedColumns(new LinkedList<>(List.of("someothercolumn")))
            .build());

    public static final CreateTableConstraintsDto TABLE_5_CONSTRAINTS_INVALID_CREATE = CreateTableConstraintsDto.builder()
            .foreignKeys(TABLE_5_FOREIGN_KEYS_INVALID_CREATE)
            .build();

    public static final List<CreateTableColumnDto> TABLE_5_COLUMNS_CREATE = List.of(CreateTableColumnDto.builder()
                    .name("id")
                    .type(ColumnTypeDto.BIGINT)
                    .nullAllowed(false)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Animal Name")
                    .type(ColumnTypeDto.VARCHAR)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Hair")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Feathers")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Bread")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Eggs")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Milk")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Water")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Airborne")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Waterborne")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Aquantic")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Predator")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Backbone")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Breathes")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Venomous")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Fin")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Legs")
                    .type(ColumnTypeDto.INT)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Tail")
                    .type(ColumnTypeDto.DECIMAL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Domestic")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Catsize")
                    .type(ColumnTypeDto.BOOL)
                    .nullAllowed(true)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("Class Type")
                    .type(ColumnTypeDto.DECIMAL)
                    .nullAllowed(true)
                    .build());

    public static final CreateTableConstraintsDto TABLE_5_CREATE_CONSTRAINTS_DTO = CreateTableConstraintsDto.builder()
            .primaryKey(Set.of("id"))
            .uniques(new LinkedList<>(List.of(List.of("id"))))
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .build();

    public static final CreateTableDto TABLE_5_CREATE_DTO = CreateTableDto.builder()
            .name(TABLE_5_NAME)
            .description(TABLE_5_DESCRIPTION)
            .columns(TABLE_5_COLUMNS_CREATE)
            .constraints(TABLE_5_CREATE_CONSTRAINTS_DTO)
            .build();

    public static final CreateTableDto TABLE_5_INVALID_CREATE_DTO = CreateTableDto.builder()
            .name(TABLE_5_NAME)
            .description(TABLE_5_DESCRIPTION)
            .columns(TABLE_5_COLUMNS_CREATE)
            .constraints(TABLE_5_CONSTRAINTS_INVALID_CREATE)
            .build();

    public static final UUID QUERY_8_ID = UUID.fromString("1c466eee-d551-4ef9-a7e0-b5a2d1b15473");
    public static final String QUERY_8_STATEMENT = "SELECT `id`, `animal_name` FROM `zoo` WHERE `hair` = TRUE AND `feathers` = FALSE;";
    public static final String QUERY_8_QUERY_HASH = "f0ee0d6dd45e092fca120c4f0eab089f91ed26ccf8dc34a03c6b9c6bb4141271";
    public static final Long QUERY_8_RESULT_NUMBER = 5L;
    public static final String QUERY_8_RESULT_HASH = "b5f9cae916d32deff81c5f2e9f8ff43904034bc084b12320730953d120698bed";
    public static final Instant QUERY_8_EXECUTION = Instant.now().minus(1, MINUTES);
    public static final Boolean QUERY_8_PERSISTED = true;

    public static final SubsetDto QUERY_8_SUBSET_DTO = SubsetDto.builder()
            .tableId(TABLE_5_ID)
            .columns(new LinkedList<>(List.of(COLUMN_5_1_ID, COLUMN_5_2_ID)))
            .filter(new LinkedList<>(List.of(FilterDto.builder()
                            .type(FilterTypeDto.WHERE)
                            .columnId(COLUMN_5_3_ID)
                            .operatorId(IMAGE_1_OPERATORS_2_ID)
                            .value("true")
                            .build(),
                    FilterDto.builder()
                            .type(FilterTypeDto.AND)
                            .build(),
                    FilterDto.builder()
                            .type(FilterTypeDto.WHERE)
                            .columnId(COLUMN_5_4_ID)
                            .operatorId(IMAGE_1_OPERATORS_2_ID)
                            .value("false")
                            .build())))
            .build();

    public static final UUID COLUMN_6_1_ID = UUID.fromString("27b04a64-2849-4fae-b295-858c3e50361f");

    public static final UUID COLUMN_6_2_ID = UUID.fromString("1ea62e32-5719-4152-94da-45d37eb88b6f");

    public static final UUID COLUMN_6_3_ID = UUID.fromString("f523f9f5-42f7-4695-841e-a5fd30fa6879");

    public static final UUID COLUMN_6_4_ID = UUID.fromString("f57ea880-f917-4127-bcbb-202a34831383");

    public static final UUID COLUMN_6_5_ID = UUID.fromString("38aaeb63-b94b-4d90-8eae-a626dfb1f092");

    public static final UUID COLUMN_6_6_ID = UUID.fromString("f788cf6f-66ed-4f28-8b24-d9d173c4d340");

    public static final List<TableColumn> TABLE_6_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_6_1_ID)
                    .ordinalPosition(0)
                    .table(TABLE_6)
                    .name("id")
                    .internalName("id")
                    .columnType(TableColumnType.BIGINT)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_6_2_ID)
                    .ordinalPosition(1)
                    .table(TABLE_6)
                    .name("firstname")
                    .internalName("firstname")
                    .columnType(TableColumnType.VARCHAR)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_6_3_ID)
                    .ordinalPosition(2)
                    .table(TABLE_6)
                    .name("lastname")
                    .internalName("lastname")
                    .columnType(TableColumnType.VARCHAR)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_6_4_ID)
                    .ordinalPosition(3)
                    .table(TABLE_6)
                    .name("birth")
                    .internalName("birth")
                    .columnType(TableColumnType.YEAR)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_6_5_ID)
                    .ordinalPosition(4)
                    .table(TABLE_6)
                    .name("reminder")
                    .internalName("reminder")
                    .columnType(TableColumnType.TIME)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_6_6_ID)
                    .ordinalPosition(5)
                    .table(TABLE_6)
                    .name("ref_id")
                    .internalName("ref_id")
                    .columnType(TableColumnType.BIGINT)
                    .isNullAllowed(true)
                    .build());

    public static final ColumnBriefDto TABLE_6_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(COLUMN_6_1_ID)
            .name("id")
            .internalName("id")
            .columnType(ColumnTypeDto.BIGINT)
            .build();

    public static final List<ColumnDto> TABLE_6_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_6_1_ID)
                    .ordinalPosition(0)
                    .tableId(TABLE_6_ID)
                    .name("id")
                    .internalName("id")
                    .columnType(ColumnTypeDto.BIGINT)
                    .isNullAllowed(false)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_6_2_ID)
                    .ordinalPosition(1)
                    .tableId(TABLE_6_ID)
                    .name("firstname")
                    .internalName("firstname")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .isNullAllowed(false)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_6_3_ID)
                    .ordinalPosition(2)
                    .tableId(TABLE_6_ID)
                    .name("lastname")
                    .internalName("lastname")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .isNullAllowed(false)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_6_4_ID)
                    .ordinalPosition(3)
                    .tableId(TABLE_6_ID)
                    .name("birth")
                    .internalName("birth")
                    .columnType(ColumnTypeDto.YEAR)
                    .isNullAllowed(false)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_6_5_ID)
                    .ordinalPosition(4)
                    .tableId(TABLE_6_ID)
                    .name("reminder")
                    .internalName("reminder")
                    .columnType(ColumnTypeDto.TIME)
                    .isNullAllowed(false)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_6_6_ID)
                    .ordinalPosition(5)
                    .tableId(TABLE_6_ID)
                    .name("ref_id")
                    .internalName("ref_id")
                    .columnType(ColumnTypeDto.BIGINT)
                    .isNullAllowed(true)
                    .build());

    public static final List<List<String>> TABLE_6_UNIQUES_CREATE = List.of(
            List.of("firstname", "lastname"));

    public static final List<CreateForeignKeyDto> TABLE_6_FOREIGN_KEYS_CREATE = List.of(CreateForeignKeyDto.builder()
            .columns(new LinkedList<>(List.of("ref_id")))
            .referencedTable("zoo")
            .referencedColumns(new LinkedList<>(List.of("id")))
            .build());

    public static final Set<String> TABLE_6_CHECKS_CREATE = Set.of("firstname != lastname");

    public static final CreateTableConstraintsDto TABLE_6_CONSTRAINTS_CREATE = CreateTableConstraintsDto.builder()
            .uniques(TABLE_6_UNIQUES_CREATE)
            .foreignKeys(TABLE_6_FOREIGN_KEYS_CREATE)
            .checks(TABLE_6_CHECKS_CREATE)
            .primaryKey(Set.of("id"))
            .build();

    public static final List<CreateTableColumnDto> TABLE_6_COLUMNS_CREATE = List.of(
            CreateTableColumnDto.builder()
                    .name("name_id")
                    .type(ColumnTypeDto.BIGINT)
                    .nullAllowed(false)
                    .build(),
            CreateTableColumnDto.builder()
                    .name("zoo_id")
                    .type(ColumnTypeDto.BIGINT)
                    .size(255L)
                    .nullAllowed(false)
                    .build());

    public static final CreateTableDto TABLE_6_CREATE_DTO = CreateTableDto.builder()
            .name(TABLE_6_NAME)
            .description(TABLE_6_DESCRIPTION)
            .columns(TABLE_6_COLUMNS_CREATE)
            .constraints(TABLE_6_CONSTRAINTS_CREATE)
            .build();

    public static final UUID COLUMN_7_1_ID = UUID.fromString("395b44a4-0e31-41ea-94ad-c4f2d5e912c6");

    public static final UUID COLUMN_7_2_ID = UUID.fromString("5713333b-872a-44c5-ab94-4d0ab62f5663");

    public static final ColumnBriefDto TABLE_7_COLUMNS_BRIEF_0_DTO = ColumnBriefDto.builder()
            .id(COLUMN_7_1_ID)
            .name("name_id")
            .internalName("name_id")
            .columnType(ColumnTypeDto.BIGINT)
            .build();

    public static final ColumnBriefDto TABLE_7_COLUMNS_BRIEF_1_DTO = ColumnBriefDto.builder()
            .id(COLUMN_7_2_ID)
            .name("zoo_id")
            .internalName("zoo_id")
            .columnType(ColumnTypeDto.BIGINT)
            .build();

    public static final List<TableColumn> TABLE_7_COLUMNS = List.of(TableColumn.builder()
                    .id(COLUMN_7_1_ID)
                    .ordinalPosition(0)
                    .table(TABLE_7)
                    .name("name_id")
                    .internalName("name_id")
                    .columnType(TableColumnType.BIGINT)
                    .isNullAllowed(false)
                    .build(),
            TableColumn.builder()
                    .id(COLUMN_7_2_ID)
                    .ordinalPosition(1)
                    .table(TABLE_7)
                    .name("zoo_id")
                    .internalName("zoo_id")
                    .columnType(TableColumnType.BIGINT)
                    .isNullAllowed(false)
                    .build());

    public static final List<ColumnDto> TABLE_7_COLUMNS_DTO = List.of(ColumnDto.builder()
                    .id(COLUMN_7_1_ID)
                    .ordinalPosition(0)
                    .tableId(TABLE_7_ID)
                    .name("name_id")
                    .internalName("name_id")
                    .columnType(ColumnTypeDto.BIGINT)
                    .isNullAllowed(false)
                    .build(),
            ColumnDto.builder()
                    .id(COLUMN_7_2_ID)
                    .ordinalPosition(1)
                    .tableId(TABLE_7_ID)
                    .name("zoo_id")
                    .internalName("zoo_id")
                    .columnType(ColumnTypeDto.BIGINT)
                    .isNullAllowed(false)
                    .build());

    public static final UUID VIEW_1_ID = UUID.fromString("7d712cf7-78c7-4a47-90b0-d6b9f7f19b70");
    public static final Boolean VIEW_1_INITIAL_VIEW = false;
    public static final String VIEW_1_NAME = "JUnit";
    public static final String VIEW_1_INTERNAL_NAME = "junit";
    public static final Boolean VIEW_1_PUBLIC = false;
    public static final Boolean VIEW_1_SCHEMA_PUBLIC = false;
    public static final String VIEW_1_QUERY = "SELECT `location`, `lat`, `lng` FROM `weather_location`";
    public static final String VIEW_1_QUERY_HASH = "dc81a6877c7c51a6a6f406e1fc2a255e44a0d49a20548596e0d583c3eb849c23";

    public static final UUID VIEW_COLUMN_1_1_ID = UUID.fromString("ebf2c5ce-4deb-4cc6-b6f6-61f5d3f6fc98");

    public static final UUID VIEW_COLUMN_1_2_ID = UUID.fromString("d6ba3475-cefa-4771-aaa1-9274f16335ee");

    public static final UUID VIEW_COLUMN_1_3_ID = UUID.fromString("4f189a5f-c9ca-4518-9758-1a0730f6276b");

    public static final SubsetDto VIEW_1_SUBSET_DTO = SubsetDto.builder()
            .tableId(TABLE_2_ID)
            .columns(new LinkedList<>(List.of(COLUMN_2_1_ID, COLUMN_2_2_ID, COLUMN_2_3_ID)))
            .build();

    public static final List<ViewColumnDto> VIEW_1_COLUMNS_DTO = List.of(
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_1_1_ID)
                    .ordinalPosition(0)
                    .databaseId(DATABASE_1_ID)
                    .name("location")
                    .internalName("location")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .size(255L)
                    .isNullAllowed(false)
                    .build(),
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_1_2_ID)
                    .ordinalPosition(1)
                    .databaseId(DATABASE_1_ID)
                    .name("lat")
                    .internalName("lat")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_1_3_ID)
                    .ordinalPosition(2)
                    .databaseId(DATABASE_1_ID)
                    .name("lng")
                    .internalName("lng")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build()
    );

    public static final View VIEW_1 = View.builder()
            .id(VIEW_1_ID)
            .isInitialView(VIEW_1_INITIAL_VIEW)
            .name(VIEW_1_NAME)
            .internalName(VIEW_1_INTERNAL_NAME)
            .isPublic(VIEW_1_PUBLIC)
            .isSchemaPublic(VIEW_1_SCHEMA_PUBLIC)
            .query(VIEW_1_QUERY)
            .queryHash(VIEW_1_QUERY_HASH)
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .identifiers(new LinkedList<>()) /* IDENTIFIER_3 */
            .columns(null) /* VIEW_1_COLUMNS */
            .database(null) /* DATABASE_1 */
            .build();

    public static final Long VIEW_1_DATA_COUNT = 3L;
    public static final List<Map<String, Object>> VIEW_1_DATA_DTO = new LinkedList<>(List.of(
            new HashMap<>() {{
                put("location", "Albury");
                put("lat", -36.0653583);
                put("lng", 146.9112214);
            }},
            new HashMap<>() {{
                put("location", "Sydney");
                put("lat", -33.847927);
                put("lng", 150.6517942);
            }},
            new HashMap<>() {{
                put("location", "Vienna");
                put("lat", null);
                put("lng", null);
            }}
    ));

    public static final List<ViewColumn> VIEW_1_COLUMNS = List.of(
            ViewColumn.builder()
                    .id(VIEW_COLUMN_1_1_ID)
                    .ordinalPosition(0)
                    .name("location")
                    .internalName("location")
                    .columnType(TableColumnType.VARCHAR)
                    .size(255L)
                    .isNullAllowed(false)
                    .view(VIEW_1)
                    .build(),
            ViewColumn.builder()
                    .id(VIEW_COLUMN_1_2_ID)
                    .ordinalPosition(1)
                    .name("lat")
                    .internalName("lat")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .view(VIEW_1)
                    .build(),
            ViewColumn.builder()
                    .id(VIEW_COLUMN_1_3_ID)
                    .ordinalPosition(2)
                    .name("lng")
                    .internalName("lng")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .view(VIEW_1)
                    .build()
    );

    public static final ViewDto VIEW_1_DTO = ViewDto.builder()
            .id(VIEW_1_ID)
            .databaseId(DATABASE_1_ID)
            .isInitialView(VIEW_1_INITIAL_VIEW)
            .name(VIEW_1_NAME)
            .internalName(VIEW_1_INTERNAL_NAME)
            .isPublic(VIEW_1_PUBLIC)
            .isSchemaPublic(VIEW_1_SCHEMA_PUBLIC)
            .identifiers(null /* VIEW_1_DTO_IDENTIFIERS */)
            .owner(USER_1_BRIEF_DTO)
            .query(VIEW_1_QUERY)
            .queryHash(VIEW_1_QUERY_HASH)
            .columns(VIEW_1_COLUMNS_DTO)
            .build();

    public static final ViewBriefDto VIEW_1_BRIEF_DTO = ViewBriefDto.builder()
            .id(VIEW_1_ID)
            .isInitialView(VIEW_1_INITIAL_VIEW)
            .name(VIEW_1_NAME)
            .internalName(VIEW_1_INTERNAL_NAME)
            .vdbid(DATABASE_1_ID)
            .isPublic(VIEW_1_PUBLIC)
            .isSchemaPublic(VIEW_1_SCHEMA_PUBLIC)
            .ownedBy(USER_1_ID)
            .query(VIEW_1_QUERY)
            .queryHash(VIEW_1_QUERY_HASH)
            .build();

    public static final CreateViewDto VIEW_1_CREATE_DTO = CreateViewDto.builder()
            .isPublic(VIEW_1_PUBLIC)
            .name(VIEW_1_NAME)
            .query(VIEW_1_SUBSET_DTO)
            .build();

    public static final UUID VIEW_2_ID = UUID.fromString("1921a0a0-e4b0-4d12-a05f-be920af9b5ce");
    public static final Boolean VIEW_2_INITIAL_VIEW = false;
    public static final String VIEW_2_NAME = "JUnit2";
    public static final String VIEW_2_INTERNAL_NAME = "junit2";
    public static final Boolean VIEW_2_PUBLIC = true;
    public static final Boolean VIEW_2_SCHEMA_PUBLIC = true;
    public static final String VIEW_2_QUERY = "select `date`, `location` as loc, `mintemp`, `rainfall` from `weather_aus` where `location` = 'Albury'";
    public static final String VIEW_2_QUERY_HASH = "987fc946772ffb6d85060262dcb5df419692a1f6772ea995e3dedb53c191e984";

    public static final UUID VIEW_COLUMN_2_1_ID = UUID.fromString("8fb30bce-04a8-4e9a-9c6b-0776eda3aab8");

    public static final UUID VIEW_COLUMN_2_2_ID = UUID.fromString("d43f9940-ae27-4d81-b17b-ccbaf578186c");

    public static final UUID VIEW_COLUMN_2_3_ID = UUID.fromString("b47733bb-aeea-414d-811e-405c64463730");

    public static final UUID VIEW_COLUMN_2_4_ID = UUID.fromString("2b467e3a-acef-4944-be19-b4b0680874c2");

    public static final List<ViewColumnDto> VIEW_2_COLUMNS_DTO = List.of(
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_2_1_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(0)
                    .name("Date")
                    .internalName("date")
                    .columnType(ColumnTypeDto.DATE)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_2_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(1)
                    .name("loc")
                    .internalName("loc")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .size(255L)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_2_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(2)
                    .name("Rainfall")
                    .internalName("rainfall")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_2_4_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(3)
                    .name("MinTemp")
                    .internalName("mintemp")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build()
    );

    public static final View VIEW_2 = View.builder()
            .id(VIEW_2_ID)
            .isInitialView(VIEW_2_INITIAL_VIEW)
            .name(VIEW_2_NAME)
            .internalName(VIEW_2_INTERNAL_NAME)
            .isPublic(VIEW_2_PUBLIC)
            .isSchemaPublic(VIEW_2_SCHEMA_PUBLIC)
            .columns(null)  /* VIEW_2_COLUMNS */
            .query(VIEW_2_QUERY)
            .queryHash(VIEW_2_QUERY_HASH)
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .database(null) /* DATABASE_1 */
            .build();

    public static final List<ViewColumn> VIEW_2_COLUMNS = List.of(
            ViewColumn.builder()
                    .id(VIEW_COLUMN_2_1_ID)
                    .ordinalPosition(0)
                    .name("Date")
                    .internalName("date")
                    .columnType(TableColumnType.DATE)
                    .isNullAllowed(true)
                    .view(VIEW_2)
                    .build(),
            ViewColumn.builder()
                    .id(VIEW_COLUMN_2_2_ID)
                    .ordinalPosition(1)
                    .name("loc")
                    .internalName("loc")
                    .columnType(TableColumnType.VARCHAR)
                    .size(255L)
                    .isNullAllowed(true)
                    .view(VIEW_2)
                    .build(),
            ViewColumn.builder()
                    .id(VIEW_COLUMN_2_3_ID)
                    .ordinalPosition(2)
                    .name("Rainfall")
                    .internalName("rainfall")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .view(VIEW_2)
                    .build(),
            ViewColumn.builder()
                    .id(VIEW_COLUMN_2_4_ID)
                    .ordinalPosition(3)
                    .name("MinTemp")
                    .internalName("mintemp")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .view(VIEW_2)
                    .build()
    );

    public static final ViewDto VIEW_2_DTO = ViewDto.builder()
            .id(VIEW_2_ID)
            .databaseId(DATABASE_1_ID)
            .isInitialView(VIEW_2_INITIAL_VIEW)
            .name(VIEW_2_NAME)
            .internalName(VIEW_2_INTERNAL_NAME)
            .isPublic(VIEW_2_PUBLIC)
            .isSchemaPublic(VIEW_2_SCHEMA_PUBLIC)
            .columns(VIEW_2_COLUMNS_DTO)
            .query(VIEW_2_QUERY)
            .queryHash(VIEW_2_QUERY_HASH)
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final ViewBriefDto VIEW_2_BRIEF_DTO = ViewBriefDto.builder()
            .id(VIEW_2_ID)
            .isInitialView(VIEW_2_INITIAL_VIEW)
            .name(VIEW_2_NAME)
            .internalName(VIEW_2_INTERNAL_NAME)
            .vdbid(DATABASE_1_ID)
            .isPublic(VIEW_2_PUBLIC)
            .isSchemaPublic(VIEW_2_SCHEMA_PUBLIC)
            .query(VIEW_2_QUERY)
            .queryHash(VIEW_2_QUERY_HASH)
            .ownedBy(USER_1_ID)
            .build();

    public static final UUID VIEW_3_ID = UUID.fromString("88940939-d456-4aae-88a6-f2b6b343c614");
    public static final Boolean VIEW_3_INITIAL_VIEW = false;
    public static final String VIEW_3_NAME = "JUnit3";
    public static final String VIEW_3_INTERNAL_NAME = "junit3";
    public static final Boolean VIEW_3_PUBLIC = true;
    public static final Boolean VIEW_3_SCHEMA_PUBLIC = false;
    public static final String VIEW_3_QUERY = "select w.`mintemp`, w.`rainfall`, w.`location`, m.`date` from `weather_aus` w join `junit2` m on m.`location` = w.`location` and m.`date` = w.`date`";
    public static final String VIEW_3_QUERY_HASH = "bbbaa56a5206b3dc3e6cf9301b0db9344eb6f19b100c7b88550ffb597a0bd255";

    public static final Long VIEW_3_DATA_COUNT = 3L;

    public static final UUID VIEW_COLUMN_3_1_ID = UUID.fromString("129839cb-dbd7-492d-8fd0-ee44a8f51c4d");

    public static final UUID VIEW_COLUMN_3_2_ID = UUID.fromString("e229d80a-c25c-4fbe-8f31-bbb2e1dff3d5");

    public static final UUID VIEW_COLUMN_3_3_ID = UUID.fromString("12083a5d-fdd3-41db-9f92-d1298558e477");

    public static final UUID VIEW_COLUMN_3_4_ID = UUID.fromString("668f8a87-1fa6-4be7-9761-1844aa8315a4");

    public static final List<ViewColumnDto> VIEW_3_COLUMNS_DTO = List.of(
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_3_1_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(0)
                    .name("MinTemp")
                    .internalName("mintemp")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_3_2_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(1)
                    .name("Rainfall")
                    .internalName("rainfall")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_3_3_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(2)
                    .name("Location")
                    .internalName("location")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .size(255L)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(VIEW_COLUMN_3_4_ID)
                    .databaseId(DATABASE_1_ID)
                    .ordinalPosition(3)
                    .name("Date")
                    .internalName("date")
                    .columnType(ColumnTypeDto.DATE)
                    .isNullAllowed(true)
                    .build()
    );

    public static final View VIEW_3 = View.builder()
            .id(VIEW_3_ID)
            .isInitialView(VIEW_3_INITIAL_VIEW)
            .name(VIEW_3_NAME)
            .internalName(VIEW_3_INTERNAL_NAME)
            .isPublic(VIEW_3_PUBLIC)
            .isSchemaPublic(VIEW_3_SCHEMA_PUBLIC)
            .columns(null)  /* VIEW_3_COLUMNS */
            .query(VIEW_3_QUERY)
            .queryHash(VIEW_3_QUERY_HASH)
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .database(null) /* DATABASE_1 */
            .build();

    public static final List<ViewColumn> VIEW_3_COLUMNS = List.of(
            ViewColumn.builder()
                    .id(VIEW_COLUMN_3_1_ID)
                    .ordinalPosition(0)
                    .name("MinTemp")
                    .internalName("mintemp")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .view(VIEW_3)
                    .build(),
            ViewColumn.builder()
                    .id(VIEW_COLUMN_3_2_ID)
                    .ordinalPosition(1)
                    .name("Rainfall")
                    .internalName("rainfall")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .view(VIEW_3)
                    .build(),
            ViewColumn.builder()
                    .id(VIEW_COLUMN_3_3_ID)
                    .ordinalPosition(2)
                    .name("Location")
                    .internalName("location")
                    .columnType(TableColumnType.VARCHAR)
                    .size(255L)
                    .isNullAllowed(true)
                    .view(VIEW_3)
                    .build(),
            ViewColumn.builder()
                    .id(VIEW_COLUMN_3_4_ID)
                    .ordinalPosition(3)
                    .name("Date")
                    .internalName("date")
                    .columnType(TableColumnType.DATE)
                    .isNullAllowed(true)
                    .view(VIEW_3)
                    .build()
    );

    public static final ViewDto VIEW_3_DTO = ViewDto.builder()
            .id(VIEW_3_ID)
            .databaseId(DATABASE_1_ID)
            .isInitialView(VIEW_3_INITIAL_VIEW)
            .name(VIEW_3_NAME)
            .internalName(VIEW_3_INTERNAL_NAME)
            .isPublic(VIEW_3_PUBLIC)
            .isSchemaPublic(VIEW_3_SCHEMA_PUBLIC)
            .columns(VIEW_3_COLUMNS_DTO)
            .query(VIEW_3_QUERY)
            .queryHash(VIEW_3_QUERY_HASH)
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final ViewBriefDto VIEW_3_BRIEF_DTO = ViewBriefDto.builder()
            .id(VIEW_3_ID)
            .isInitialView(VIEW_3_INITIAL_VIEW)
            .name(VIEW_3_NAME)
            .internalName(VIEW_3_INTERNAL_NAME)
            .vdbid(DATABASE_1_ID)
            .isPublic(VIEW_3_PUBLIC)
            .isSchemaPublic(VIEW_3_SCHEMA_PUBLIC)
            .query(VIEW_3_QUERY)
            .queryHash(VIEW_3_QUERY_HASH)
            .ownedBy(USER_1_ID)
            .build();

    public static final UUID VIEW_4_ID = UUID.fromString("13b36fa0-a65a-4ccf-80b1-5b3a2444a41a");
    public static final Boolean VIEW_4_INITIAL_VIEW = false;
    public static final String VIEW_4_NAME = "Mock View";
    public static final String VIEW_4_INTERNAL_NAME = "mock_view";
    public static final Table VIEW_4_TABLE = TABLE_5;
    public static final Boolean VIEW_4_PUBLIC = true;
    public static final Boolean VIEW_4_SCHEMA_PUBLIC = false;
    public static final String VIEW_4_QUERY = "SELECT `animal_name`, `hair`, `feathers`, `eggs`, `milk`, `airborne`, `aquatic`, `predator`, `backbone`, `breathes`, `venomous`, `fins`, `legs`, `tail`, `domestic`, `catsize`, `class_type` FROM `zoo` WHERE `class_type` = 1";
    public static final String VIEW_4_QUERY_HASH = "3561cd0bb0b0e94d6f15ae602134252a5760d09d660a71a4fb015b6991c8ba0b";

    public static final List<ViewColumnDto> VIEW_4_COLUMNS_DTO = List.of(
            ViewColumnDto.builder()
                    .id(COLUMN_5_1_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(0)
                    .name("Animal Name")
                    .internalName("animal_name")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_2_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(1)
                    .name("Hair")
                    .internalName("hair")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_3_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(2)
                    .name("Feathers")
                    .internalName("feathers")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_4_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(3)
                    .name("Eggs")
                    .internalName("eggs")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_5_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(4)
                    .name("Milk")
                    .internalName("milk")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_6_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(5)
                    .name("Airborne")
                    .internalName("airborne")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_7_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(6)
                    .name("Aquantic")
                    .internalName("aquantic")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_8_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(7)
                    .name("Predator")
                    .internalName("predator")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_9_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(8)
                    .name("Backbone")
                    .internalName("backbone")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_10_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(9)
                    .name("Breathes")
                    .internalName("breathes")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_11_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(10)
                    .name("Venomous")
                    .internalName("venomous")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_12_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(11)
                    .name("Fin")
                    .internalName("fin")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_13_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(12)
                    .name("Legs")
                    .internalName("legs")
                    .columnType(ColumnTypeDto.INT)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_14_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(13)
                    .name("Tail")
                    .internalName("tail")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_15_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(14)
                    .name("Domestic")
                    .internalName("domestic")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_16_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(15)
                    .name("Catsize")
                    .internalName("catsize")
                    .columnType(ColumnTypeDto.BOOL)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_5_17_ID)
                    .databaseId(DATABASE_2_ID)
                    .ordinalPosition(16)
                    .name("Class Type")
                    .internalName("class_type")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .isNullAllowed(true)
                    .build());

    public static final View VIEW_4 = View.builder()
            .id(VIEW_4_ID)
            .isInitialView(VIEW_4_INITIAL_VIEW)
            .name(VIEW_4_NAME)
            .internalName(VIEW_4_INTERNAL_NAME)
            .isPublic(VIEW_4_PUBLIC)
            .isSchemaPublic(VIEW_4_SCHEMA_PUBLIC)
            .query(VIEW_4_QUERY)
            .queryHash(VIEW_4_QUERY_HASH)
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .columns(null) /* VIEW_4_COLUMNS */
            .build();

    public static final ViewDto VIEW_4_DTO = ViewDto.builder()
            .id(VIEW_4_ID)
            .databaseId(DATABASE_2_ID)
            .isInitialView(VIEW_4_INITIAL_VIEW)
            .name(VIEW_4_NAME)
            .internalName(VIEW_4_INTERNAL_NAME)
            .isPublic(VIEW_4_PUBLIC)
            .isSchemaPublic(VIEW_4_SCHEMA_PUBLIC)
            .query(VIEW_4_QUERY)
            .queryHash(VIEW_4_QUERY_HASH)
            .owner(USER_1_BRIEF_DTO)
            .columns(VIEW_4_COLUMNS_DTO)
            .build();

    public static final ViewBriefDto VIEW_4_BRIEF_DTO = ViewBriefDto.builder()
            .id(VIEW_4_ID)
            .isInitialView(VIEW_4_INITIAL_VIEW)
            .name(VIEW_4_NAME)
            .internalName(VIEW_4_INTERNAL_NAME)
            .vdbid(DATABASE_2_ID)
            .isPublic(VIEW_4_PUBLIC)
            .isSchemaPublic(VIEW_4_SCHEMA_PUBLIC)
            .query(VIEW_4_QUERY)
            .queryHash(VIEW_4_QUERY_HASH)
            .ownedBy(USER_1_ID)
            .build();

    public static final List<ViewColumn> VIEW_4_COLUMNS = List.of(
            ViewColumn.builder()
                    .id(COLUMN_5_1_ID)
                    .ordinalPosition(0)
                    .name("Animal Name")
                    .internalName("animal_name")
                    .columnType(TableColumnType.VARCHAR)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_2_ID)
                    .ordinalPosition(1)
                    .name("Hair")
                    .internalName("hair")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_3_ID)
                    .ordinalPosition(2)
                    .name("Feathers")
                    .internalName("feathers")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_4_ID)
                    .ordinalPosition(3)
                    .name("Eggs")
                    .internalName("eggs")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_5_ID)
                    .ordinalPosition(4)
                    .name("Milk")
                    .internalName("milk")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_6_ID)
                    .ordinalPosition(5)
                    .name("Airborne")
                    .internalName("airborne")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_7_ID)
                    .ordinalPosition(6)
                    .name("Aquantic")
                    .internalName("aquantic")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_8_ID)
                    .ordinalPosition(7)
                    .name("Predator")
                    .internalName("predator")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_9_ID)
                    .ordinalPosition(8)
                    .name("Backbone")
                    .internalName("backbone")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_10_ID)
                    .ordinalPosition(9)
                    .name("Breathes")
                    .internalName("breathes")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_11_ID)
                    .ordinalPosition(10)
                    .name("Venomous")
                    .internalName("venomous")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_12_ID)
                    .ordinalPosition(11)
                    .name("Fin")
                    .internalName("fin")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_13_ID)
                    .ordinalPosition(12)
                    .name("Legs")
                    .internalName("legs")
                    .columnType(TableColumnType.INT)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_14_ID)
                    .ordinalPosition(13)
                    .name("Tail")
                    .internalName("tail")
                    .columnType(TableColumnType.DECIMAL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_15_ID)
                    .ordinalPosition(14)
                    .name("Domestic")
                    .internalName("domestic")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_16_ID)
                    .ordinalPosition(15)
                    .name("Catsize")
                    .internalName("catsize")
                    .columnType(TableColumnType.BOOL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_5_17_ID)
                    .ordinalPosition(16)
                    .name("Class Type")
                    .internalName("class_type")
                    .columnType(TableColumnType.DECIMAL)
                    .isNullAllowed(true)
                    .view(VIEW_4)
                    .build());

    public static final UUID VIEW_5_ID = UUID.fromString("bc6b8507-51f1-4d05-bb0c-1f619a991dec");
    public static final Boolean VIEW_5_INITIAL_VIEW = false;
    public static final String VIEW_5_NAME = "Mock View";
    public static final String VIEW_5_INTERNAL_NAME = "mock_view";
    public static final Boolean VIEW_5_PUBLIC = true;
    public static final Boolean VIEW_5_SCHEMA_PUBLIC = true;
    public static final String VIEW_5_QUERY = "SELECT `location`, `lat`, `lng` FROM `weather_location` WHERE `location` = 'Albury'";
    public static final String VIEW_5_QUERY_HASH = "120f32478aaff874c25ab32eceb9f00b64cc9d422831046f2f5d43953aca01e7";

    public static final View VIEW_5 = View.builder()
            .id(VIEW_5_ID)
            .isInitialView(VIEW_5_INITIAL_VIEW)
            .name(VIEW_5_NAME)
            .internalName(VIEW_5_INTERNAL_NAME)
            .isPublic(VIEW_5_PUBLIC)
            .isSchemaPublic(VIEW_5_SCHEMA_PUBLIC)
            .query(VIEW_5_QUERY)
            .queryHash(VIEW_5_QUERY_HASH)
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .columns(null)
            .build();

    public static final ViewDto VIEW_5_DTO = ViewDto.builder()
            .id(VIEW_5_ID)
            .databaseId(DATABASE_3_ID)
            .isInitialView(VIEW_5_INITIAL_VIEW)
            .name(VIEW_5_NAME)
            .internalName(VIEW_5_INTERNAL_NAME)
            .isPublic(VIEW_5_PUBLIC)
            .isSchemaPublic(VIEW_5_SCHEMA_PUBLIC)
            .query(VIEW_5_QUERY)
            .queryHash(VIEW_5_QUERY_HASH)
            .owner(USER_1_BRIEF_DTO)
            .columns(new LinkedList<>())
            .build();

    public static final ViewBriefDto VIEW_5_BRIEF_DTO = ViewBriefDto.builder()
            .id(VIEW_5_ID)
            .isInitialView(VIEW_5_INITIAL_VIEW)
            .name(VIEW_5_NAME)
            .internalName(VIEW_5_INTERNAL_NAME)
            .vdbid(DATABASE_3_ID)
            .isPublic(VIEW_5_PUBLIC)
            .isSchemaPublic(VIEW_5_SCHEMA_PUBLIC)
            .query(VIEW_5_QUERY)
            .queryHash(VIEW_5_QUERY_HASH)
            .build();

    public static final List<ViewColumn> VIEW_5_COLUMNS = List.of(
            ViewColumn.builder()
                    .id(COLUMN_2_1_ID)
                    .ordinalPosition(0)
                    .name("location")
                    .internalName("location")
                    .columnType(TableColumnType.VARCHAR)
                    .size(255L)
                    .isNullAllowed(false)
                    .view(VIEW_5)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_2_2_ID)
                    .ordinalPosition(1)
                    .name("lat")
                    .internalName("lat")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .view(VIEW_5)
                    .build(),
            ViewColumn.builder()
                    .id(COLUMN_2_3_ID)
                    .ordinalPosition(2)
                    .name("lng")
                    .internalName("lng")
                    .columnType(TableColumnType.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .view(VIEW_5)
                    .build());

    public static final List<ViewColumnDto> VIEW_5_COLUMNS_DTO = List.of(
            ViewColumnDto.builder()
                    .id(COLUMN_2_1_ID)
                    .databaseId(DATABASE_3_ID)
                    .ordinalPosition(0)
                    .name("location")
                    .internalName("location")
                    .columnType(ColumnTypeDto.VARCHAR)
                    .size(255L)
                    .isNullAllowed(false)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_2_2_ID)
                    .databaseId(DATABASE_3_ID)
                    .ordinalPosition(1)
                    .name("lat")
                    .internalName("lat")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build(),
            ViewColumnDto.builder()
                    .id(COLUMN_2_3_ID)
                    .databaseId(DATABASE_3_ID)
                    .ordinalPosition(2)
                    .name("lng")
                    .internalName("lng")
                    .columnType(ColumnTypeDto.DECIMAL)
                    .size(10L)
                    .d(0L)
                    .isNullAllowed(true)
                    .build());

    public static final Long QUERY_1_RESULT_ID = 1L;
    public static final List<Map<String, Object>> QUERY_1_RESULT_DTO = new LinkedList<>(List.of(
            new HashMap<>() {{
                put("location", "Albury");
                put("lat", -36.0653583);
                put("lng", 146.9112214);
            }}, new HashMap<>() {{
                put("location", "Sydney");
                put("lat", -33.847927);
                put("lng", 150.6517942);
            }}));

    public static final String LICENSE_1_IDENTIFIER = "MIT";
    public static final String LICENSE_1_URI = "https://opensource.org/license/mit/";

    public static final License LICENSE_1 = License.builder()
            .identifier(LICENSE_1_IDENTIFIER)
            .uri(LICENSE_1_URI)
            .build();

    public static final LicenseDto LICENSE_1_DTO = LicenseDto.builder()
            .identifier(LICENSE_1_IDENTIFIER)
            .uri(LICENSE_1_URI)
            .build();

    public static final UUID CREATOR_1_ID = UUID.fromString("a0417f34-80ff-419f-821d-ce179021484a");
    public static final String CREATOR_1_ORCID = "00000-00000-00000";
    public static final String CREATOR_1_AFFIL = "TU Graz";
    public static final String CREATOR_1_AFFIL_ROR = "https://ror.org/04wn28048";
    public static final String CREATOR_1_AFFIL_URI = "https://ror.org/";
    public static final AffiliationIdentifierSchemeType CREATOR_1_AFFIL_TYPE = AffiliationIdentifierSchemeType.ROR;
    public static final AffiliationIdentifierSchemeTypeDto CREATOR_1_AFFIL_TYPE_DTO = AffiliationIdentifierSchemeTypeDto.ROR;
    public static final String CREATOR_1_FIRSTNAME = "Max";
    public static final String CREATOR_1_LASTNAME = "Mustermann";
    public static final String CREATOR_1_NAME = CREATOR_1_LASTNAME + ", " + CREATOR_1_FIRSTNAME;
    public static final Instant CREATOR_1_CREATED = Instant.ofEpochSecond(1641588352L);
    public static final Instant CREATOR_1_MODIFIED = Instant.ofEpochSecond(1541588352L);

    public static final OrcidDto ORCID_1_DTO = OrcidDto.builder()
            .person(OrcidPersonDto.builder()
                    .name(OrcidNameDto.builder()
                            .givenNames(OrcidValueDto.builder()
                                    .value(USER_1_FIRSTNAME)
                                    .build())
                            .familyName(OrcidValueDto.builder()
                                    .value(USER_1_LASTNAME)
                                    .build())
                            .build())
                    .build())
            .activitiesSummary(OrcidActivitiesSummaryDto.builder()
                    .employments(OrcidEmploymentsDto.builder()
                            .affiliationGroup(new OrcidAffiliationGroupDto[]{
                                    OrcidAffiliationGroupDto.builder()
                                            .summaries(new OrcidEmploymentSummaryDto[]{
                                                    OrcidEmploymentSummaryDto.builder()
                                                            .employmentSummary(OrcidSummaryDto.builder()
                                                                    .organization(OrcidOrganizationDto.builder()
                                                                            .name(USER_1_AFFILIATION)
                                                                            .build())
                                                                    .build())
                                                            .build()
                                            })
                                            .build()
                            })
                            .build())
                    .build())
            .build();

    public static final UUID CREATOR_2_ID = UUID.fromString("56b70dae-17a7-4f76-9c1e-a493762ba760");
    public static final Long CREATOR_2_QUERY_ID = 1L;
    public static final String CREATOR_2_ORCID = "00000-00000-00000";
    public static final String CREATOR_2_AFFIL = "TU Wien";
    public static final String CREATOR_2_FIRSTNAME = "Martina";
    public static final String CREATOR_2_LASTNAME = "Mustermann";
    public static final String CREATOR_2_NAME = CREATOR_2_LASTNAME + ", " + CREATOR_2_FIRSTNAME;
    public static final Instant CREATOR_2_CREATED = Instant.ofEpochSecond(1641588352L);
    public static final Instant CREATOR_2_MODIFIED = Instant.ofEpochSecond(1541588352L);

    public static final UUID CREATOR_3_ID = UUID.fromString("a2dfea46-7d88-4069-9b93-2417e1fb578b");
    public static final Long CREATOR_3_QUERY_ID = 1L;
    public static final String CREATOR_3_ORCID = "00000-00000-00000";
    public static final String CREATOR_3_AFFIL = "TU Graz";
    public static final String CREATOR_3_AFFIL_ROR = "https://ror.org/04wn28048";
    public static final AffiliationIdentifierSchemeType CREATOR_3_AFFIL_SCHEME_TYPE = AffiliationIdentifierSchemeType.ROR;
    public static final AffiliationIdentifierSchemeTypeDto CREATOR_3_AFFIL_SCHEME_TYPE_DTO = AffiliationIdentifierSchemeTypeDto.ROR;
    public static final String CREATOR_3_AFFIL_URI = "https://ror.org/";
    public static final String CREATOR_3_FIRSTNAME = "Max";
    public static final String CREATOR_3_LASTNAME = "Mustermann";
    public static final String CREATOR_3_NAME = CREATOR_3_LASTNAME + ", " + CREATOR_3_FIRSTNAME;
    public static final Instant CREATOR_3_CREATED = Instant.ofEpochSecond(1641588352L);
    public static final Instant CREATOR_3_MODIFIED = Instant.ofEpochSecond(1541588352L);

    public static final UUID CREATOR_4_ID = UUID.fromString("473489fa-ad02-4e48-856f-5a3f83ff541d");
    public static final Long CREATOR_4_QUERY_ID = 1L;
    public static final String CREATOR_4_ORCID = "00000-00000-00000";
    public static final String CREATOR_4_AFFIL = "TU Wien";
    public static final String CREATOR_4_AFFIL_ROR = "https://ror.org/04d836q62";
    public static final String CREATOR_4_AFFIL_URI = "https://ror.org/";
    public static final AffiliationIdentifierSchemeType CREATOR_4_AFFIL_TYPE = AffiliationIdentifierSchemeType.ROR;
    public static final AffiliationIdentifierSchemeTypeDto CREATOR_4_AFFIL_TYPE_DTO = AffiliationIdentifierSchemeTypeDto.ROR;
    public static final String CREATOR_4_FIRSTNAME = "Martina";
    public static final String CREATOR_4_LASTNAME = "Mustermann";
    public static final String CREATOR_4_NAME = CREATOR_4_LASTNAME + ", " + CREATOR_4_FIRSTNAME;
    public static final Instant CREATOR_4_CREATED = Instant.ofEpochSecond(1641588352L);
    public static final Instant CREATOR_4_MODIFIED = Instant.ofEpochSecond(1541588352L);

    public static final UUID IDENTIFIER_1_ID = UUID.fromString("679a83f2-ef23-4b4b-98f7-ad77b9d68733");
    public static final String IDENTIFIER_1_DOI = "10.12345/183";
    public static final Instant IDENTIFIER_1_CREATED = Instant.ofEpochSecond(1641588352L) /* 2022-01-07 20:45:52 */;
    public static final Instant IDENTIFIER_1_MODIFIED = Instant.ofEpochSecond(1541588352L) /* 2022-01-07 20:45:52 */;
    public static final Instant IDENTIFIER_1_EXECUTION = Instant.ofEpochSecond(1541588352L) /* 2022-01-07 20:45:52 */;
    public static final Integer IDENTIFIER_1_PUBLICATION_MONTH = 5;
    public static final Integer IDENTIFIER_1_PUBLICATION_YEAR = 2022;
    public static final Integer IDENTIFIER_1_PUBLICATION_DAY = null;
    public static final String IDENTIFIER_1_PUBLISHER = "Austrian Government";
    public static final IdentifierType IDENTIFIER_1_TYPE = IdentifierType.DATABASE;
    public static final IdentifierTypeDto IDENTIFIER_1_TYPE_DTO = IdentifierTypeDto.DATABASE;
    public static final IdentifierStatusType IDENTIFIER_1_STATUS_TYPE = IdentifierStatusType.PUBLISHED;
    public static final IdentifierStatusTypeDto IDENTIFIER_1_STATUS_TYPE_DTO = IdentifierStatusTypeDto.PUBLISHED;

    public static final UUID IDENTIFIER_1_TITLE_1_ID = UUID.fromString("3df6b286-9bd2-4ae3-b8f4-29c217544bef");
    public static final String IDENTIFIER_1_TITLE_1_TITLE = "Austrian weather data";
    public static final String IDENTIFIER_1_TITLE_1_TITLE_MODIFY = "Austrian weather some data";
    public static final TitleType IDENTIFIER_1_TITLE_1_TYPE = null;
    public static final TitleTypeDto IDENTIFIER_1_TITLE_1_TYPE_DTO = null;
    public static final LanguageType IDENTIFIER_1_TITLE_1_LANG = LanguageType.EN;
    public static final LanguageTypeDto IDENTIFIER_1_TITLE_1_LANG_DTO = LanguageTypeDto.EN;

    public static final IdentifierTitle IDENTIFIER_1_TITLE_1 = IdentifierTitle.builder()
            .id(IDENTIFIER_1_TITLE_1_ID)
            .title(IDENTIFIER_1_TITLE_1_TITLE)
            .titleType(IDENTIFIER_1_TITLE_1_TYPE)
            .language(IDENTIFIER_1_TITLE_1_LANG)
            .build();

    public static final IdentifierTitleDto IDENTIFIER_1_TITLE_1_DTO = IdentifierTitleDto.builder()
            .id(IDENTIFIER_1_TITLE_1_ID)
            .title(IDENTIFIER_1_TITLE_1_TITLE)
            .titleType(IDENTIFIER_1_TITLE_1_TYPE_DTO)
            .language(IDENTIFIER_1_TITLE_1_LANG_DTO)
            .build();

    public static final IdentifierTitleDto IDENTIFIER_1_TITLE_1_DTO_MODIFY = IdentifierTitleDto.builder()
            .id(IDENTIFIER_1_TITLE_1_ID)
            .title(IDENTIFIER_1_TITLE_1_TITLE_MODIFY)
            .titleType(IDENTIFIER_1_TITLE_1_TYPE_DTO)
            .language(IDENTIFIER_1_TITLE_1_LANG_DTO)
            .build();

    public static final SaveIdentifierTitleDto IDENTIFIER_1_TITLE_1_CREATE_DTO = SaveIdentifierTitleDto.builder()
            .title(IDENTIFIER_1_TITLE_1_TITLE)
            .titleType(IDENTIFIER_1_TITLE_1_TYPE_DTO)
            .language(IDENTIFIER_1_TITLE_1_LANG_DTO)
            .build();

    public static final SaveIdentifierTitleDto IDENTIFIER_1_TITLE_1_UPDATE_DTO = SaveIdentifierTitleDto.builder()
            .title(IDENTIFIER_1_TITLE_1_TITLE_MODIFY)
            .titleType(IDENTIFIER_1_TITLE_1_TYPE_DTO)
            .language(IDENTIFIER_1_TITLE_1_LANG_DTO)
            .build();

    public static final UUID IDENTIFIER_1_TITLE_2_ID = UUID.fromString("903a7e5b-8014-4b8a-b8fd-44f477880905");
    public static final String IDENTIFIER_1_TITLE_2_TITLE = "Österreichische Wetterdaten";
    public static final String IDENTIFIER_1_TITLE_2_TITLE_MODIFY = "Österreichische Wetterdaten übersetzt";
    public static final TitleType IDENTIFIER_1_TITLE_2_TYPE = TitleType.TRANSLATED_TITLE;
    public static final TitleTypeDto IDENTIFIER_1_TITLE_2_TYPE_DTO = TitleTypeDto.TRANSLATED_TITLE;
    public static final LanguageType IDENTIFIER_1_TITLE_2_LANG = LanguageType.EN;
    public static final LanguageTypeDto IDENTIFIER_1_TITLE_2_LANG_DTO = LanguageTypeDto.EN;

    public static final IdentifierTitle IDENTIFIER_1_TITLE_2 = IdentifierTitle.builder()
            .id(IDENTIFIER_1_TITLE_2_ID)
            .title(IDENTIFIER_1_TITLE_2_TITLE)
            .titleType(IDENTIFIER_1_TITLE_2_TYPE)
            .language(IDENTIFIER_1_TITLE_2_LANG)
            .build();

    public static final IdentifierTitleDto IDENTIFIER_1_TITLE_2_DTO = IdentifierTitleDto.builder()
            .id(IDENTIFIER_1_TITLE_2_ID)
            .title(IDENTIFIER_1_TITLE_2_TITLE)
            .titleType(IDENTIFIER_1_TITLE_2_TYPE_DTO)
            .language(IDENTIFIER_1_TITLE_2_LANG_DTO)
            .build();

    public static final IdentifierTitleDto IDENTIFIER_1_TITLE_2_DTO_MODIFY = IdentifierTitleDto.builder()
            .id(IDENTIFIER_1_TITLE_2_ID)
            .title(IDENTIFIER_1_TITLE_2_TITLE_MODIFY)
            .titleType(IDENTIFIER_1_TITLE_2_TYPE_DTO)
            .language(IDENTIFIER_1_TITLE_2_LANG_DTO)
            .build();

    public static final SaveIdentifierTitleDto IDENTIFIER_1_TITLE_2_CREATE_DTO = SaveIdentifierTitleDto.builder()
            .title(IDENTIFIER_1_TITLE_2_TITLE)
            .titleType(IDENTIFIER_1_TITLE_2_TYPE_DTO)
            .language(IDENTIFIER_1_TITLE_2_LANG_DTO)
            .build();

    public static final SaveIdentifierTitleDto IDENTIFIER_1_TITLE_2_UPDATE_DTO = SaveIdentifierTitleDto.builder()
            .title(IDENTIFIER_1_TITLE_2_TITLE_MODIFY)
            .titleType(IDENTIFIER_1_TITLE_2_TYPE_DTO)
            .language(IDENTIFIER_1_TITLE_2_LANG_DTO)
            .build();

    public static final UUID IDENTIFIER_1_DESCRIPTION_1_ID = UUID.fromString("1c438756-93f0-4797-983c-175a17e18c2c");
    public static final String IDENTIFIER_1_DESCRIPTION_1_DESCRIPTION = "Selecting all from the weather Austrian table";
    public static final String IDENTIFIER_1_DESCRIPTION_1_DESCRIPTION_MODIFY = "Selecting some from the weather Austrian table";
    public static final DescriptionType IDENTIFIER_1_DESCRIPTION_1_TYPE = null;
    public static final DescriptionTypeDto IDENTIFIER_1_DESCRIPTION_1_TYPE_DTO = null;
    public static final LanguageType IDENTIFIER_1_DESCRIPTION_1_LANG = LanguageType.EN;
    public static final LanguageTypeDto IDENTIFIER_1_DESCRIPTION_1_LANG_DTO = LanguageTypeDto.EN;

    public static final IdentifierDescription IDENTIFIER_1_DESCRIPTION_1 = IdentifierDescription.builder()
            .id(IDENTIFIER_1_DESCRIPTION_1_ID)
            .description(IDENTIFIER_1_DESCRIPTION_1_DESCRIPTION)
            .descriptionType(IDENTIFIER_1_DESCRIPTION_1_TYPE)
            .language(IDENTIFIER_1_DESCRIPTION_1_LANG)
            .build();

    public static final IdentifierDescriptionDto IDENTIFIER_1_DESCRIPTION_1_DTO = IdentifierDescriptionDto.builder()
            .id(IDENTIFIER_1_DESCRIPTION_1_ID)
            .description(IDENTIFIER_1_DESCRIPTION_1_DESCRIPTION)
            .descriptionType(IDENTIFIER_1_DESCRIPTION_1_TYPE_DTO)
            .language(IDENTIFIER_1_DESCRIPTION_1_LANG_DTO)
            .build();

    public static final IdentifierDescriptionDto IDENTIFIER_1_DESCRIPTION_1_DTO_MODIFY = IdentifierDescriptionDto.builder()
            .id(IDENTIFIER_1_DESCRIPTION_1_ID)
            .description(IDENTIFIER_1_DESCRIPTION_1_DESCRIPTION_MODIFY)
            .descriptionType(IDENTIFIER_1_DESCRIPTION_1_TYPE_DTO)
            .language(IDENTIFIER_1_DESCRIPTION_1_LANG_DTO)
            .build();

    public static final SaveIdentifierDescriptionDto IDENTIFIER_1_DESCRIPTION_1_CREATE_DTO = SaveIdentifierDescriptionDto.builder()
            .id(null)
            .description(IDENTIFIER_1_DESCRIPTION_1_DESCRIPTION)
            .descriptionType(IDENTIFIER_1_DESCRIPTION_1_TYPE_DTO)
            .language(IDENTIFIER_1_DESCRIPTION_1_LANG_DTO)
            .build();

    public static final UUID IDENTIFIER_1_CREATOR_1_ID = UUID.fromString("667cd1d6-4f94-4808-b5cb-12e5ec0788d8");
    public static final String IDENTIFIER_1_CREATOR_1_FIRSTNAME = CREATOR_1_FIRSTNAME;
    public static final String IDENTIFIER_1_CREATOR_1_LASTNAME = CREATOR_1_LASTNAME;
    public static final String IDENTIFIER_1_CREATOR_1_NAME = CREATOR_1_NAME;
    public static final String IDENTIFIER_1_CREATOR_1_ORCID = CREATOR_1_ORCID;
    public static final NameIdentifierSchemeType IDENTIFIER_1_CREATOR_1_IDENTIFIER_SCHEME_TYPE = NameIdentifierSchemeType.ORCID;
    public static final NameIdentifierSchemeTypeDto IDENTIFIER_1_CREATOR_1_IDENTIFIER_SCHEME_TYPE_DTO = NameIdentifierSchemeTypeDto.ORCID;
    public static final String IDENTIFIER_1_CREATOR_1_AFFILIATION = CREATOR_1_AFFIL;
    public static final String IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER = CREATOR_1_AFFIL_ROR;
    public static final AffiliationIdentifierSchemeType IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER_SCHEME = CREATOR_1_AFFIL_TYPE;
    public static final AffiliationIdentifierSchemeTypeDto IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER_SCHEME_DTO = CREATOR_1_AFFIL_TYPE_DTO;
    public static final String IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER_SCHEME_URI = CREATOR_1_AFFIL_URI;

    public static final Creator IDENTIFIER_1_CREATOR_1 = Creator.builder()
            .id(IDENTIFIER_1_CREATOR_1_ID)
            .firstname(IDENTIFIER_1_CREATOR_1_FIRSTNAME)
            .lastname(IDENTIFIER_1_CREATOR_1_LASTNAME)
            .creatorName(IDENTIFIER_1_CREATOR_1_NAME)
            .nameType(NameType.PERSONAL)
            .nameIdentifier(IDENTIFIER_1_CREATOR_1_ORCID)
            .nameIdentifierScheme(IDENTIFIER_1_CREATOR_1_IDENTIFIER_SCHEME_TYPE)
            .affiliation(IDENTIFIER_1_CREATOR_1_AFFILIATION)
            .affiliationIdentifier(IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER)
            .affiliationIdentifierScheme(IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER_SCHEME)
            .affiliationIdentifierSchemeUri(IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER_SCHEME_URI)
            .build();

    public static final CreatorDto IDENTIFIER_1_CREATOR_1_DTO = CreatorDto.builder()
            .id(IDENTIFIER_1_CREATOR_1_ID)
            .firstname(IDENTIFIER_1_CREATOR_1_FIRSTNAME)
            .lastname(IDENTIFIER_1_CREATOR_1_LASTNAME)
            .creatorName(IDENTIFIER_1_CREATOR_1_NAME)
            .nameType(NameTypeDto.PERSONAL)
            .nameIdentifier(IDENTIFIER_1_CREATOR_1_ORCID)
            .nameIdentifierScheme(IDENTIFIER_1_CREATOR_1_IDENTIFIER_SCHEME_TYPE_DTO)
            .affiliation(IDENTIFIER_1_CREATOR_1_AFFILIATION)
            .affiliationIdentifier(IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER)
            .affiliationIdentifierScheme(IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER_SCHEME_DTO)
            .affiliationIdentifierSchemeUri(IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER_SCHEME_URI)
            .build();

    public static final SaveIdentifierCreatorDto IDENTIFIER_1_CREATOR_1_CREATE_DTO = SaveIdentifierCreatorDto.builder()
            .id(null)
            .firstname(IDENTIFIER_1_CREATOR_1_FIRSTNAME)
            .lastname(IDENTIFIER_1_CREATOR_1_LASTNAME)
            .creatorName(IDENTIFIER_1_CREATOR_1_NAME)
            .nameType(NameTypeDto.PERSONAL)
            .nameIdentifier(IDENTIFIER_1_CREATOR_1_ORCID)
            .nameIdentifierScheme(IDENTIFIER_1_CREATOR_1_IDENTIFIER_SCHEME_TYPE_DTO)
            .affiliation(IDENTIFIER_1_CREATOR_1_AFFILIATION)
            .affiliationIdentifier(IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER)
            .affiliationIdentifierScheme(IDENTIFIER_1_CREATOR_1_AFFILIATION_IDENTIFIER_SCHEME_DTO)
            .build();

    public static final UUID FUNDER_1_ID = UUID.fromString("8deb273d-6dd6-407d-970a-01534035ac01");
    public static final String FUNDER_1_NAME = "European Commission";
    public static final String FUNDER_1_IDENTIFIER = "https://doi.org/10.13039/501100000780";
    public static final String FUNDER_1_IDENTIFIER_ID_ONLY = "10.13039/501100000780";
    public static final IdentifierFunderType FUNDER_1_IDENTIFIER_TYPE = IdentifierFunderType.CROSSREF_FUNDER_ID;
    public static final IdentifierFunderTypeDto FUNDER_1_IDENTIFIER_TYPE_DTO = IdentifierFunderTypeDto.CROSSREF_FUNDER_ID;
    public static final String FUNDER_1_AWARD_TITLE = "Institutionalizing global genetic-resource commons. Global Strategies for accessing and using essential public knowledge assets in the life science";

    public static final IdentifierFunder IDENTIFIER_1_FUNDER_1 = IdentifierFunder.builder()
            .id(FUNDER_1_ID)
            .funderName(FUNDER_1_NAME)
            .funderIdentifier(FUNDER_1_IDENTIFIER)
            .funderIdentifierType(FUNDER_1_IDENTIFIER_TYPE)
            .awardTitle(FUNDER_1_AWARD_TITLE)
            .build();

    public static final IdentifierFunderDto IDENTIFIER_1_FUNDER_1_DTO = IdentifierFunderDto.builder()
            .id(FUNDER_1_ID)
            .funderName(FUNDER_1_NAME)
            .funderIdentifier(FUNDER_1_IDENTIFIER)
            .funderIdentifierType(FUNDER_1_IDENTIFIER_TYPE_DTO)
            .awardTitle(FUNDER_1_AWARD_TITLE)
            .build();

    public static final SaveIdentifierFunderDto IDENTIFIER_1_FUNDER_1_CREATE_DTO = SaveIdentifierFunderDto.builder()
            .funderName(FUNDER_1_NAME)
            .funderIdentifier(FUNDER_1_IDENTIFIER)
            .funderIdentifierType(FUNDER_1_IDENTIFIER_TYPE_DTO)
            .awardTitle(FUNDER_1_AWARD_TITLE)
            .build();

    public static final DataCiteBody<DataCiteDoi> IDENTIFIER_1_DATA_CITE = DataCiteBody.<DataCiteDoi>builder()
            .data(DataCiteData.<DataCiteDoi>builder()
                    .type("dois")
                    .attributes(DataCiteDoi.builder()
                            .doi(IDENTIFIER_1_DOI)
                            .build())
                    .build())
            .build();

    public static final Identifier IDENTIFIER_1 = Identifier.builder()
            .id(IDENTIFIER_1_ID)
            .queryId(QUERY_1_ID)
            .titles(new LinkedList<>(List.of(IDENTIFIER_1_TITLE_1, IDENTIFIER_1_TITLE_2)))
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_1_DESCRIPTION_1)))
            .doi(IDENTIFIER_1_DOI)
            .database(null /* DATABASE_1 */)
            .created(IDENTIFIER_1_CREATED)
            .lastModified(IDENTIFIER_1_MODIFIED)
            .execution(IDENTIFIER_1_EXECUTION)
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .publicationMonth(IDENTIFIER_1_PUBLICATION_MONTH)
            .queryHash(QUERY_1_QUERY_HASH)
            .resultHash(QUERY_1_RESULT_HASH)
            .query(QUERY_1_STATEMENT)
            .queryNormalized(QUERY_1_STATEMENT)
            .resultNumber(QUERY_1_RESULT_NUMBER)
            .publisher(IDENTIFIER_1_PUBLISHER)
            .type(IDENTIFIER_1_TYPE)
            .owner(USER_1)
            .ownedBy(USER_1_ID)
            .licenses(new LinkedList<>(List.of(LICENSE_1)))
            .creators(new LinkedList<>(List.of(IDENTIFIER_1_CREATOR_1)))
            .funders(new LinkedList<>(List.of(IDENTIFIER_1_FUNDER_1)))
            .status(IDENTIFIER_1_STATUS_TYPE)
            .build();

    public static final Identifier IDENTIFIER_1_WITH_DOI = Identifier.builder()
            .id(IDENTIFIER_1_ID)
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_1_DESCRIPTION_1)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_1_TITLE_1, IDENTIFIER_1_TITLE_2)))
            .doi(IDENTIFIER_1_DOI)
            .database(null /* for jpa */)
            .created(IDENTIFIER_1_CREATED)
            .lastModified(IDENTIFIER_1_MODIFIED)
            .execution(IDENTIFIER_1_EXECUTION)
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .publicationMonth(IDENTIFIER_1_PUBLICATION_MONTH)
            .queryHash(QUERY_1_QUERY_HASH)
            .resultHash(QUERY_1_RESULT_HASH)
            .query(QUERY_1_STATEMENT)
            .queryNormalized(QUERY_1_STATEMENT)
            .resultNumber(QUERY_1_RESULT_NUMBER)
            .publisher(IDENTIFIER_1_PUBLISHER)
            .type(IDENTIFIER_1_TYPE)
            .owner(USER_1)
            .licenses(new LinkedList<>(List.of(LICENSE_1)))
            .creators(new LinkedList<>(List.of(IDENTIFIER_1_CREATOR_1)))
            .funders(new LinkedList<>(List.of(IDENTIFIER_1_FUNDER_1)))
            .status(IDENTIFIER_1_STATUS_TYPE)
            .build();

    public static final IdentifierDto IDENTIFIER_1_DTO = IdentifierDto.builder()
            .id(IDENTIFIER_1_ID)
            .databaseId(DATABASE_1_ID)
            .links(LinksDto.builder()
                    .self("/api/identifier/" + IDENTIFIER_1_ID)
                    .selfHtml("/pid/" + IDENTIFIER_1_ID)
                    .build())
            .queryId(QUERY_1_ID)
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_1_DESCRIPTION_1_DTO)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_1_TITLE_1_DTO, IDENTIFIER_1_TITLE_2_DTO)))
            .doi(IDENTIFIER_1_DOI)
            .execution(IDENTIFIER_1_EXECUTION)
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .publicationMonth(IDENTIFIER_1_PUBLICATION_MONTH)
            .queryHash(QUERY_1_QUERY_HASH)
            .resultHash(QUERY_1_RESULT_HASH)
            .query(QUERY_1_STATEMENT)
            .queryNormalized(QUERY_1_STATEMENT)
            .resultNumber(QUERY_1_RESULT_NUMBER)
            .publisher(IDENTIFIER_1_PUBLISHER)
            .type(IDENTIFIER_1_TYPE_DTO)
            .owner(USER_1_BRIEF_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .creators(new LinkedList<>(List.of(IDENTIFIER_1_CREATOR_1_DTO)))
            .funders(new LinkedList<>(List.of(IDENTIFIER_1_FUNDER_1_DTO)))
            .status(IDENTIFIER_1_STATUS_TYPE_DTO)
            .build();

    public static final IdentifierBriefDto IDENTIFIER_1_BRIEF_DTO = IdentifierBriefDto.builder()
            .id(IDENTIFIER_1_ID)
            .databaseId(DATABASE_1_ID)
            .titles(new LinkedList<>(List.of(IDENTIFIER_1_TITLE_1_DTO, IDENTIFIER_1_TITLE_2_DTO)))
            .doi(IDENTIFIER_1_DOI)
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_1_PUBLISHER)
            .type(IDENTIFIER_1_TYPE_DTO)
            .status(IDENTIFIER_1_STATUS_TYPE_DTO)
            .build();

    public static final CreateIdentifierDto IDENTIFIER_1_CREATE_DTO = CreateIdentifierDto.builder()
            .databaseId(DATABASE_1_ID)
            .type(IDENTIFIER_1_TYPE_DTO)
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_1_PUBLISHER)
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_1_DESCRIPTION_1_CREATE_DTO)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_1_TITLE_1_CREATE_DTO, IDENTIFIER_1_TITLE_2_CREATE_DTO)))
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .publicationMonth(IDENTIFIER_1_PUBLICATION_MONTH)
            .publisher(IDENTIFIER_1_PUBLISHER)
            .type(IDENTIFIER_1_TYPE_DTO)
            .doi(IDENTIFIER_1_DOI)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .creators(new LinkedList<>(List.of(IDENTIFIER_1_CREATOR_1_CREATE_DTO)))
            .funders(new LinkedList<>(List.of(IDENTIFIER_1_FUNDER_1_CREATE_DTO)))
            .build();

    public static final CreateIdentifierDto IDENTIFIER_1_CREATE_WITH_DOI_DTO = CreateIdentifierDto.builder()
            .databaseId(DATABASE_1_ID)
            .type(IDENTIFIER_1_TYPE_DTO)
            .doi(IDENTIFIER_1_DOI)
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_1_PUBLISHER)
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_1_DESCRIPTION_1_CREATE_DTO)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_1_TITLE_1_CREATE_DTO, IDENTIFIER_1_TITLE_2_CREATE_DTO)))
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .publicationMonth(IDENTIFIER_1_PUBLICATION_MONTH)
            .publisher(IDENTIFIER_1_PUBLISHER)
            .type(IDENTIFIER_1_TYPE_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .creators(new LinkedList<>(List.of(IDENTIFIER_1_CREATOR_1_CREATE_DTO)))
            .funders(new LinkedList<>(List.of(IDENTIFIER_1_FUNDER_1_CREATE_DTO)))
            .build();

    public static final IdentifierSaveDto IDENTIFIER_1_SAVE_DTO = IdentifierSaveDto.builder()
            .id(IDENTIFIER_1_ID)
            .databaseId(DATABASE_1_ID)
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_1_DESCRIPTION_1_CREATE_DTO)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_1_TITLE_1_CREATE_DTO, IDENTIFIER_1_TITLE_2_CREATE_DTO)))
            .relatedIdentifiers(new LinkedList<>())
            .publicationMonth(IDENTIFIER_1_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .creators(new LinkedList<>(List.of(IDENTIFIER_1_CREATOR_1_CREATE_DTO)))
            .funders(new LinkedList<>(List.of(IDENTIFIER_1_FUNDER_1_CREATE_DTO)))
            .publisher(IDENTIFIER_1_PUBLISHER)
            .type(IDENTIFIER_1_TYPE_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .build();

    public static final IdentifierSaveDto IDENTIFIER_1_SAVE_MODIFY_DTO = IdentifierSaveDto.builder()
            .id(IDENTIFIER_1_ID)
            .databaseId(DATABASE_1_ID)
            .descriptions(new LinkedList<>(List.of())) // <<<
            .titles(new LinkedList<>(List.of(IDENTIFIER_1_TITLE_1_CREATE_DTO))) // <<<
            .relatedIdentifiers(new LinkedList<>())
            .publicationMonth(IDENTIFIER_1_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_1_PUBLICATION_YEAR)
            .creators(new LinkedList<>(List.of())) // <<<
            .funders(new LinkedList<>(List.of())) // <<<
            .publisher(IDENTIFIER_1_PUBLISHER)
            .type(IDENTIFIER_1_TYPE_DTO)
            .licenses(new LinkedList<>(List.of())) // <<<
            .build();

    public static final UUID IDENTIFIER_5_ID = UUID.fromString("e05bb4c9-ed26-48c9-bd91-5c48a93a04bd");
    public static final String IDENTIFIER_5_DOI = "10.12345/13/50BBFCFE08A12";
    public static final Instant IDENTIFIER_5_CREATED = Instant.ofEpochSecond(1641588352L);
    public static final Instant IDENTIFIER_5_MODIFIED = Instant.ofEpochSecond(1541588352L);
    public static final Instant IDENTIFIER_5_EXECUTION = Instant.ofEpochSecond(1541588352L);
    public static final Integer IDENTIFIER_5_PUBLICATION_DAY = 14;
    public static final Integer IDENTIFIER_5_PUBLICATION_MONTH = 7;
    public static final Integer IDENTIFIER_5_PUBLICATION_YEAR = 2022;
    public static final String IDENTIFIER_5_QUERY_HASH = QUERY_2_QUERY_HASH;
    public static final String IDENTIFIER_5_RESULT_HASH = QUERY_2_RESULT_HASH;
    public static final String IDENTIFIER_5_QUERY = QUERY_2_STATEMENT;
    public static final String IDENTIFIER_5_NORMALIZED = QUERY_2_STATEMENT;
    public static final Long IDENTIFIER_5_RESULT_NUMBER = QUERY_2_RESULT_NUMBER;
    public static final String IDENTIFIER_5_PUBLISHER = "Australian Government";
    public static final IdentifierType IDENTIFIER_5_TYPE = IdentifierType.SUBSET;
    public static final IdentifierTypeDto IDENTIFIER_5_TYPE_DTO = IdentifierTypeDto.SUBSET;
    public static final IdentifierStatusType IDENTIFIER_5_STATUS_TYPE = IdentifierStatusType.DRAFT;
    public static final IdentifierStatusTypeDto IDENTIFIER_5_STATUS_TYPE_DTO = IdentifierStatusTypeDto.DRAFT;
    public static final UUID IDENTIFIER_5_CREATED_BY = USER_2_ID;

    public static final UUID IDENTIFIER_5_TITLE_1_ID = UUID.fromString("1a0ae9c2-61c6-44f8-b886-26a4f4dabc52");
    public static final String IDENTIFIER_5_TITLE_1_TITLE = "Australische Wetterdaten";
    public static final LanguageType IDENTIFIER_5_TITLE_1_LANG = LanguageType.DE;
    public static final LanguageTypeDto IDENTIFIER_5_TITLE_1_LANG_DTO = LanguageTypeDto.DE;
    public static final TitleType IDENTIFIER_5_TITLE_1_TYPE = TitleType.SUBTITLE;
    public static final TitleTypeDto IDENTIFIER_5_TITLE_1_TYPE_DTO = TitleTypeDto.SUBTITLE;

    public static final IdentifierTitle IDENTIFIER_5_TITLE_1 = IdentifierTitle.builder()
            .id(IDENTIFIER_5_TITLE_1_ID)
            .title(IDENTIFIER_5_TITLE_1_TITLE)
            .language(IDENTIFIER_5_TITLE_1_LANG)
            .titleType(IDENTIFIER_5_TITLE_1_TYPE)
            .build();

    public static final IdentifierTitleDto IDENTIFIER_5_TITLE_1_DTO = IdentifierTitleDto.builder()
            .id(IDENTIFIER_5_TITLE_1_ID)
            .title(IDENTIFIER_5_TITLE_1_TITLE)
            .language(IDENTIFIER_5_TITLE_1_LANG_DTO)
            .titleType(IDENTIFIER_5_TITLE_1_TYPE_DTO)
            .build();

    public static final SaveIdentifierTitleDto IDENTIFIER_5_TITLE_1_CREATE_DTO = SaveIdentifierTitleDto.builder()
            .title(IDENTIFIER_5_TITLE_1_TITLE)
            .language(IDENTIFIER_5_TITLE_1_LANG_DTO)
            .titleType(IDENTIFIER_5_TITLE_1_TYPE_DTO)
            .build();

    public static final UUID IDENTIFIER_5_DESCRIPTION_1_ID = UUID.fromString("ab49bdca-f373-4823-9947-2a0cbfa88350");
    public static final String IDENTIFIER_5_DESCRIPTION_1_DESCRIPTION = "Alle Wetterdaten in Australien";
    public static final LanguageType IDENTIFIER_5_DESCRIPTION_1_LANG = LanguageType.DE;
    public static final LanguageTypeDto IDENTIFIER_5_DESCRIPTION_1_LANG_DTO = LanguageTypeDto.DE;
    public static final DescriptionType IDENTIFIER_5_DESCRIPTION_1_TYPE = DescriptionType.ABSTRACT;
    public static final DescriptionTypeDto IDENTIFIER_5_DESCRIPTION_1_TYPE_DTO = DescriptionTypeDto.ABSTRACT;

    public static final IdentifierDescription IDENTIFIER_5_DESCRIPTION_1 = IdentifierDescription.builder()
            .id(IDENTIFIER_5_DESCRIPTION_1_ID)
            .description(IDENTIFIER_5_DESCRIPTION_1_DESCRIPTION)
            .language(IDENTIFIER_5_DESCRIPTION_1_LANG)
            .descriptionType(IDENTIFIER_5_DESCRIPTION_1_TYPE)
            .build();

    public static final IdentifierDescriptionDto IDENTIFIER_5_DESCRIPTION_1_DTO = IdentifierDescriptionDto.builder()
            .id(IDENTIFIER_5_DESCRIPTION_1_ID)
            .description(IDENTIFIER_5_DESCRIPTION_1_DESCRIPTION)
            .language(IDENTIFIER_5_DESCRIPTION_1_LANG_DTO)
            .descriptionType(IDENTIFIER_5_DESCRIPTION_1_TYPE_DTO)
            .build();

    public static final SaveIdentifierDescriptionDto IDENTIFIER_5_DESCRIPTION_1_CREATE_DTO = SaveIdentifierDescriptionDto.builder()
            .id(null)
            .description(IDENTIFIER_5_DESCRIPTION_1_DESCRIPTION)
            .language(IDENTIFIER_5_DESCRIPTION_1_LANG_DTO)
            .descriptionType(IDENTIFIER_5_DESCRIPTION_1_TYPE_DTO)
            .build();

    public static final UUID IDENTIFIER_5_CREATOR_1_ID = UUID.fromString("6844b684-93e4-47d2-a615-5939127fdafe");

    public static final Creator IDENTIFIER_5_CREATOR_1 = Creator.builder()
            .id(IDENTIFIER_5_CREATOR_1_ID)
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeType.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_1_AFFIL_TYPE)
            .affiliationIdentifierSchemeUri(CREATOR_1_AFFIL_URI)
            .build();

    public static final CreatorDto IDENTIFIER_5_CREATOR_1_DTO = CreatorDto.builder()
            .id(IDENTIFIER_5_CREATOR_1_ID)
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_1_AFFIL_TYPE_DTO)
            .affiliationIdentifierSchemeUri(CREATOR_1_AFFIL_URI)
            .build();

    public static final SaveIdentifierCreatorDto IDENTIFIER_5_CREATOR_1_CREATE_DTO = SaveIdentifierCreatorDto.builder()
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .build();

    public static final SaveIdentifierCreatorDto IDENTIFIER_5_CREATOR_1_MODIFY_DTO = SaveIdentifierCreatorDto.builder()
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(null) /* <<<< */
            .nameIdentifierScheme(null) /* <<<< */
            .affiliation(CREATOR_1_AFFIL)
            .build();

    public static final UUID IDENTIFIER_5_CREATOR_2_ID = UUID.fromString("14943ad6-a935-49f5-b07e-f9eb789b8604");

    public static final Creator IDENTIFIER_5_CREATOR_2 = Creator.builder()
            .id(IDENTIFIER_5_CREATOR_2_ID)
            .firstname(CREATOR_2_FIRSTNAME)
            .lastname(CREATOR_2_LASTNAME)
            .creatorName(CREATOR_2_NAME)
            .nameIdentifier(CREATOR_2_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeType.ORCID)
            .affiliation(CREATOR_2_AFFIL)
            .build();

    public static final CreatorDto IDENTIFIER_5_CREATOR_2_DTO = CreatorDto.builder()
            .id(IDENTIFIER_5_CREATOR_2_ID)
            .firstname(CREATOR_2_FIRSTNAME)
            .lastname(CREATOR_2_LASTNAME)
            .creatorName(CREATOR_2_NAME)
            .nameIdentifier(CREATOR_2_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_2_AFFIL)
            .build();

    public static final SaveIdentifierCreatorDto IDENTIFIER_5_CREATOR_2_CREATE_DTO = SaveIdentifierCreatorDto.builder()
            .firstname(CREATOR_2_FIRSTNAME)
            .lastname(CREATOR_2_LASTNAME)
            .creatorName(CREATOR_2_NAME)
            .nameIdentifier(CREATOR_2_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_2_AFFIL)
            .build();

    public static final SaveIdentifierCreatorDto IDENTIFIER_5_CREATOR_2_MODIFY_DTO = SaveIdentifierCreatorDto.builder()
            .firstname(CREATOR_2_FIRSTNAME)
            .lastname(CREATOR_2_LASTNAME)
            .creatorName(CREATOR_2_NAME)
            .nameIdentifier(null) /* <<<< */
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_2_AFFIL)
            .build();

    public static final Identifier IDENTIFIER_5 = Identifier.builder()
            .id(IDENTIFIER_5_ID)
            .queryId(QUERY_2_ID)
            .database(null) /* DATABASE_2 */
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_5_DESCRIPTION_1)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_5_TITLE_1)))
            .doi(IDENTIFIER_5_DOI)
            .created(IDENTIFIER_5_CREATED)
            .lastModified(IDENTIFIER_5_MODIFIED)
            .execution(IDENTIFIER_5_EXECUTION)
            .publicationDay(IDENTIFIER_5_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_5_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_5_PUBLICATION_YEAR)
            .queryHash(IDENTIFIER_5_QUERY_HASH)
            .resultHash(IDENTIFIER_5_RESULT_HASH)
            .query(IDENTIFIER_5_QUERY)
            .queryNormalized(IDENTIFIER_5_NORMALIZED)
            .resultNumber(IDENTIFIER_5_RESULT_NUMBER)
            .publisher(IDENTIFIER_5_PUBLISHER)
            .type(IDENTIFIER_5_TYPE)
            .owner(USER_2)
            .ownedBy(USER_2_ID)
            .creators(new LinkedList<>(List.of(IDENTIFIER_5_CREATOR_1, IDENTIFIER_5_CREATOR_2)))
            .status(IDENTIFIER_5_STATUS_TYPE)
            .build();

    public static final IdentifierDto IDENTIFIER_5_DTO = IdentifierDto.builder()
            .id(IDENTIFIER_5_ID)
            .databaseId(DATABASE_2_ID)
            .queryId(QUERY_2_ID)
            .links(LinksDto.builder()
                    .self("/api/identifier/" + IDENTIFIER_5_ID)
                    .selfHtml("/pid/" + IDENTIFIER_5_ID)
                    .data("/api/database/" + DATABASE_2_ID + "/subset/" + QUERY_2_ID + "/data")
                    .build())
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_5_DESCRIPTION_1_DTO)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_5_TITLE_1_DTO)))
            .doi(IDENTIFIER_5_DOI)
            .execution(IDENTIFIER_5_EXECUTION)
            .publicationDay(IDENTIFIER_5_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_5_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_5_PUBLICATION_YEAR)
            .queryHash(IDENTIFIER_5_QUERY_HASH)
            .resultHash(IDENTIFIER_5_RESULT_HASH)
            .query(IDENTIFIER_5_QUERY)
            .queryNormalized(IDENTIFIER_5_NORMALIZED)
            .resultNumber(IDENTIFIER_5_RESULT_NUMBER)
            .publisher(IDENTIFIER_5_PUBLISHER)
            .type(IDENTIFIER_5_TYPE_DTO)
            .owner(USER_2_BRIEF_DTO)
            .status(IDENTIFIER_5_STATUS_TYPE_DTO)
            .creators(new LinkedList<>(List.of(IDENTIFIER_5_CREATOR_1_DTO, IDENTIFIER_5_CREATOR_2_DTO)))
            .build();

    public static final IdentifierBriefDto IDENTIFIER_5_BRIEF_DTO = IdentifierBriefDto.builder()
            .id(IDENTIFIER_5_ID)
            .databaseId(DATABASE_2_ID)
            .queryId(QUERY_2_ID)
            .titles(new LinkedList<>(List.of(IDENTIFIER_5_TITLE_1_DTO)))
            .doi(IDENTIFIER_5_DOI)
            .publicationYear(IDENTIFIER_5_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_5_PUBLISHER)
            .type(IDENTIFIER_5_TYPE_DTO)
            .build();

    public static final UUID RELATED_IDENTIFIER_5_ID = UUID.fromString("26545877-574d-44fa-819d-d9d9a9750b38");
    public static final String RELATED_IDENTIFIER_5_VALUE = "10.5281/zenodo.6637333";
    public static final RelatedType RELATED_IDENTIFIER_5_TYPE = RelatedType.DOI;
    public static final RelatedTypeDto RELATED_IDENTIFIER_5_TYPE_DTO = RelatedTypeDto.DOI;
    public static final RelationType RELATED_IDENTIFIER_5_RELATION_TYPE = RelationType.CITES;
    public static final RelationTypeDto RELATED_IDENTIFIER_5_RELATION_TYPE_DTO = RelationTypeDto.CITES;

    public static final RelatedIdentifier IDENTIFIER_1_RELATED_IDENTIFIER_1 = RelatedIdentifier.builder()
            .id(RELATED_IDENTIFIER_5_ID)
            .identifier(IDENTIFIER_5)
            .type(RELATED_IDENTIFIER_5_TYPE)
            .relation(RELATED_IDENTIFIER_5_RELATION_TYPE)
            .value(RELATED_IDENTIFIER_5_VALUE)
            .build();

    public static final SaveRelatedIdentifierDto IDENTIFIER_1_RELATED_IDENTIFIER_5_CREATE_DTO = SaveRelatedIdentifierDto.builder()
            .value(RELATED_IDENTIFIER_5_VALUE)
            .type(RELATED_IDENTIFIER_5_TYPE_DTO)
            .relation(RELATED_IDENTIFIER_5_RELATION_TYPE_DTO)
            .build();

    public static final CreateIdentifierDto IDENTIFIER_5_CREATE_DTO = CreateIdentifierDto.builder()
            .databaseId(DATABASE_2_ID)
            .publicationYear(IDENTIFIER_5_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_5_PUBLISHER)
            .build();

    public static final IdentifierSaveDto IDENTIFIER_5_SAVE_DTO = IdentifierSaveDto.builder()
            .id(IDENTIFIER_5_ID)
            .queryId(QUERY_2_ID)
            .databaseId(DATABASE_2_ID)
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_5_DESCRIPTION_1_CREATE_DTO)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_5_TITLE_1_CREATE_DTO)))
            .relatedIdentifiers(new LinkedList<>(List.of(IDENTIFIER_1_RELATED_IDENTIFIER_5_CREATE_DTO)))
            .publicationDay(IDENTIFIER_5_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_5_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_5_PUBLICATION_YEAR)
            .creators(new LinkedList<>(List.of(IDENTIFIER_5_CREATOR_1_CREATE_DTO, IDENTIFIER_5_CREATOR_2_CREATE_DTO)))
            .publisher(IDENTIFIER_5_PUBLISHER)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .type(IDENTIFIER_5_TYPE_DTO)
            .build();

    public static final UUID IDENTIFIER_6_ID = UUID.fromString("a244204d-9671-42a0-be07-9b14402238fd");
    public static final String IDENTIFIER_6_DOI = null;
    public static final Instant IDENTIFIER_6_CREATED = Instant.ofEpochSecond(1641588352L);
    public static final Instant IDENTIFIER_6_MODIFIED = Instant.ofEpochSecond(1541588352L);
    public static final Instant IDENTIFIER_6_EXECUTION = Instant.ofEpochSecond(1541588352L);
    public static final Integer IDENTIFIER_6_PUBLICATION_DAY = 14;
    public static final Integer IDENTIFIER_6_PUBLICATION_MONTH = 7;
    public static final Integer IDENTIFIER_6_PUBLICATION_YEAR = 2022;
    public static final String IDENTIFIER_6_QUERY_HASH = QUERY_3_QUERY_HASH;
    public static final String IDENTIFIER_6_RESULT_HASH = QUERY_3_RESULT_HASH;
    public static final String IDENTIFIER_6_QUERY = QUERY_3_STATEMENT;
    public static final String IDENTIFIER_6_NORMALIZED = QUERY_3_STATEMENT;
    public static final Long IDENTIFIER_6_RESULT_NUMBER = QUERY_3_RESULT_NUMBER;
    public static final String IDENTIFIER_6_PUBLISHER = "Norwegian Government";
    public static final IdentifierType IDENTIFIER_6_TYPE = IdentifierType.SUBSET;
    public static final IdentifierTypeDto IDENTIFIER_6_TYPE_DTO = IdentifierTypeDto.SUBSET;
    public static final IdentifierStatusType IDENTIFIER_6_STATUS_TYPE = IdentifierStatusType.PUBLISHED;
    public static final IdentifierStatusTypeDto IDENTIFIER_6_STATUS_TYPE_DTO = IdentifierStatusTypeDto.PUBLISHED;

    public static final UUID IDENTIFIER_6_TITLE_1_ID = UUID.fromString("0449011c-1490-4c8e-b46c-c1f862126aea");
    public static final String IDENTIFIER_6_TITLE_1_TITLE = "Norwegian weather data";
    public static final String IDENTIFIER_6_TITLE_1_TITLE_MODIFY = "Norwegian weather some data";
    public static final LanguageType IDENTIFIER_6_TITLE_1_LANG = LanguageType.EN;
    public static final LanguageTypeDto IDENTIFIER_6_TITLE_1_LANG_DTO = LanguageTypeDto.EN;

    public static final IdentifierTitle IDENTIFIER_6_TITLE_1 = IdentifierTitle.builder()
            .id(IDENTIFIER_6_TITLE_1_ID)
            .title(IDENTIFIER_6_TITLE_1_TITLE)
            .language(IDENTIFIER_6_TITLE_1_LANG)
            .build();

    public static final IdentifierTitleDto IDENTIFIER_6_TITLE_1_DTO = IdentifierTitleDto.builder()
            .id(IDENTIFIER_6_TITLE_1_ID)
            .title(IDENTIFIER_6_TITLE_1_TITLE)
            .language(IDENTIFIER_6_TITLE_1_LANG_DTO)
            .build();

    public static final IdentifierTitleDto IDENTIFIER_6_TITLE_1_DTO_MODIFY = IdentifierTitleDto.builder()
            .id(IDENTIFIER_6_TITLE_1_ID)
            .title(IDENTIFIER_6_TITLE_1_TITLE_MODIFY)
            .language(IDENTIFIER_6_TITLE_1_LANG_DTO)
            .build();

    public static final SaveIdentifierTitleDto IDENTIFIER_6_TITLE_1_CREATE_DTO = SaveIdentifierTitleDto.builder()
            .title(IDENTIFIER_6_TITLE_1_TITLE_MODIFY)
            .language(IDENTIFIER_6_TITLE_1_LANG_DTO)
            .build();

    public static final UUID IDENTIFIER_6_DESCRIPTION_1_ID = UUID.fromString("aac03bbd-27e6-419d-8118-f996d594f00f");
    public static final String IDENTIFIER_6_DESCRIPTION_1_DESCRIPTION = "Selecting all from the weather Norwegian table";
    public static final String IDENTIFIER_6_DESCRIPTION_1_DESCRIPTION_MODIFY = "Selecting some from the weather Norwegian table";
    public static final LanguageType IDENTIFIER_6_DESCRIPTION_1_LANG = LanguageType.EN;
    public static final LanguageTypeDto IDENTIFIER_6_DESCRIPTION_1_LANG_DTO = LanguageTypeDto.EN;

    public static final IdentifierDescription IDENTIFIER_6_DESCRIPTION_1 = IdentifierDescription.builder()
            .id(IDENTIFIER_6_DESCRIPTION_1_ID)
            .description(IDENTIFIER_6_DESCRIPTION_1_DESCRIPTION)
            .language(IDENTIFIER_6_DESCRIPTION_1_LANG)
            .build();

    public static final IdentifierDescriptionDto IDENTIFIER_6_DESCRIPTION_1_DTO = IdentifierDescriptionDto.builder()
            .id(IDENTIFIER_6_DESCRIPTION_1_ID)
            .description(IDENTIFIER_6_DESCRIPTION_1_DESCRIPTION)
            .language(IDENTIFIER_6_DESCRIPTION_1_LANG_DTO)
            .build();

    public static final IdentifierDescriptionDto IDENTIFIER_6_DESCRIPTION_1_DTO_MODIFY = IdentifierDescriptionDto.builder()
            .id(IDENTIFIER_6_DESCRIPTION_1_ID)
            .description(IDENTIFIER_6_DESCRIPTION_1_DESCRIPTION_MODIFY)
            .language(IDENTIFIER_6_DESCRIPTION_1_LANG_DTO)
            .build();

    public static final SaveIdentifierDescriptionDto IDENTIFIER_6_DESCRIPTION_1_CREATE_DTO = SaveIdentifierDescriptionDto.builder()
            .id(null)
            .description(IDENTIFIER_6_DESCRIPTION_1_DESCRIPTION_MODIFY)
            .language(IDENTIFIER_6_DESCRIPTION_1_LANG_DTO)
            .build();

    private final static UUID IDENTIFIER_6_CREATOR_1_ID = UUID.fromString("f8a52dca-8aec-46c1-b0e1-603dbe6a1a65");

    public static final Creator IDENTIFIER_6_CREATOR_1 = Creator.builder()
            .id(IDENTIFIER_6_CREATOR_1_ID)
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeType.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_1_AFFIL_TYPE)
            .affiliationIdentifierSchemeUri(CREATOR_1_AFFIL_URI)
            .build();

    public static final CreatorDto IDENTIFIER_6_CREATOR_1_DTO = CreatorDto.builder()
            .id(IDENTIFIER_6_CREATOR_1_ID)
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_1_AFFIL_TYPE_DTO)
            .affiliationIdentifierSchemeUri(CREATOR_1_AFFIL_URI)
            .build();

    public static final SaveIdentifierCreatorDto IDENTIFIER_6_CREATOR_1_CREATE_DTO = SaveIdentifierCreatorDto.builder()
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_1_AFFIL_TYPE_DTO)
            .build();

    public static final SaveIdentifierCreatorDto IDENTIFIER_6_CREATOR_1_MODIFY_DTO = SaveIdentifierCreatorDto.builder()
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(null) /* <<<< */
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ISNI) /* <<<< */
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_1_AFFIL_TYPE_DTO)
            .build();

    private final static UUID IDENTIFIER_6_CREATOR_2_ID = UUID.fromString("eeae78cb-75a1-42e2-b608-7082e5fbecc6");

    public static final Creator IDENTIFIER_6_CREATOR_2 = Creator.builder()
            .id(IDENTIFIER_6_CREATOR_2_ID)
            .firstname(CREATOR_2_FIRSTNAME)
            .lastname(CREATOR_2_LASTNAME)
            .creatorName(CREATOR_2_NAME)
            .nameIdentifier(CREATOR_2_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeType.ORCID)
            .affiliation(CREATOR_2_AFFIL)
            .build();

    public static final CreatorDto IDENTIFIER_6_CREATOR_2_DTO = CreatorDto.builder()
            .id(IDENTIFIER_6_CREATOR_2_ID)
            .firstname(CREATOR_2_FIRSTNAME)
            .lastname(CREATOR_2_LASTNAME)
            .creatorName(CREATOR_2_NAME)
            .nameIdentifier(CREATOR_2_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_2_AFFIL)
            .build();

    private final static UUID IDENTIFIER_6_CREATOR_3_ID = UUID.fromString("700058f1-6314-4cd1-9c0c-62e75c8f422b");

    public static final Creator IDENTIFIER_6_CREATOR_3 = Creator.builder()
            .id(IDENTIFIER_6_CREATOR_3_ID)
            .firstname(CREATOR_3_FIRSTNAME)
            .lastname(CREATOR_3_LASTNAME)
            .creatorName(CREATOR_3_NAME)
            .nameIdentifier(CREATOR_3_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeType.ORCID)
            .affiliation(CREATOR_3_AFFIL)
            .affiliationIdentifier(CREATOR_3_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_3_AFFIL_SCHEME_TYPE)
            .affiliationIdentifierSchemeUri(CREATOR_3_AFFIL_URI)
            .build();

    public static final CreatorDto IDENTIFIER_6_CREATOR_3_DTO = CreatorDto.builder()
            .id(IDENTIFIER_6_CREATOR_3_ID)
            .firstname(CREATOR_3_FIRSTNAME)
            .lastname(CREATOR_3_LASTNAME)
            .creatorName(CREATOR_3_NAME)
            .nameIdentifier(CREATOR_3_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_3_AFFIL)
            .affiliationIdentifier(CREATOR_3_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_3_AFFIL_SCHEME_TYPE_DTO)
            .affiliationIdentifierSchemeUri(CREATOR_3_AFFIL_URI)
            .build();

    public static final Identifier IDENTIFIER_6 = Identifier.builder()
            .id(IDENTIFIER_6_ID)
            .queryId(QUERY_3_ID)
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_6_DESCRIPTION_1)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_6_TITLE_1)))
            .doi(IDENTIFIER_6_DOI)
            .created(IDENTIFIER_6_CREATED)
            .lastModified(IDENTIFIER_6_MODIFIED)
            .execution(IDENTIFIER_6_EXECUTION)
            .publicationDay(IDENTIFIER_6_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_6_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_6_PUBLICATION_YEAR)
            .queryHash(IDENTIFIER_6_QUERY_HASH)
            .resultHash(IDENTIFIER_6_RESULT_HASH)
            .query(IDENTIFIER_6_QUERY)
            .queryNormalized(IDENTIFIER_6_NORMALIZED)
            .resultNumber(IDENTIFIER_6_RESULT_NUMBER)
            .publisher(IDENTIFIER_6_PUBLISHER)
            .type(IDENTIFIER_6_TYPE)
            .owner(USER_3)
            .ownedBy(USER_3_ID)
            .licenses(new LinkedList<>(List.of(LICENSE_1)))
            .creators(new LinkedList<>(List.of(IDENTIFIER_6_CREATOR_1, IDENTIFIER_6_CREATOR_2, IDENTIFIER_6_CREATOR_3)))
            .status(IDENTIFIER_6_STATUS_TYPE)
            .build();

    public static final IdentifierDto IDENTIFIER_6_DTO = IdentifierDto.builder()
            .id(IDENTIFIER_6_ID)
            .databaseId(DATABASE_3_ID)
            .queryId(QUERY_3_ID)
            .links(LinksDto.builder()
                    .self("/api/identifier/" + IDENTIFIER_6_ID)
                    .selfHtml("/pid/" + IDENTIFIER_6_ID)
                    .data("/api/database/" + DATABASE_3_ID + "/subset/" + QUERY_3_ID + "/data")
                    .build())
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_6_DESCRIPTION_1_DTO)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_6_TITLE_1_DTO)))
            .doi(IDENTIFIER_6_DOI)
            .execution(IDENTIFIER_6_EXECUTION)
            .publicationDay(IDENTIFIER_6_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_6_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_6_PUBLICATION_YEAR)
            .queryHash(IDENTIFIER_6_QUERY_HASH)
            .resultHash(IDENTIFIER_6_RESULT_HASH)
            .query(IDENTIFIER_6_QUERY)
            .queryNormalized(IDENTIFIER_6_NORMALIZED)
            .resultNumber(IDENTIFIER_6_RESULT_NUMBER)
            .publisher(IDENTIFIER_6_PUBLISHER)
            .type(IDENTIFIER_6_TYPE_DTO)
            .owner(USER_3_BRIEF_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .creators(new LinkedList<>(List.of(IDENTIFIER_6_CREATOR_1_DTO, IDENTIFIER_6_CREATOR_2_DTO, IDENTIFIER_6_CREATOR_3_DTO)))
            .status(IDENTIFIER_6_STATUS_TYPE_DTO)
            .build();


    public static final IdentifierBriefDto IDENTIFIER_6_BRIEF_DTO = IdentifierBriefDto.builder()
            .id(IDENTIFIER_6_ID)
            .databaseId(DATABASE_3_ID)
            .queryId(QUERY_3_ID)
            .titles(new LinkedList<>(List.of(IDENTIFIER_6_TITLE_1_DTO)))
            .doi(IDENTIFIER_6_DOI)
            .publicationYear(IDENTIFIER_6_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_6_PUBLISHER)
            .type(IDENTIFIER_6_TYPE_DTO)
            .status(IDENTIFIER_6_STATUS_TYPE_DTO)
            .build();

    public static final CreateIdentifierDto IDENTIFIER_6_CREATE_DTO = CreateIdentifierDto.builder()
            .databaseId(DATABASE_3_ID)
            .publicationYear(IDENTIFIER_6_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_6_PUBLISHER)
            .build();

    public static final IdentifierSaveDto IDENTIFIER_6_SAVE_DTO = IdentifierSaveDto.builder()
            .id(IDENTIFIER_6_ID)
            .databaseId(DATABASE_3_ID)
            .queryId(QUERY_3_ID)
            .descriptions(new LinkedList<>(List.of(IDENTIFIER_6_DESCRIPTION_1_CREATE_DTO)))
            .titles(new LinkedList<>(List.of(IDENTIFIER_6_TITLE_1_CREATE_DTO)))
            .relatedIdentifiers(new LinkedList<>())
            .publicationMonth(IDENTIFIER_6_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_6_PUBLICATION_YEAR)
            .creators(new LinkedList<>(List.of(IDENTIFIER_6_CREATOR_1_CREATE_DTO)))
            .publisher(IDENTIFIER_6_PUBLISHER)
            .type(IDENTIFIER_6_TYPE_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .build();

    public static final UUID IDENTIFIER_7_ID = UUID.fromString("b216ae00-a31d-4ecb-95fb-37eb4da3946f");
    public static final String IDENTIFIER_7_DOI = null;
    public static final Instant IDENTIFIER_7_CREATED = Instant.ofEpochSecond(1641588352L);
    public static final Instant IDENTIFIER_7_MODIFIED = Instant.ofEpochSecond(1541588352L);
    public static final Instant IDENTIFIER_7_EXECUTION = Instant.ofEpochSecond(1541588352L);
    public static final Integer IDENTIFIER_7_PUBLICATION_DAY = 14;
    public static final Integer IDENTIFIER_7_PUBLICATION_MONTH = 7;
    public static final Integer IDENTIFIER_7_PUBLICATION_YEAR = 2022;
    public static final Long IDENTIFIER_7_RESULT_NUMBER = 2L;
    public static final String IDENTIFIER_7_PUBLISHER = "Swedish Government";
    public static final IdentifierType IDENTIFIER_7_TYPE = IdentifierType.DATABASE;
    public static final IdentifierTypeDto IDENTIFIER_7_TYPE_DTO = IdentifierTypeDto.DATABASE;
    public static final IdentifierStatusType IDENTIFIER_7_STATUS_TYPE = IdentifierStatusType.DRAFT;
    public static final IdentifierStatusTypeDto IDENTIFIER_7_STATUS_TYPE_DTO = IdentifierStatusTypeDto.DRAFT;

    public static final DataCiteBody<DataCiteDoi> IDENTIFIER_7_DATA_CITE = DataCiteBody.<DataCiteDoi>builder()
            .data(DataCiteData.<DataCiteDoi>builder()
                    .type("dois")
                    .attributes(DataCiteDoi.builder()
                            .doi(IDENTIFIER_7_DOI)
                            .build())
                    .build())
            .build();

    private final static UUID IDENTIFIER_7_CREATOR_1_ID = UUID.fromString("b899c367-06c7-4f47-8aea-5f15061ee3ee");

    public static final Creator IDENTIFIER_7_CREATOR_1 = Creator.builder()
            .id(IDENTIFIER_7_CREATOR_1_ID)
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeType.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_1_AFFIL_TYPE)
            .affiliationIdentifierSchemeUri(CREATOR_1_AFFIL_URI)
            .build();

    public static final CreatorDto IDENTIFIER_7_CREATOR_1_DTO = CreatorDto.builder()
            .id(IDENTIFIER_7_CREATOR_1_ID)
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .affiliationIdentifierScheme(CREATOR_1_AFFIL_TYPE_DTO)
            .affiliationIdentifierSchemeUri(CREATOR_1_AFFIL_URI)
            .build();

    public static final IdentifierDto IDENTIFIER_7_DTO = IdentifierDto.builder()
            .id(IDENTIFIER_7_ID)
            .databaseId(DATABASE_4_ID)
            .links(LinksDto.builder()
                    .self("/api/identifier/" + IDENTIFIER_7_ID)
                    .selfHtml("/pid/" + IDENTIFIER_7_ID)
                    .build())
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_7_DOI)
            .execution(IDENTIFIER_7_EXECUTION)
            .publicationDay(IDENTIFIER_7_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_7_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_7_PUBLICATION_YEAR)
            .resultNumber(IDENTIFIER_7_RESULT_NUMBER)
            .publisher(IDENTIFIER_7_PUBLISHER)
            .type(IDENTIFIER_7_TYPE_DTO)
            .owner(USER_4_BRIEF_DTO)
            .relatedIdentifiers(new LinkedList<>())
            .licenses(new LinkedList<>())
            .funders(new LinkedList<>())
            .creators(new LinkedList<>(List.of(IDENTIFIER_7_CREATOR_1_DTO)))
            .status(IDENTIFIER_7_STATUS_TYPE_DTO)
            .build();

    public static final SaveIdentifierCreatorDto IDENTIFIER_7_CREATOR_1_CREATE_DTO = SaveIdentifierCreatorDto.builder()
            .firstname(CREATOR_1_FIRSTNAME)
            .lastname(CREATOR_1_LASTNAME)
            .creatorName(CREATOR_1_NAME)
            .nameIdentifier(CREATOR_1_ORCID)
            .nameIdentifierScheme(NameIdentifierSchemeTypeDto.ORCID)
            .affiliation(CREATOR_1_AFFIL)
            .affiliationIdentifier(CREATOR_1_AFFIL_ROR)
            .build();

    public static final CreateIdentifierDto IDENTIFIER_7_CREATE_DTO = CreateIdentifierDto.builder()
            .databaseId(DATABASE_4_ID)
            .publicationYear(IDENTIFIER_7_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_7_PUBLISHER)
            .build();

    public static final IdentifierSaveDto IDENTIFIER_7_SAVE_DTO = IdentifierSaveDto.builder()
            .id(IDENTIFIER_7_ID)
            .databaseId(DATABASE_4_ID)
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .relatedIdentifiers(new LinkedList<>())
            .publicationMonth(IDENTIFIER_7_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_7_PUBLICATION_YEAR)
            .creators(new LinkedList<>(List.of(IDENTIFIER_7_CREATOR_1_CREATE_DTO)))
            .funders(new LinkedList<>())
            .licenses(new LinkedList<>())
            .publisher(IDENTIFIER_7_PUBLISHER)
            .type(IDENTIFIER_7_TYPE_DTO)
            .build();

    public static final UUID IDENTIFIER_2_ID = UUID.fromString("fdb95f60-48e7-4e74-8122-d3c8d079c889");
    public static final String IDENTIFIER_2_DOI = null;
    public static final Instant IDENTIFIER_2_CREATED = Instant.ofEpochSecond(1651588352L);
    public static final Instant IDENTIFIER_2_MODIFIED = Instant.ofEpochSecond(1551588352L);
    public static final Instant IDENTIFIER_2_EXECUTION = Instant.ofEpochSecond(1551588352L);
    public static final Integer IDENTIFIER_2_PUBLICATION_DAY = 10;
    public static final Integer IDENTIFIER_2_PUBLICATION_MONTH = 7;
    public static final Integer IDENTIFIER_2_PUBLICATION_YEAR = 2023;
    public static final String IDENTIFIER_2_QUERY_HASH = QUERY_1_QUERY_HASH;
    public static final String IDENTIFIER_2_RESULT_HASH = QUERY_1_RESULT_HASH;
    public static final String IDENTIFIER_2_QUERY = QUERY_1_STATEMENT;
    public static final String IDENTIFIER_2_NORMALIZED = QUERY_1_STATEMENT;
    public static final Long IDENTIFIER_2_RESULT_NUMBER = QUERY_1_RESULT_NUMBER;
    public static final String IDENTIFIER_2_PUBLISHER = "Swedish Government";
    public static final IdentifierType IDENTIFIER_2_TYPE = IdentifierType.SUBSET;
    public static final IdentifierTypeDto IDENTIFIER_2_TYPE_DTO = IdentifierTypeDto.SUBSET;
    public static final IdentifierStatusType IDENTIFIER_2_STATUS_TYPE = IdentifierStatusType.PUBLISHED;
    public static final IdentifierStatusTypeDto IDENTIFIER_2_STATUS_TYPE_DTO = IdentifierStatusTypeDto.PUBLISHED;
    public static final UUID IDENTIFIER_2_CREATED_BY = USER_1_ID;

    public static final CreateIdentifierDto IDENTIFIER_2_CREATE_DTO = CreateIdentifierDto.builder()
            .databaseId(DATABASE_1_ID)
            .queryId(QUERY_1_ID)
            .type(IDENTIFIER_2_TYPE_DTO)
            .publicationYear(IDENTIFIER_2_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_2_PUBLISHER)
            .build();

    public static final Identifier IDENTIFIER_2 = Identifier.builder()
            .id(IDENTIFIER_2_ID)
            .queryId(QUERY_1_ID)
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_2_DOI)
            .database(null /* DATABASE_1 */)
            .created(IDENTIFIER_2_CREATED)
            .lastModified(IDENTIFIER_2_MODIFIED)
            .execution(IDENTIFIER_2_EXECUTION)
            .publicationDay(IDENTIFIER_2_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_2_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_2_PUBLICATION_YEAR)
            .queryHash(IDENTIFIER_2_QUERY_HASH)
            .resultHash(IDENTIFIER_2_RESULT_HASH)
            .query(IDENTIFIER_2_QUERY)
            .queryNormalized(IDENTIFIER_2_NORMALIZED)
            .resultNumber(IDENTIFIER_2_RESULT_NUMBER)
            .publisher(IDENTIFIER_2_PUBLISHER)
            .type(IDENTIFIER_2_TYPE)
            .owner(USER_1)
            .ownedBy(USER_1_ID)
            .licenses(new LinkedList<>(List.of(LICENSE_1)))
            .creators(new LinkedList<>())
            .status(IDENTIFIER_2_STATUS_TYPE)
            .build();

    public static final IdentifierDto IDENTIFIER_2_DTO = IdentifierDto.builder()
            .id(IDENTIFIER_2_ID)
            .queryId(QUERY_1_ID)
            .databaseId(DATABASE_1_ID)
            .links(LinksDto.builder()
                    .self("/api/identifier/" + IDENTIFIER_2_ID)
                    .selfHtml("/pid/" + IDENTIFIER_2_ID)
                    .data("/api/database/" + DATABASE_1_ID + "/subset/" + QUERY_1_ID + "/data")
                    .build())
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_2_DOI)
            .execution(IDENTIFIER_2_EXECUTION)
            .publicationDay(IDENTIFIER_2_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_2_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_2_PUBLICATION_YEAR)
            .queryHash(IDENTIFIER_2_QUERY_HASH)
            .resultHash(IDENTIFIER_2_RESULT_HASH)
            .query(IDENTIFIER_2_QUERY)
            .queryNormalized(IDENTIFIER_2_NORMALIZED)
            .resultNumber(IDENTIFIER_2_RESULT_NUMBER)
            .publisher(IDENTIFIER_2_PUBLISHER)
            .type(IDENTIFIER_2_TYPE_DTO)
            .owner(USER_1_BRIEF_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .creators(new LinkedList<>())
            .status(IDENTIFIER_2_STATUS_TYPE_DTO)
            .build();

    public static final IdentifierBriefDto IDENTIFIER_2_BRIEF_DTO = IdentifierBriefDto.builder()
            .id(IDENTIFIER_2_ID)
            .queryId(QUERY_1_ID)
            .databaseId(DATABASE_1_ID)
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_2_DOI)
            .publicationYear(IDENTIFIER_2_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_2_PUBLISHER)
            .type(IDENTIFIER_2_TYPE_DTO)
            .status(IDENTIFIER_2_STATUS_TYPE_DTO)
            .build();

    public static final IdentifierSaveDto IDENTIFIER_2_SAVE_DTO = IdentifierSaveDto.builder()
            .id(IDENTIFIER_2_ID)
            .databaseId(DATABASE_1_ID)
            .queryId(QUERY_1_ID)
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .relatedIdentifiers(new LinkedList<>())
            .publicationMonth(IDENTIFIER_2_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_2_PUBLICATION_YEAR)
            .creators(new LinkedList<>())
            .publisher(IDENTIFIER_2_PUBLISHER)
            .type(IDENTIFIER_2_TYPE_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .queryId(QUERY_1_ID)
            .build();

    public static final UUID IDENTIFIER_3_ID = UUID.fromString("e2d831c2-3694-4fdc-8c48-7a7e94b73c43");
    public static final String IDENTIFIER_3_DOI = null;
    public static final Instant IDENTIFIER_3_CREATED = Instant.ofEpochSecond(1651588352L);
    public static final Instant IDENTIFIER_3_MODIFIED = Instant.ofEpochSecond(1551588352L);
    public static final Instant IDENTIFIER_3_EXECUTION = Instant.ofEpochSecond(1551588352L);
    public static final Integer IDENTIFIER_3_PUBLICATION_DAY = 10;
    public static final Integer IDENTIFIER_3_PUBLICATION_MONTH = 7;
    public static final Integer IDENTIFIER_3_PUBLICATION_YEAR = 2023;
    public static final String IDENTIFIER_3_QUERY_HASH = VIEW_1_QUERY_HASH;
    public static final String IDENTIFIER_3_RESULT_HASH = null;
    public static final String IDENTIFIER_3_QUERY = VIEW_1_QUERY;
    public static final String IDENTIFIER_3_NORMALIZED = VIEW_1_QUERY;
    public static final Long IDENTIFIER_3_RESULT_NUMBER = null;
    public static final String IDENTIFIER_3_PUBLISHER = "Polish Government";
    public static final IdentifierType IDENTIFIER_3_TYPE = IdentifierType.VIEW;
    public static final IdentifierTypeDto IDENTIFIER_3_TYPE_DTO = IdentifierTypeDto.VIEW;
    public static final IdentifierStatusType IDENTIFIER_3_STATUS_TYPE = IdentifierStatusType.PUBLISHED;
    public static final IdentifierStatusTypeDto IDENTIFIER_3_STATUS_TYPE_DTO = IdentifierStatusTypeDto.PUBLISHED;
    public static final UUID IDENTIFIER_3_CREATED_BY = USER_1_ID;

    public static final Identifier IDENTIFIER_3 = Identifier.builder()
            .id(IDENTIFIER_3_ID)
            .viewId(VIEW_1_ID)
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_3_DOI)
            .database(null /* DATABASE_1 */)
            .created(IDENTIFIER_3_CREATED)
            .lastModified(IDENTIFIER_3_MODIFIED)
            .execution(IDENTIFIER_3_EXECUTION)
            .publicationDay(IDENTIFIER_3_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_3_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_3_PUBLICATION_YEAR)
            .queryHash(IDENTIFIER_3_QUERY_HASH)
            .resultHash(IDENTIFIER_3_RESULT_HASH)
            .query(IDENTIFIER_3_QUERY)
            .queryNormalized(IDENTIFIER_3_NORMALIZED)
            .resultNumber(IDENTIFIER_3_RESULT_NUMBER)
            .publisher(IDENTIFIER_3_PUBLISHER)
            .type(IDENTIFIER_3_TYPE)
            .owner(USER_1)
            .ownedBy(USER_1_ID)
            .licenses(new LinkedList<>(List.of(LICENSE_1)))
            .creators(new LinkedList<>())
            .status(IDENTIFIER_3_STATUS_TYPE)
            .build();

    public static final IdentifierDto IDENTIFIER_3_DTO = IdentifierDto.builder()
            .id(IDENTIFIER_3_ID)
            .databaseId(DATABASE_1_ID)
            .viewId(VIEW_1_ID)
            .links(LinksDto.builder()
                    .self("/api/identifier/" + IDENTIFIER_3_ID)
                    .selfHtml("/pid/" + IDENTIFIER_3_ID)
                    .data("/api/database/" + DATABASE_1_ID + "/view/" + VIEW_1_ID + "/data")
                    .build())
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_3_DOI)
            .execution(IDENTIFIER_3_EXECUTION)
            .publicationDay(IDENTIFIER_3_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_3_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_3_PUBLICATION_YEAR)
            .queryHash(IDENTIFIER_3_QUERY_HASH)
            .resultHash(IDENTIFIER_3_RESULT_HASH)
            .query(IDENTIFIER_3_QUERY)
            .queryNormalized(IDENTIFIER_3_NORMALIZED)
            .resultNumber(IDENTIFIER_3_RESULT_NUMBER)
            .publisher(IDENTIFIER_3_PUBLISHER)
            .type(IDENTIFIER_3_TYPE_DTO)
            .owner(USER_1_BRIEF_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .creators(new LinkedList<>())
            .status(IDENTIFIER_3_STATUS_TYPE_DTO)
            .build();

    public static final IdentifierBriefDto IDENTIFIER_3_BRIEF_DTO = IdentifierBriefDto.builder()
            .id(IDENTIFIER_3_ID)
            .databaseId(DATABASE_1_ID)
            .viewId(VIEW_1_ID)
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_3_DOI)
            .publicationYear(IDENTIFIER_3_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_3_PUBLISHER)
            .type(IDENTIFIER_3_TYPE_DTO)
            .status(IDENTIFIER_3_STATUS_TYPE_DTO)
            .build();

    public static final CreateIdentifierDto IDENTIFIER_3_CREATE_DTO = CreateIdentifierDto.builder()
            .databaseId(DATABASE_1_ID)
            .viewId(VIEW_1_ID)
            .type(IDENTIFIER_3_TYPE_DTO)
            .publicationYear(IDENTIFIER_3_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_3_PUBLISHER)
            .build();

    public static final IdentifierSaveDto IDENTIFIER_3_SAVE_DTO = IdentifierSaveDto.builder()
            .id(IDENTIFIER_3_ID)
            .databaseId(DATABASE_1_ID)
            .viewId(VIEW_1_ID)
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .relatedIdentifiers(new LinkedList<>())
            .publicationMonth(IDENTIFIER_3_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_3_PUBLICATION_YEAR)
            .creators(new LinkedList<>())
            .publisher(IDENTIFIER_3_PUBLISHER)
            .type(IDENTIFIER_3_TYPE_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .build();

    public static final UUID IDENTIFIER_4_ID = UUID.fromString("3bd69bb8-f7e3-48e4-9717-823787e7ba23");
    public static final String IDENTIFIER_4_DOI = null;
    public static final Instant IDENTIFIER_4_CREATED = Instant.ofEpochSecond(1751588352L);
    public static final Instant IDENTIFIER_4_MODIFIED = Instant.ofEpochSecond(1551588352L);
    public static final Instant IDENTIFIER_4_EXECUTION = Instant.ofEpochSecond(1551588352L);
    public static final Integer IDENTIFIER_4_PUBLICATION_DAY = 10;
    public static final Integer IDENTIFIER_4_PUBLICATION_MONTH = 7;
    public static final Integer IDENTIFIER_4_PUBLICATION_YEAR = 2023;
    public static final String IDENTIFIER_4_RESULT_HASH = null;
    public static final Long IDENTIFIER_4_RESULT_NUMBER = null;
    public static final String IDENTIFIER_4_PUBLISHER = "Example Publisher";
    public static final IdentifierType IDENTIFIER_4_TYPE = IdentifierType.TABLE;
    public static final IdentifierTypeDto IDENTIFIER_4_TYPE_DTO = IdentifierTypeDto.TABLE;
    public static final IdentifierStatusType IDENTIFIER_4_STATUS_TYPE = IdentifierStatusType.PUBLISHED;
    public static final IdentifierStatusTypeDto IDENTIFIER_4_STATUS_TYPE_DTO = IdentifierStatusTypeDto.PUBLISHED;
    public static final UUID IDENTIFIER_4_CREATED_BY = USER_1_ID;

    public static final Identifier IDENTIFIER_4 = Identifier.builder()
            .id(IDENTIFIER_4_ID)
            .tableId(TABLE_1_ID)
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_4_DOI)
            .database(null /* DATABASE_1 */)
            .created(IDENTIFIER_4_CREATED)
            .lastModified(IDENTIFIER_4_MODIFIED)
            .execution(IDENTIFIER_4_EXECUTION)
            .publicationDay(IDENTIFIER_4_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_4_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_4_PUBLICATION_YEAR)
            .resultHash(IDENTIFIER_4_RESULT_HASH)
            .resultNumber(IDENTIFIER_4_RESULT_NUMBER)
            .publisher(IDENTIFIER_4_PUBLISHER)
            .type(IDENTIFIER_4_TYPE)
            .owner(USER_1)
            .ownedBy(USER_1_ID)
            .licenses(new LinkedList<>(List.of(LICENSE_1)))
            .creators(new LinkedList<>())
            .status(IDENTIFIER_4_STATUS_TYPE)
            .build();

    public static final IdentifierDto IDENTIFIER_4_DTO = IdentifierDto.builder()
            .id(IDENTIFIER_4_ID)
            .databaseId(DATABASE_1_ID)
            .tableId(TABLE_1_ID)
            .links(LinksDto.builder()
                    .self("/api/identifier/" + IDENTIFIER_4_ID)
                    .selfHtml("/pid/" + IDENTIFIER_4_ID)
                    .data("/api/database/" + DATABASE_1_ID + "/table/" + TABLE_1_ID + "/data")
                    .build())
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_4_DOI)
            .execution(IDENTIFIER_4_EXECUTION)
            .publicationDay(IDENTIFIER_4_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_4_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_4_PUBLICATION_YEAR)
            .resultHash(IDENTIFIER_4_RESULT_HASH)
            .resultNumber(IDENTIFIER_4_RESULT_NUMBER)
            .publisher(IDENTIFIER_4_PUBLISHER)
            .type(IDENTIFIER_4_TYPE_DTO)
            .owner(USER_1_BRIEF_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .creators(new LinkedList<>())
            .status(IDENTIFIER_4_STATUS_TYPE_DTO)
            .build();

    public static final IdentifierBriefDto IDENTIFIER_4_BRIEF_DTO = IdentifierBriefDto.builder()
            .id(IDENTIFIER_4_ID)
            .databaseId(DATABASE_1_ID)
            .tableId(TABLE_1_ID)
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_4_DOI)
            .publicationYear(IDENTIFIER_4_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_4_PUBLISHER)
            .type(IDENTIFIER_4_TYPE_DTO)
            .status(IDENTIFIER_4_STATUS_TYPE_DTO)
            .build();

    public static final CreateIdentifierDto IDENTIFIER_4_CREATE_DTO = CreateIdentifierDto.builder()
            .databaseId(DATABASE_1_ID)
            .publicationYear(IDENTIFIER_4_PUBLICATION_YEAR)
            .publisher(IDENTIFIER_4_PUBLISHER)
            .build();

    public static final IdentifierSaveDto IDENTIFIER_4_SAVE_DTO = IdentifierSaveDto.builder()
            .id(IDENTIFIER_4_ID)
            .databaseId(DATABASE_1_ID)
            .tableId(TABLE_1_ID)
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .relatedIdentifiers(new LinkedList<>())
            .publicationMonth(IDENTIFIER_4_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_4_PUBLICATION_YEAR)
            .creators(new LinkedList<>())
            .publisher(IDENTIFIER_4_PUBLISHER)
            .type(IDENTIFIER_4_TYPE_DTO)
            .licenses(new LinkedList<>(List.of(LICENSE_1_DTO)))
            .build();

    public static final String VIRTUAL_HOST_NAME = "fda";
    public static final String VIRTUAL_HOST_DESCRIPTION = "FAIR Data Austria";
    public static final String VIRTUAL_HOST_TAGS = "";

    public static final CreateVirtualHostDto VIRTUAL_HOST_CREATE_DTO = CreateVirtualHostDto.builder()
            .name(VIRTUAL_HOST_NAME)
            .description(VIRTUAL_HOST_DESCRIPTION)
            .tags(VIRTUAL_HOST_TAGS)
            .build();

    public static final ExchangeUpdatePermissionsDto VIRTUAL_HOST_EXCHANGE_UPDATE_DTO = ExchangeUpdatePermissionsDto.builder()
            .exchange(DATABASE_1_EXCHANGE)
            .read(".*")
            .write(".*")
            .build();

    public static final GrantVirtualHostPermissionsDto VIRTUAL_HOST_GRANT_DTO = GrantVirtualHostPermissionsDto.builder()
            .read(".*")
            .write(".*")
            .configure(".*")
            .build();

    public static final UUID BANNER_MESSAGE_1_ID = UUID.fromString("81cf09b7-0d86-44ad-be8e-a407e7d114e1");
    public static final String BANNER_MESSAGE_1_MESSAGE = "Next maintenance in 7 days!";
    public static final BannerMessageType BANNER_MESSAGE_1_TYPE = BannerMessageType.INFO;
    public static final BannerMessageTypeDto BANNER_MESSAGE_1_TYPE_DTO = BannerMessageTypeDto.INFO;
    public static final Instant BANNER_MESSAGE_1_START = Instant.ofEpochSecond(1684577786L) /* 2022-12-23 22:00:00 (UTC) */;
    public static final Instant BANNER_MESSAGE_1_END = null;

    public static final BannerMessage BANNER_MESSAGE_1 = BannerMessage.builder()
            .id(BANNER_MESSAGE_1_ID)
            .message(BANNER_MESSAGE_1_MESSAGE)
            .type(BANNER_MESSAGE_1_TYPE)
            .displayStart(BANNER_MESSAGE_1_START)
            .displayEnd(BANNER_MESSAGE_1_END)
            .build();

    public static final BannerMessageDto BANNER_MESSAGE_1_DTO = BannerMessageDto.builder()
            .id(BANNER_MESSAGE_1_ID)
            .message(BANNER_MESSAGE_1_MESSAGE)
            .type(BANNER_MESSAGE_1_TYPE_DTO)
            .displayStart(BANNER_MESSAGE_1_START)
            .displayEnd(BANNER_MESSAGE_1_END)
            .build();

    public static final BannerMessageCreateDto BANNER_MESSAGE_1_CREATE_DTO = BannerMessageCreateDto.builder()
            .message(BANNER_MESSAGE_1_MESSAGE)
            .type(BANNER_MESSAGE_1_TYPE_DTO)
            .displayStart(BANNER_MESSAGE_1_START)
            .displayEnd(BANNER_MESSAGE_1_END)
            .build();

    public static final BannerMessageUpdateDto BANNER_MESSAGE_1_UPDATE_DTO = BannerMessageUpdateDto.builder()
            .message(BANNER_MESSAGE_1_MESSAGE)
            .type(BannerMessageTypeDto.WARNING)
            .displayStart(BANNER_MESSAGE_1_START)
            .displayEnd(BANNER_MESSAGE_1_END)
            .build();

    public static final UUID BANNER_MESSAGE_2_ID = UUID.fromString("1e7e2c03-e2c6-46b8-9fdc-6668ef055d99");
    public static final String BANNER_MESSAGE_2_MESSAGE = "No operation on Christmas 2022!";
    public static final BannerMessageType BANNER_MESSAGE_2_TYPE = BannerMessageType.ERROR;
    public static final BannerMessageTypeDto BANNER_MESSAGE_2_TYPE_DTO = BannerMessageTypeDto.ERROR;
    public static final Instant BANNER_MESSAGE_2_START = Instant.ofEpochSecond(1671836400L) /* 2022-12-23 22:00:00 (UTC) */;
    public static final Instant BANNER_MESSAGE_2_END = Instant.ofEpochSecond(1672009200L) /* 2022-12-25 22:00:00 (UTC) */;

    public static final BannerMessage BANNER_MESSAGE_2 = BannerMessage.builder()
            .id(BANNER_MESSAGE_2_ID)
            .message(BANNER_MESSAGE_2_MESSAGE)
            .type(BANNER_MESSAGE_2_TYPE)
            .displayStart(BANNER_MESSAGE_2_START)
            .displayEnd(BANNER_MESSAGE_2_END)
            .build();

    public static final BannerMessageCreateDto BANNER_MESSAGE_2_CREATE_DTO = BannerMessageCreateDto.builder()
            .message(BANNER_MESSAGE_2_MESSAGE)
            .type(BANNER_MESSAGE_2_TYPE_DTO)
            .displayStart(BANNER_MESSAGE_2_START)
            .displayEnd(BANNER_MESSAGE_2_END)
            .build();

    public static final Database DATABASE_1 = Database.builder()
            .id(DATABASE_1_ID)
            .created(Instant.now().minus(1, HOURS))
            .lastModified(Instant.now())
            .isPublic(DATABASE_1_PUBLIC)
            .isSchemaPublic(DATABASE_1_SCHEMA_PUBLIC)
            .name(DATABASE_1_NAME)
            .description(DATABASE_1_DESCRIPTION)
            .identifiers(new LinkedList<>(List.of(IDENTIFIER_1, IDENTIFIER_2, IDENTIFIER_3, IDENTIFIER_4)))
            .cid(CONTAINER_1_ID)
            .container(CONTAINER_1)
            .internalName(DATABASE_1_INTERNALNAME)
            .exchangeName(DATABASE_1_EXCHANGE)
            .created(DATABASE_1_CREATED)
            .lastModified(DATABASE_1_LAST_MODIFIED)
            .ownedBy(DATABASE_1_CREATED_BY)
            .owner(USER_1)
            .ownedBy(USER_1_ID)
            .owner(USER_1)
            .image(new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
            .contactPerson(USER_1_ID)
            .contact(USER_1)
            .subsets(new LinkedList<>())
            .tables(new LinkedList<>())
            .views(new LinkedList<>())
            .accesses(new LinkedList<>())
            .identifiers(new LinkedList<>())
            .build();

    public static final DatabaseDto DATABASE_1_DTO = DatabaseDto.builder()
            .id(DATABASE_1_ID)
            .isPublic(DATABASE_1_PUBLIC)
            .isSchemaPublic(DATABASE_1_SCHEMA_PUBLIC)
            .name(DATABASE_1_NAME)
            .container(CONTAINER_1_DTO)
            .internalName(DATABASE_1_INTERNALNAME)
            .exchangeName(DATABASE_1_EXCHANGE)
            .identifiers(new LinkedList<>(List.of(IDENTIFIER_1_DTO, IDENTIFIER_2_DTO, IDENTIFIER_3_DTO, IDENTIFIER_4_DTO)))
            .tables(new LinkedList<>(List.of(TABLE_1_DTO, TABLE_2_DTO, TABLE_3_DTO, TABLE_4_DTO)))
            .views(new LinkedList<>(List.of(VIEW_1_DTO, VIEW_2_DTO, VIEW_3_DTO)))
            .owner(USER_1_BRIEF_DTO)
            .build();

    public static final DatabaseDto DATABASE_1_PRIVILEGED_DTO = DatabaseDto.builder()
            .id(DATABASE_1_ID)
            .isPublic(DATABASE_1_PUBLIC)
            .isSchemaPublic(DATABASE_1_SCHEMA_PUBLIC)
            .name(DATABASE_1_NAME)
            .container(CONTAINER_1_PRIVILEGED_DTO)
            .internalName(DATABASE_1_INTERNALNAME)
            .exchangeName(DATABASE_1_EXCHANGE)
            .accesses(new LinkedList<>(List.of())) /* DATABASE_1_USER_1_READ_ACCESS_DTO */
            .identifiers(new LinkedList<>(List.of(IDENTIFIER_1_DTO, IDENTIFIER_2_DTO, IDENTIFIER_3_DTO, IDENTIFIER_4_DTO)))
            .tables(new LinkedList<>(List.of(TABLE_1_DTO, TABLE_2_DTO, TABLE_3_DTO, TABLE_4_DTO)))
            .views(new LinkedList<>(List.of(VIEW_1_DTO, VIEW_2_DTO, VIEW_3_DTO)))
            .owner(USER_1_BRIEF_DTO)
            .lastRetrieved(Instant.now())
            .build();

    public static final DatabaseBriefDto DATABASE_1_BRIEF_DTO = DatabaseBriefDto.builder()
            .id(DATABASE_1_ID)
            .isPublic(DATABASE_1_PUBLIC)
            .isSchemaPublic(DATABASE_1_SCHEMA_PUBLIC)
            .name(DATABASE_1_NAME)
            .internalName(DATABASE_1_INTERNALNAME)
            .identifiers(new LinkedList<>(List.of(IDENTIFIER_1_BRIEF_DTO, IDENTIFIER_2_BRIEF_DTO, IDENTIFIER_3_BRIEF_DTO, IDENTIFIER_4_BRIEF_DTO)))
            .build();

    public static final DatabaseAccess DATABASE_1_USER_1_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccessDto DATABASE_1_USER_1_READ_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.READ)
            .hdbid(DATABASE_1_ID)
            .huserid(USER_1_ID)
            .user(USER_1_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_1_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_1_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_2_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccessDto DATABASE_1_USER_2_READ_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.READ)
            .hdbid(DATABASE_1_ID)
            .huserid(USER_2_ID)
            .user(USER_2_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_2_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccessDto DATABASE_1_USER_2_WRITE_OWN_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.WRITE_OWN)
            .hdbid(DATABASE_1_ID)
            .huserid(USER_2_ID)
            .user(USER_2_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_2_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccessDto DATABASE_1_USER_2_WRITE_ALL_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.WRITE_ALL)
            .hdbid(DATABASE_1_ID)
            .huserid(USER_2_ID)
            .user(USER_2_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_3_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_3_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_3_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final DatabaseAccessDto DATABASE_1_USER_3_WRITE_ALL_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.WRITE_ALL)
            .hdbid(DATABASE_1_ID)
            .huserid(USER_3_ID)
            .user(USER_3_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_1_USER_4_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_1_ID)
            .database(DATABASE_1)
            .huserid(USER_4_ID)
            .user(USER_4)
            .build();

    public static final DatabaseAccessDto DATABASE_1_USER_4_READ_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.READ)
            .hdbid(DATABASE_1_ID)
            .huserid(USER_4_ID)
            .user(USER_4_BRIEF_DTO)
            .build();

    public static final Database DATABASE_2 = Database.builder()
            .id(DATABASE_2_ID)
            .created(DATABASE_2_CREATED)
            .lastModified(Instant.now())
            .isPublic(DATABASE_2_PUBLIC)
            .isSchemaPublic(DATABASE_2_SCHEMA_PUBLIC)
            .name(DATABASE_2_NAME)
            .description(DATABASE_2_DESCRIPTION)
            .cid(CONTAINER_1_ID)
            .container(CONTAINER_1)
            .internalName(DATABASE_2_INTERNALNAME)
            .exchangeName(DATABASE_2_EXCHANGE)
            .created(DATABASE_2_CREATED)
            .lastModified(DATABASE_2_LAST_MODIFIED)
            .ownedBy(DATABASE_2_OWNER)
            .owner(USER_2)
            .contactPerson(USER_2_ID)
            .contact(USER_2)
            .tables(new LinkedList<>())
            .views(new LinkedList<>())
            .accesses(new LinkedList<>())
            .identifiers(new LinkedList<>())
            .build();

    public static final DatabaseDto DATABASE_2_DTO = DatabaseDto.builder()
            .id(DATABASE_2_ID)
            .isPublic(DATABASE_2_PUBLIC)
            .isSchemaPublic(DATABASE_2_SCHEMA_PUBLIC)
            .name(DATABASE_2_NAME)
            .container(CONTAINER_1_DTO)
            .internalName(DATABASE_2_INTERNALNAME)
            .exchangeName(DATABASE_2_EXCHANGE)
            .identifiers(new LinkedList<>(List.of(IDENTIFIER_5_DTO)))
            .tables(new LinkedList<>(List.of(TABLE_5_DTO, TABLE_6_DTO, TABLE_7_DTO)))
            .views(new LinkedList<>(List.of(VIEW_4_DTO)))
            .owner(USER_2_BRIEF_DTO)
            .lastRetrieved(Instant.now())
            .build();

    public static final DatabaseDto DATABASE_2_PRIVILEGED_DTO = DatabaseDto.builder()
            .id(DATABASE_2_ID)
            .isPublic(DATABASE_2_PUBLIC)
            .isSchemaPublic(DATABASE_2_SCHEMA_PUBLIC)
            .name(DATABASE_2_NAME)
            .container(CONTAINER_1_PRIVILEGED_DTO)
            .internalName(DATABASE_2_INTERNALNAME)
            .exchangeName(DATABASE_2_EXCHANGE)
            .identifiers(new LinkedList<>(List.of(IDENTIFIER_5_DTO)))
            .tables(new LinkedList<>(List.of(TABLE_5_DTO, TABLE_6_DTO, TABLE_7_DTO)))
            .views(new LinkedList<>(List.of(VIEW_4_DTO)))
            .owner(USER_2_BRIEF_DTO)
            .lastRetrieved(Instant.now())
            .build();

    public static final DatabaseBriefDto DATABASE_2_PRIVILEGED_BRIEF_DTO = DatabaseBriefDto.builder()
            .id(DATABASE_2_ID)
            .isPublic(DATABASE_2_PUBLIC)
            .isSchemaPublic(DATABASE_2_SCHEMA_PUBLIC)
            .name(DATABASE_2_NAME)
            .internalName(DATABASE_2_INTERNALNAME)
            .identifiers(new LinkedList<>(List.of(IDENTIFIER_5_BRIEF_DTO)))
            .ownerId(USER_2_ID)
            .build();

    public static final DatabaseBriefDto DATABASE_2_BRIEF_DTO = DatabaseBriefDto.builder()
            .id(DATABASE_2_ID)
            .isPublic(DATABASE_2_PUBLIC)
            .isSchemaPublic(DATABASE_2_SCHEMA_PUBLIC)
            .name(DATABASE_2_NAME)
            .internalName(DATABASE_2_INTERNALNAME)
            .identifiers(new LinkedList<>(List.of(IDENTIFIER_5_BRIEF_DTO)))
            .ownerId(USER_2_ID)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_1_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_1_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_1_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_2_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccessDto DATABASE_2_USER_2_READ_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.READ)
            .hdbid(DATABASE_2_ID)
            .huserid(USER_2_ID)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_2_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_2_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccessDto DATABASE_2_USER_2_WRITE_ALL_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.WRITE_ALL)
            .hdbid(DATABASE_2_ID)
            .huserid(USER_2_ID)
            .user(USER_2_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_3_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final DatabaseAccessDto DATABASE_2_USER_3_READ_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.READ)
            .hdbid(DATABASE_2_ID)
            .huserid(USER_3_ID)
            .user(USER_3_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_3_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final DatabaseAccess DATABASE_2_USER_3_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_2_ID)
            .database(DATABASE_2)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final Database DATABASE_3 = Database.builder()
            .id(DATABASE_3_ID)
            .created(Instant.now().minus(1, HOURS))
            .lastModified(Instant.now())
            .isPublic(DATABASE_3_PUBLIC)
            .isSchemaPublic(DATABASE_3_SCHEMA_PUBLIC)
            .name(DATABASE_3_NAME)
            .description(DATABASE_3_DESCRIPTION)
            .cid(CONTAINER_1_ID)
            .container(CONTAINER_1)
            .internalName(DATABASE_3_INTERNALNAME)
            .exchangeName(DATABASE_3_EXCHANGE)
            .created(DATABASE_3_CREATED)
            .lastModified(DATABASE_3_LAST_MODIFIED)
            .ownedBy(DATABASE_3_OWNER)
            .owner(USER_3)
            .contactPerson(USER_3_ID)
            .contact(USER_3)
            .tables(new LinkedList<>())
            .views(new LinkedList<>())
            .accesses(new LinkedList<>()) /* DATABASE_3_USER_1_WRITE_ALL_ACCESS */
            .identifiers(new LinkedList<>()) /* IDENTIFIER_6 */
            .build();

    public static final DatabaseAccess DATABASE_3_USER_1_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccessDto DATABASE_3_USER_1_READ_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.READ)
            .hdbid(DATABASE_3_ID)
            .huserid(USER_1_ID)
            .user(USER_1_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_3_USER_1_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccessDto DATABASE_3_USER_1_WRITE_OWN_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.WRITE_OWN)
            .hdbid(DATABASE_3_ID)
            .huserid(USER_1_ID)
            .user(USER_1_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_3_USER_1_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_1_ID)
            .user(USER_1)
            .build();

    public static final DatabaseAccessDto DATABASE_3_USER_1_WRITE_ALL_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.WRITE_ALL)
            .hdbid(DATABASE_3_ID)
            .huserid(USER_1_ID)
            .user(USER_1_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_3_USER_2_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccess DATABASE_3_USER_2_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccess DATABASE_3_USER_2_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_2_ID)
            .user(USER_2)
            .build();

    public static final DatabaseAccess DATABASE_3_USER_3_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final DatabaseAccessDto DATABASE_3_USER_3_READ_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.READ)
            .hdbid(DATABASE_3_ID)
            .huserid(USER_3_ID)
            .user(USER_3_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_3_USER_3_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final DatabaseAccessDto DATABASE_3_USER_3_WRITE_OWN_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.WRITE_OWN)
            .hdbid(DATABASE_3_ID)
            .huserid(USER_3_ID)
            .user(USER_3_BRIEF_DTO)
            .build();

    public static final DatabaseAccess DATABASE_3_USER_3_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_3_ID)
            .database(DATABASE_3)
            .huserid(USER_3_ID)
            .user(USER_3)
            .build();

    public static final DatabaseAccessDto DATABASE_3_USER_3_WRITE_ALL_ACCESS_DTO = DatabaseAccessDto.builder()
            .type(AccessTypeDto.WRITE_ALL)
            .hdbid(DATABASE_3_ID)
            .huserid(USER_3_ID)
            .user(USER_3_BRIEF_DTO)
            .build();

    public static final Identifier IDENTIFIER_7 = Identifier.builder()
            .id(IDENTIFIER_7_ID)
            .descriptions(new LinkedList<>())
            .titles(new LinkedList<>())
            .doi(IDENTIFIER_7_DOI)
            .created(IDENTIFIER_7_CREATED)
            .lastModified(IDENTIFIER_7_MODIFIED)
            .execution(IDENTIFIER_7_EXECUTION)
            .publicationDay(IDENTIFIER_7_PUBLICATION_DAY)
            .publicationMonth(IDENTIFIER_7_PUBLICATION_MONTH)
            .publicationYear(IDENTIFIER_7_PUBLICATION_YEAR)
            .resultNumber(IDENTIFIER_7_RESULT_NUMBER)
            .publisher(IDENTIFIER_7_PUBLISHER)
            .type(IDENTIFIER_7_TYPE)
            .owner(USER_4)
            .ownedBy(USER_4_ID)
            .licenses(new LinkedList<>())
            .creators(new LinkedList<>(List.of(IDENTIFIER_7_CREATOR_1)))
            .relatedIdentifiers(new LinkedList<>())
            .funders(new LinkedList<>())
            .status(IDENTIFIER_7_STATUS_TYPE)
            .build();

    public static final Database DATABASE_4 = Database.builder()
            .id(DATABASE_4_ID)
            .created(Instant.now().minus(4, HOURS))
            .lastModified(Instant.now())
            .isPublic(DATABASE_4_PUBLIC)
            .isSchemaPublic(DATABASE_4_SCHEMA_PUBLIC)
            .name(DATABASE_4_NAME)
            .description(DATABASE_4_DESCRIPTION)
            .cid(CONTAINER_4_ID)
            .container(CONTAINER_4)
            .internalName(DATABASE_4_INTERNALNAME)
            .exchangeName(DATABASE_4_EXCHANGE)
            .created(DATABASE_4_CREATED)
            .lastModified(DATABASE_4_LAST_MODIFIED)
            .ownedBy(DATABASE_4_OWNER)
            .owner(USER_4)
            .contactPerson(USER_4_ID)
            .contact(USER_4)
            .tables(new LinkedList<>())
            .views(new LinkedList<>())
            .identifiers(new LinkedList<>())
            .build();

    public static final DatabaseAccess DATABASE_4_USER_1_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_1_ID)
            .build();

    public static final DatabaseAccess DATABASE_4_USER_1_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_1_ID)
            .build();

    public static final DatabaseAccess DATABASE_4_USER_1_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_1_ID)
            .build();

    public static final DatabaseAccess DATABASE_4_USER_2_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_2_ID)
            .build();

    public static final DatabaseAccess DATABASE_4_USER_2_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_2_ID)
            .build();

    public static final DatabaseAccess DATABASE_4_USER_2_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_2_ID)
            .build();

    public static final DatabaseAccess DATABASE_4_USER_3_READ_ACCESS = DatabaseAccess.builder()
            .type(AccessType.READ)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_3_ID)
            .build();

    public static final DatabaseAccess DATABASE_4_USER_3_WRITE_OWN_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_OWN)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_3_ID)
            .build();

    public static final DatabaseAccess DATABASE_4_USER_3_WRITE_ALL_ACCESS = DatabaseAccess.builder()
            .type(AccessType.WRITE_ALL)
            .hdbid(DATABASE_4_ID)
            .database(DATABASE_4)
            .huserid(USER_3_ID)
            .build();

    public static final List<IdentifierDto> VIEW_1_DTO_IDENTIFIERS = List.of(IDENTIFIER_3_DTO);

    public static final Constraints TABLE_1_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_1)
                    .column(TABLE_1_COLUMNS.get(0))
                    .id(COLUMN_1_1_ID)
                    .build())))
            .build();

    public static final ConstraintsDto TABLE_1_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .id(UUID.fromString("b3f40a88-4f21-4de0-a595-3d15e63943aa"))
                    .table(TABLE_1_BRIEF_DTO)
                    .column(TABLE_1_COLUMNS_BRIEF_0_DTO)
                    .build())))
            .build();

    public static final Constraints TABLE_2_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>(List.of("`mintemp` > 0")))
            .foreignKeys(new LinkedList<>(List.of(ForeignKey.builder()
                    .id(UUID.fromString("d79f0fb1-05d6-4f3e-a5e2-8559982b8516"))
                    .name("fk_location")
                    .onDelete(ReferenceType.NO_ACTION)
                    .references(new LinkedList<>(List.of(ForeignKeyReference.builder()
                            .id(UUID.fromString("a4da8f2f-2999-4621-8066-801a2fb73c8d"))
                            .column(TABLE_2_COLUMNS.get(2))
                            .referencedColumn(TABLE_1_COLUMNS.get(0))
                            .foreignKey(null) // set later
                            .build())))
                    .table(TABLE_2)
                    .referencedTable(TABLE_1)
                    .onUpdate(ReferenceType.NO_ACTION)
                    .build())))
            .uniques(new LinkedList<>(List.of(Unique.builder()
                    .id(UUID.fromString("408e398f-d157-49a1-8b45-87a070f3b4de"))
                    .table(TABLE_2)
                    .name("uk_1")
                    .columns(new LinkedList<>(List.of(TABLE_2_COLUMNS.get(1))))
                    .build())))
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_2)
                    .column(TABLE_2_COLUMNS.get(0))
                    .id(COLUMN_2_1_ID)
                    .build())))
            .build();

    public static final ConstraintsDto TABLE_2_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>(List.of("`mintemp` > 0")))
            .foreignKeys(new LinkedList<>(List.of(ForeignKeyDto.builder()
                    .id(UUID.fromString("ca833111-1e9a-48a3-bb16-ad6f90196f96"))
                    .name("fk_location")
                    .onDelete(ReferenceTypeDto.NO_ACTION)
                    .references(new LinkedList<>(List.of(ForeignKeyReferenceDto.builder()
                            .id(UUID.fromString("8552f282-0403-424d-b2ba-4ed0f760197c"))
                            .column(TABLE_2_COLUMNS_BRIEF_2_DTO)
                            .referencedColumn(TABLE_1_COLUMNS_BRIEF_0_DTO)
                            .foreignKey(null) // set later
                            .build())))
                    .table(TABLE_1_BRIEF_DTO)
                    .referencedTable(TABLE_2_BRIEF_DTO)
                    .onUpdate(ReferenceTypeDto.NO_ACTION)
                    .build())))
            .uniques(new LinkedList<>(List.of(UniqueDto.builder()
                    .id(UUID.fromString("b9aba807-dd9c-43a3-9614-2493cb4b26bd"))
                    .table(TABLE_2_BRIEF_DTO)
                    .name("uk_1")
                    .columns(new LinkedList<>(List.of(TABLE_2_COLUMNS_BRIEF_DTO.get(1))))
                    .build())))
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .table(TABLE_2_BRIEF_DTO)
                    .column(TABLE_2_COLUMNS_BRIEF_0_DTO)
                    .id(COLUMN_2_1_ID)
                    .build())))
            .build();

    public static final Constraints TABLE_3_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_3)
                    .column(TABLE_3_COLUMNS.get(0))
                    .id(COLUMN_3_1_ID)
                    .build())))
            .build();

    public static final ConstraintsDto TABLE_3_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .table(TABLE_3_BRIEF_DTO)
                    .column(TABLE_3_COLUMNS_BRIEF_0_DTO)
                    .id(COLUMN_3_1_ID)
                    .build())))
            .build();

    public static final Constraints TABLE_4_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_4)
                    .column(TABLE_4_COLUMNS.get(0))
                    .id(COLUMN_4_1_ID)
                    .build())))
            .build();

    public static final ConstraintsDto TABLE_4_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .table(TABLE_4_BRIEF_DTO)
                    .column(TABLE_4_COLUMNS_BRIEF_0_DTO)
                    .id(COLUMN_4_1_ID)
                    .build())))
            .build();

    public static final Constraints TABLE_5_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_5)
                    .column(TABLE_5_COLUMNS.get(0))
                    .id(COLUMN_5_1_ID)
                    .build())))
            .build();

    public static final ConstraintsDto TABLE_5_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .table(TABLE_5_BRIEF_DTO)
                    .column(TABLE_5_COLUMNS_BRIEF_0_DTO)
                    .id(COLUMN_5_1_ID)
                    .build())))
            .build();

    public static final Constraints TABLE_6_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>(List.of()))
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_6)
                    .column(TABLE_6_COLUMNS.get(0))
                    .id(COLUMN_6_1_ID)
                    .build())))
            .build();

    public static final ConstraintsDto TABLE_6_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .table(TABLE_6_BRIEF_DTO)
                    .column(TABLE_6_COLUMNS_BRIEF_0_DTO)
                    .id(COLUMN_6_1_ID)
                    .build())))
            .build();

    public static final Constraints TABLE_7_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>(List.of(ForeignKey.builder()
                            .id(UUID.fromString("421c3dd8-ae09-4c72-a6ca-09de009e755f"))
                            .name("fk_name_id")
                            .onDelete(ReferenceType.NO_ACTION)
                            .references(new LinkedList<>(List.of(ForeignKeyReference.builder()
                                    .id(UUID.fromString("7c0e4a3c-88b8-4276-8924-403fd122fbf1"))
                                    .column(TABLE_6_COLUMNS.get(0))
                                    .referencedColumn(TABLE_7_COLUMNS.get(0))
                                    .foreignKey(null) // set later
                                    .build())))
                            .table(TABLE_7)
                            .referencedTable(TABLE_6)
                            .onUpdate(ReferenceType.NO_ACTION)
                            .build(),
                    ForeignKey.builder()
                            .id(UUID.fromString("fce75207-6009-49ff-a646-d3e18aed787a"))
                            .name("fk_zoo_id")
                            .onDelete(ReferenceType.NO_ACTION)
                            .references(new LinkedList<>(List.of(ForeignKeyReference.builder()
                                    .id(UUID.fromString("e6cb1daa-a210-41c4-bb79-2c98ef25a02c"))
                                    .column(TABLE_5_COLUMNS.get(0))
                                    .referencedColumn(TABLE_7_COLUMNS.get(1))
                                    .foreignKey(null) // set later
                                    .build())))
                            .table(TABLE_7)
                            .referencedTable(TABLE_5)
                            .onUpdate(ReferenceType.NO_ACTION)
                            .build())))
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_7)
                    .column(TABLE_7_COLUMNS.get(0))
                    .id(COLUMN_7_1_ID)
                    .build())))
            .build();

    public static final ForeignKeyDto TABLE_7_CONSTRAINTS_FOREIGN_KEY_0_DTO = ForeignKeyDto.builder()
            .id(UUID.fromString("561b4933-54e5-4dad-a536-39836da87fe3"))
            .name("fk_name_id")
            .onDelete(ReferenceTypeDto.NO_ACTION)
            .references(new LinkedList<>(List.of(ForeignKeyReferenceDto.builder()
                    .id(UUID.fromString("0f4b00c0-f2a8-4929-8619-bdc941b5dc8c"))
                    .column(TABLE_6_COLUMNS_BRIEF_0_DTO)
                    .referencedColumn(TABLE_7_COLUMNS_BRIEF_0_DTO)
                    .foreignKey(null) // set later
                    .build())))
            .table(TABLE_7_BRIEF_DTO)
            .referencedTable(TABLE_6_BRIEF_DTO)
            .onUpdate(ReferenceTypeDto.NO_ACTION)
            .build();

    public static final ForeignKeyBriefDto TABLE_7_CONSTRAINTS_FOREIGN_KEY_BRIEF_0_DTO = ForeignKeyBriefDto.builder()
            .id(UUID.fromString("a92f09c5-9bce-4f77-8f7b-a9afc1d30ec2"))
            .build();

    public static final ForeignKeyDto TABLE_7_CONSTRAINTS_FOREIGN_KEY_1_DTO = ForeignKeyDto.builder()
            .id(UUID.fromString("f2e82566-ddc3-4b76-8d27-adc3c51780a9"))
            .name("fk_zoo_id")
            .onDelete(ReferenceTypeDto.NO_ACTION)
            .references(new LinkedList<>(List.of(ForeignKeyReferenceDto.builder()
                    .id(UUID.fromString("7a393166-25d2-4b8c-a5e7-7d1b3b33b823"))
                    .column(TABLE_5_COLUMNS_BRIEF_0_DTO)
                    .referencedColumn(TABLE_7_COLUMNS_BRIEF_1_DTO)
                    .foreignKey(null) // set later
                    .build())))
            .table(TABLE_7_BRIEF_DTO)
            .referencedTable(TABLE_5_BRIEF_DTO)
            .onUpdate(ReferenceTypeDto.NO_ACTION)
            .build();

    public static final ForeignKeyBriefDto TABLE_7_CONSTRAINTS_FOREIGN_KEY_BRIEF_1_DTO = ForeignKeyBriefDto.builder()
            .id(UUID.fromString("6ce1f707-0bdf-4930-be77-157801d2735a"))
            .build();

    public static final ConstraintsDto TABLE_7_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>(List.of(TABLE_7_CONSTRAINTS_FOREIGN_KEY_0_DTO,
                    TABLE_7_CONSTRAINTS_FOREIGN_KEY_1_DTO)))
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .table(TABLE_7_BRIEF_DTO)
                    .column(TABLE_7_COLUMNS_BRIEF_0_DTO)
                    .id(UUID.fromString("9969e13f-2a2f-45c7-bccf-a7df0ac813a8"))
                    .build())))
            .build();

    public static final Constraints TABLE_8_CONSTRAINTS = Constraints.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedList<>(List.of(PrimaryKey.builder()
                    .table(TABLE_8)
                    .column(TABLE_8_COLUMNS.get(0))
                    .id(UUID.fromString("cd23b601-966c-4aa7-9722-6bcb009200cc"))
                    .build())))
            .build();

    public static final ConstraintsDto TABLE_8_CONSTRAINTS_DTO = ConstraintsDto.builder()
            .checks(new LinkedHashSet<>())
            .foreignKeys(new LinkedList<>())
            .uniques(new LinkedList<>())
            .primaryKey(new LinkedHashSet<>(Set.of(PrimaryKeyDto.builder()
                    .table(TABLE_8_BRIEF_DTO)
                    .column(TABLE_8_COLUMNS_BRIEF_0_DTO)
                    .id(UUID.fromString("c61196d1-a902-405c-a825-0781c0c94df1"))
                    .build())))
            .build();

    public static final ExportResourceDto EXPORT_RESOURCE_DTO = ExportResourceDto.builder()
            .filename("68b329da9893e34099c7d8ad5cb9c940")
            .resource(new InputStreamResource(InputStream.nullInputStream()))
            .build();

    public static final QueryDto QUERY_1_DTO = QueryDto.builder()
            .id(QUERY_1_ID)
            .databaseId(DATABASE_1_ID)
            .query(QUERY_1_STATEMENT)
            .queryHash(QUERY_1_QUERY_HASH)
            .resultHash(QUERY_1_RESULT_HASH)
            .execution(QUERY_1_EXECUTION)
            .owner(USER_1_BRIEF_DTO)
            .isPersisted(QUERY_1_PERSISTED)
            .resultNumber(3L)
            .build();

    public static final QueryDto QUERY_2_DTO = QueryDto.builder()
            .id(QUERY_2_ID)
            .databaseId(DATABASE_1_ID)
            .query(QUERY_2_STATEMENT)
            .queryNormalized(QUERY_2_STATEMENT)
            .resultNumber(QUERY_2_RESULT_NUMBER)
            .resultHash(QUERY_2_RESULT_HASH)
            .owner(USER_1_BRIEF_DTO)
            .queryHash(QUERY_2_QUERY_HASH)
            .execution(QUERY_2_EXECUTION)
            .isPersisted(QUERY_2_PERSISTED)
            .resultNumber(3L)
            .build();

    public static final QueryDto QUERY_3_DTO = QueryDto.builder()
            .id(QUERY_3_ID)
            .databaseId(DATABASE_1_ID)
            .query(QUERY_3_STATEMENT)
            .queryNormalized(QUERY_3_STATEMENT)
            .resultNumber(QUERY_3_RESULT_NUMBER)
            .resultHash(QUERY_3_RESULT_HASH)
            .owner(USER_1_BRIEF_DTO)
            .queryHash(QUERY_3_QUERY_HASH)
            .execution(QUERY_3_EXECUTION)
            .isPersisted(QUERY_3_PERSISTED)
            .resultNumber(2L)
            .build();

    public static final QueryDto QUERY_7_DTO = QueryDto.builder()
            .id(QUERY_7_ID)
            .databaseId(DATABASE_4_ID)
            .query(QUERY_7_STATEMENT)
            .queryNormalized(QUERY_7_STATEMENT)
            .resultNumber(QUERY_7_RESULT_NUMBER)
            .resultHash(QUERY_7_RESULT_HASH)
            .owner(USER_1_BRIEF_DTO)
            .queryHash(QUERY_7_QUERY_HASH)
            .execution(QUERY_7_EXECUTION)
            .isPersisted(QUERY_7_PERSISTED)
            .resultNumber(2L)
            .build();

    public static final QueryDto QUERY_6_DTO = QueryDto.builder()
            .id(QUERY_6_ID)
            .databaseId(DATABASE_1_ID)
            .query(QUERY_6_STATEMENT)
            .queryNormalized(QUERY_6_STATEMENT)
            .resultNumber(QUERY_6_RESULT_NUMBER)
            .resultHash(QUERY_6_RESULT_HASH)
            .owner(USER_1_BRIEF_DTO)
            .queryHash(QUERY_6_QUERY_HASH)
            .execution(QUERY_6_EXECUTION)
            .isPersisted(QUERY_6_PERSISTED)
            .build();

    public static final QueryDto QUERY_8_DTO = QueryDto.builder()
            .id(QUERY_8_ID)
            .databaseId(DATABASE_2_ID)
            .query(QUERY_8_STATEMENT)
            .queryNormalized(QUERY_8_STATEMENT)
            .resultNumber(QUERY_8_RESULT_NUMBER)
            .resultHash(QUERY_8_RESULT_HASH)
            .owner(USER_1_BRIEF_DTO)
            .queryHash(QUERY_8_QUERY_HASH)
            .execution(QUERY_8_EXECUTION)
            .isPersisted(QUERY_8_PERSISTED)
            .resultNumber(3L)
            .build();

}
