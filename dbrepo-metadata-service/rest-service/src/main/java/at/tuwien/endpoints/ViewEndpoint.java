package at.tuwien.endpoints;

import at.tuwien.api.database.CreateViewDto;
import at.tuwien.api.database.ViewBriefDto;
import at.tuwien.api.database.ViewDto;
import at.tuwien.api.database.ViewUpdateDto;
import at.tuwien.api.error.ApiErrorDto;
import at.tuwien.entities.database.Database;
import at.tuwien.entities.database.View;
import at.tuwien.entities.user.User;
import at.tuwien.exception.*;
import at.tuwien.mapper.MetadataMapper;
import at.tuwien.service.DatabaseService;
import at.tuwien.service.UserService;
import at.tuwien.service.ViewService;
import io.micrometer.observation.annotation.Observed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "/api/database/{databaseId}/view")
public class ViewEndpoint extends AbstractEndpoint {

    private final UserService userService;
    private final ViewService viewService;
    private final MetadataMapper metadataMapper;
    private final DatabaseService databaseService;

    @Autowired
    public ViewEndpoint(UserService userService, ViewService viewService, MetadataMapper metadataMapper,
                        DatabaseService databaseService) {
        this.userService = userService;
        this.viewService = viewService;
        this.metadataMapper = metadataMapper;
        this.databaseService = databaseService;
    }

    @GetMapping
    @Transactional(readOnly = true)
    @Observed(name = "dbrepo_views_findall")
    @Operation(summary = "List views",
            description = "Lists views known to the metadata database.",
            security = {@SecurityRequirement(name = "bearerAuth"), @SecurityRequirement(name = "basicAuth")})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Find views successfully",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ViewBriefDto.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "Database or user could not be found",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
    })
    public ResponseEntity<List<ViewBriefDto>> findAll(@NotNull @PathVariable("databaseId") UUID databaseId,
                                                      Principal principal) throws UserNotFoundException,
            DatabaseNotFoundException {
        log.debug("endpoint find all views, databaseId={}", databaseId);
        final Database database = databaseService.findById(databaseId);
        final User caller;
        if (principal != null) {
            caller = userService.findById(getId(principal));
        } else {
            caller = null;
        }
        return ResponseEntity.ok(viewService.findAll(database, caller)
                .stream()
                .map(metadataMapper::viewToViewBriefDto)
                .collect(Collectors.toList()));
    }

    @PostMapping
    @Transactional
    @PreAuthorize("hasAuthority('create-database-view')")
    @Observed(name = "dbrepo_view_create")
    @Operation(summary = "Create view",
            description = "Creates a view. This can only be performed by the database owner. Requires role `create-database-view`.",
            security = {@SecurityRequirement(name = "bearerAuth"), @SecurityRequirement(name = "basicAuth")})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Create view successfully",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ViewBriefDto.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Create view query is malformed",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "403",
                    description = "Credentials missing",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Failed to find database/user in metadata database.",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "409",
                    description = "View exists with name",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "423",
                    description = "Create view resulted in an invalid query statement",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "502",
                    description = "Connection to search service failed",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "503",
                    description = "Failed to save in search service",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
    })
    public ResponseEntity<ViewBriefDto> create(@NotNull @PathVariable("databaseId") UUID databaseId,
                                               @NotNull @Valid @RequestBody CreateViewDto data,
                                               @NotNull Principal principal) throws NotAllowedException,
            MalformedException, DataServiceException, DataServiceConnectionException, DatabaseNotFoundException,
            UserNotFoundException, SearchServiceException, SearchServiceConnectionException, TableNotFoundException,
            ImageNotFoundException, ViewExistsException {
        log.debug("endpoint create view, databaseId={}, data.name={}", databaseId, data.getName());
        final Database database = databaseService.findById(databaseId);
        if (!database.getOwner().getId().equals(getId(principal))) {
            log.error("Failed to create view: not the database owner");
            throw new NotAllowedException("Failed to create view: not the database owner");
        }
        if (database.getViews().stream().anyMatch(v -> v.getInternalName().equals(metadataMapper.nameToInternalName(data.getName())))) {
            log.error("Failed to create view: name exists");
            throw new ViewExistsException("Failed to create view: name exists");
        }
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(metadataMapper.viewToViewBriefDto(
                        viewService.create(database, userService.findById(getId(principal)), data)));
    }

    @GetMapping("/{viewId}")
    @Transactional(readOnly = true)
    @Observed(name = "dbrepo_view_find")
    @Operation(summary = "Get view",
            description = "Gets a view with id in the metadata database.",
            security = {@SecurityRequirement(name = "bearerAuth"), @SecurityRequirement(name = "basicAuth")})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Find view successfully",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ViewDto.class))}),
            @ApiResponse(responseCode = "403",
                    description = "Find view is not permitted",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Database, view or user could not be found",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
    })
    public ResponseEntity<ViewDto> find(@NotNull @PathVariable("databaseId") UUID databaseId,
                                        @NotNull @PathVariable("viewId") UUID viewId,
                                        Principal principal) throws DatabaseNotFoundException,
            ViewNotFoundException {
        log.debug("endpoint find view, databaseId={}, viewId={}", databaseId, viewId);
        final Database database = databaseService.findById(databaseId);
        final View view = viewService.findById(database, viewId);
        return ResponseEntity.status(HttpStatus.OK)
                .body(metadataMapper.viewToViewDto(view));
    }

    @DeleteMapping("/{viewId}")
    @Transactional
    @PreAuthorize("hasAuthority('delete-database-view')")
    @Observed(name = "dbrepo_view_delete")
    @Operation(summary = "Delete view",
            description = "Deletes a view with id. Requires role `delete-database-view`.",
            security = {@SecurityRequirement(name = "bearerAuth"), @SecurityRequirement(name = "basicAuth")})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202",
                    description = "Delete view successfully"),
            @ApiResponse(responseCode = "400",
                    description = "Delete view query is malformed",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "403",
                    description = "Deletion not allowed",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Database, view or user could not be found",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "423",
                    description = "Delete view resulted in an invalid query statement",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "502",
                    description = "Connection to search service failed",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "503",
                    description = "Failed to save in search service",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
    })
    public ResponseEntity<Void> delete(@NotNull @PathVariable("databaseId") UUID databaseId,
                                       @NotNull @PathVariable("viewId") UUID viewId,
                                       @NotNull Principal principal) throws NotAllowedException, DataServiceException,
            DataServiceConnectionException, DatabaseNotFoundException, ViewNotFoundException, SearchServiceException,
            SearchServiceConnectionException, UserNotFoundException {
        log.debug("endpoint delete view, databaseId={}, viewId={}", databaseId, viewId);
        final Database database = databaseService.findById(databaseId);
        if (!database.getOwner().getId().equals(getId(principal))) {
            log.error("Failed to delete view: not the database owner {}", database.getOwner().getId());
            throw new NotAllowedException("Failed to delete view: not the database owner " + database.getOwner().getId());
        }
        final View view = viewService.findById(database, viewId);
        viewService.delete(view);
        return ResponseEntity.accepted()
                .build();
    }

    @PutMapping("/{viewId}")
    @Transactional
    @PreAuthorize("hasAuthority('modify-view-visibility')")
    @Observed(name = "dbrepo_view_update")
    @Operation(summary = "Update view",
            description = "Updates a view with id. This can only be performed by the view owner or database owner. Requires role `create-database-view`.",
            security = {@SecurityRequirement(name = "bearerAuth"), @SecurityRequirement(name = "basicAuth")})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202",
                    description = "Update view successfully"),
            @ApiResponse(responseCode = "400",
                    description = "Update view query is malformed",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "403",
                    description = "Update not allowed",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Database or View could not be found",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "502",
                    description = "Connection to search service failed",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
            @ApiResponse(responseCode = "503",
                    description = "Failed to save in search service",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorDto.class))}),
    })
    public ResponseEntity<ViewBriefDto> update(@NotNull @PathVariable("databaseId") UUID databaseId,
                                               @NotNull @PathVariable("viewId") UUID viewId,
                                               @NotNull @Valid @RequestBody ViewUpdateDto data,
                                               @NotNull Principal principal) throws NotAllowedException,
            DataServiceConnectionException, DatabaseNotFoundException, ViewNotFoundException, SearchServiceException,
            SearchServiceConnectionException, UserNotFoundException {
        log.debug("endpoint update view, databaseId={}, viewId={}", databaseId, viewId);
        final Database database = databaseService.findById(databaseId);
        final View view = viewService.findById(database, viewId);
        if (!database.getOwner().getId().equals(getId(principal)) && !view.getOwner().getId().equals(getId(principal))) {
            log.error("Failed to update view: not the database- or view owner");
            throw new NotAllowedException("Failed to update view: not the database- or view owner");
        }
        return ResponseEntity.accepted()
                .body(metadataMapper.viewToViewBriefDto(
                        viewService.update(database, view, data)));
    }

}
