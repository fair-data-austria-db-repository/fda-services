package at.tuwien.gateway;

import at.tuwien.test.AbstractUnitTest;
import at.tuwien.api.orcid.OrcidDto;
import at.tuwien.exception.OrcidNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@Log4j2
@SpringBootTest
@ExtendWith(SpringExtension.class)
public class OrcidGatewayUnitTest extends AbstractUnitTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private OrcidGateway orcidGateway;

    @Test
    public void findByUrl_succeeds() throws OrcidNotFoundException {

        /* mock */
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(OrcidDto.class)))
                .thenReturn(ResponseEntity.status(HttpStatus.OK)
                        .build());

        /* test */
        orcidGateway.findByUrl(USER_1_ORCID_URL);
    }

    @Test
    public void findByUrl_fails() {

        /* mock */
        doThrow(HttpServerErrorException.class)
                .when(restTemplate)
                .exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(OrcidDto.class));

        /* test */
        assertThrows(OrcidNotFoundException.class, () -> {
            orcidGateway.findByUrl(USER_1_ORCID_URL);
        });
    }

}
