package at.tuwien.entities.identifier;

import lombok.Getter;

@Getter
public enum NameType {
    PERSONAL,
    ORGANIZATIONAL;
}
