
package at.tuwien.entities.identifier;

import lombok.Getter;

@Getter
public enum NameIdentifierSchemeType {
    ORCID,
    ROR,
    ISNI,
    GRID
}
