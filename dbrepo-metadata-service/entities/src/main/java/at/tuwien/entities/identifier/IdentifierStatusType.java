package at.tuwien.entities.identifier;

import lombok.Getter;

@Getter
public enum IdentifierStatusType {
    DRAFT,
    PUBLISHED;
}
