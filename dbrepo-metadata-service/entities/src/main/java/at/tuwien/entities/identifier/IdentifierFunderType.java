package at.tuwien.entities.identifier;

public enum IdentifierFunderType {
    CROSSREF_FUNDER_ID,
    ROR,
    GND,
    ISNI,
    OTHER
}
