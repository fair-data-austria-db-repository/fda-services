
package at.tuwien.entities.identifier;

import lombok.Getter;

@Getter
public enum AffiliationIdentifierSchemeType {
    ROR,
    GRID,
    ISNI
}
