| **Metric**                         | **Description**               |
|------------------------------------|-------------------------------|
| `dbrepo_access_delete`             | Delete access                 |
| `dbrepo_access_get`                | Find/Check access             |
| `dbrepo_access_give`               | Give access                   |
| `dbrepo_access_modify`             | Modify access                 |
| `dbrepo_container_create`          | Create container              |
| `dbrepo_container_delete`          | Delete container              |
| `dbrepo_container_find`            | Find container                |
| `dbrepo_container_findall`         | List containers               |
| `dbrepo_database_create`           | Create database               |
| `dbrepo_database_find`             | Find database                 |
| `dbrepo_database_findall`          | List databases                |
| `dbrepo_database_image`            | Update database preview image |
| `dbrepo_database_image_view`       | Get database preview image    |
| `dbrepo_database_transfer`         | Update database owner         |
| `dbrepo_database_visibility`       | Update database visibility    |
| `dbrepo_identifier_create`         | Create identifier             |
| `dbrepo_identifier_delete`         | Delete identifier             |
| `dbrepo_identifier_find`           | Find identifier               |
| `dbrepo_identifier_list`           | List identifiers              |
| `dbrepo_identifier_publish`        | Publish identifier            |
| `dbrepo_identifier_retrieve`       | Retrieve PID metadata         |
| `dbrepo_identifier_save`           | Save identifier               |
| `dbrepo_image_create`              | Create image                  |
| `dbrepo_image_delete`              | Delete image                  |
| `dbrepo_image_find`                | Find image                    |
| `dbrepo_image_findall`             | List images                   |
| `dbrepo_image_update`              | Update image                  |
| `dbrepo_license_findall`           | List licenses                 |
| `dbrepo_maintenance_create`        | Create message                |
| `dbrepo_maintenance_delete`        | Delete message                |
| `dbrepo_maintenance_find`          | Find message                  |
| `dbrepo_maintenance_findall`       | List messages                 |
| `dbrepo_maintenance_update`        | Update message                |
| `dbrepo_oai_identifiers_list`      | List identifiers              |
| `dbrepo_oai_identify`              | Identify repository           |
| `dbrepo_oai_metadataformats_list`  | List metadata formats         |
| `dbrepo_oai_record_get`            | Get record                    |
| `dbrepo_ontologies_create`         | Create ontology               |
| `dbrepo_ontologies_delete`         | Delete ontology               |
| `dbrepo_ontologies_entities_find`  | Find entities                 |
| `dbrepo_ontologies_find`           | Find ontology                 |
| `dbrepo_ontologies_findall`        | List ontologies               |
| `dbrepo_ontologies_update`         | Update ontology               |
| `dbrepo_semantic_column_analyse`   | Suggest semantics             |
| `dbrepo_semantic_concepts_findall` | List concepts                 |
| `dbrepo_semantic_table_analyse`    | Suggest semantics             |
| `dbrepo_semantic_units_findall`    | List units                    |
| `dbrepo_semantics_column_save`     | Update semantics              |
| `dbrepo_statistic_table_update`    | Update statistics             |
| `dbrepo_table_create`              | Create table                  |
| `dbrepo_table_delete`              | Delete table                  |
| `dbrepo_table_update`              | Update table                  |
| `dbrepo_tables_find`               | Find table                    |
| `dbrepo_tables_findall`            | List tables                   |
| `dbrepo_tables_refresh`            | Update database table schemas |
| `dbrepo_user_create`               | Create user                   |
| `dbrepo_user_find`                 | Get user                      |
| `dbrepo_user_modify`               | Update user                   |
| `dbrepo_users_list`                | List users                    |
| `dbrepo_view_create`               | Create view                   |
| `dbrepo_view_delete`               | Delete view                   |
| `dbrepo_view_find`                 | Get view                      |
| `dbrepo_view_update`               | Update view                   |
| `dbrepo_views_findall`             | List views                    |
| `dbrepo_views_refresh`             | Update database view schemas  |
