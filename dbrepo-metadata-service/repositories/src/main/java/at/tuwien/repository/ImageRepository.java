package at.tuwien.repository;

import at.tuwien.entities.container.image.ContainerImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ImageRepository extends JpaRepository<ContainerImage, UUID> {

    Optional<ContainerImage> findByNameAndVersion(String name, String version);

    Optional<ContainerImage> findByIsDefault(Boolean isDefault);

}
