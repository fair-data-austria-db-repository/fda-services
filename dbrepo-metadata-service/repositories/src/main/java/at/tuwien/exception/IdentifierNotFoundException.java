package at.tuwien.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "error.identifier.missing")
public class IdentifierNotFoundException extends Exception {

    public IdentifierNotFoundException(String msg) {
        super(msg);
    }

    public IdentifierNotFoundException(String msg, Throwable thr) {
        super(msg + ": " + thr.getLocalizedMessage(), thr);
    }

    public IdentifierNotFoundException(Throwable thr) {
        super(thr);
    }

}
