package at.tuwien.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "error.queue.missing")
public class QueueNotFoundException extends Exception {

    public QueueNotFoundException(String msg) {
        super(msg);
    }

    public QueueNotFoundException(String msg, Throwable thr) {
        super(msg + ": " + thr.getLocalizedMessage(), thr);
    }

    public QueueNotFoundException(Throwable thr) {
        super(thr);
    }

}
