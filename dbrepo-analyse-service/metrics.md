| **Metric**                  | **Description**                             |
|-----------------------------|---------------------------------------------|
| `dbrepo_analyse_datatypes`  | Time needed to analyse datatypes of dataset |
| `dbrepo_analyse_keys`       | Time needed to analyse keys of dataset      |
| `dbrepo_analyse_table_stat` | Time needed to analyse table statistics     |
| `dbrepo_analyse_table_stat` | Time needed to analyse table statistics     |