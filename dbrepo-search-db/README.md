
# Search Database

## REST

### Indices

* View all [http://localhost:9200/_cat/indices](http://localhost:9200/_cat/indices)

### Nodes

* View all [http://localhost:9200/_cat/nodes](http://localhost:9200/_cat/nodes)

### Health

* View all [http://localhost:9200/_plugins/_security/health](http://localhost:9200/_plugins/_security/health)