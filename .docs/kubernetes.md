---
author: Martin Weise
---

## TL;DR

To install DBRepo in your existing cluster, download the
sample [
`values.yaml`](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services/-/blob/release-1.7/helm/dbrepo/values.yaml)
for your deployment and update the variables, especially `hostname`.

```shell
helm upgrade --install dbrepo \
  -n dbrepo \
  "oci://registry.datalab.tuwien.ac.at/dbrepo/helm/dbrepo" \
  --values ./values.yaml \
  --version "1.7.1" \
  --create-namespace \
  --cleanup-on-fail
```

## Prerequisites

* Kubernetes 1.30+
* PV provisioner support in the underlying infrastructure

### Resource Quota

By default, any service requests a minimum quota of cpu and memory by default. These values are based on the absolute
minimum needed to start the service. From our experience your cluster needs to meet the following `ResourceQuota`:

```yaml
requests.cpu: 12000m
requests.ephemeral-storage: 3072Mi
secrets: '50'
<storageClass>.storageclass.storage.k8s.io/persistentvolumeclaims: '20'
<storageClass>.storageclass.storage.k8s.io/requests.storage: 150Gi
persistentvolumeclaims: '20'
requests.memory: 14336Mi
pods: '40'
requests.storage: 150Gi
limits.memory: 20480Mi
configmaps: '20'
services: '40'
```

You can reduce the resources needed by disabling services and metric collection. You can disable e.g. the dashboard
service:

``` title="values.yaml"
dashboardservice:
  enabled: false
```

## Limitations

1. The entire Helm deployment is rootless (=`runAsNonRoot=true`) except for
   the [Storage Service](../api/storage-service) which still requires a root user.

!!! question "Do you miss functionality? Do these limitations affect you?"

    We strongly encourage you to help us implement it as we are welcoming contributors to open-source software and get
    in [contact](../../contact) with us, we happily answer requests for collaboration with attached CV and your programming 
    experience!
