---
author: Martin Weise
---

## tl;dr

[:fontawesome-solid-database: &nbsp;Dataset](https://dbrepo1.ec.tuwien.ac.at/pid/52){ .md-button .md-button--primary target="_blank" }

## Description

This section will be expanded soon.

## Solution

We host a mirror from the [public data](https://www.stadttheater.uni-hamburg.de/) provided by UHH.

## DBRepo Features

- [x] System versioning
- [x] Subset exploration
- [x] External data access for analysis

## Acknowledgement

This work was part of a cooperation with the [University of Hamburg](https://www.uni-hamburg.de/en.html).

<img src="../../images/logos/uhh.png" width=100 />