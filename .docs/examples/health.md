---
author: Martin Weise
---

## tl;dr

[:fontawesome-solid-database: &nbsp;Dataset](https://dbrepo1.ec.tuwien.ac.at/database/32/info){ .md-button .md-button--primary target="_blank" }

## Description

This dataset contains anonymous behavioural and ocular recordings from healthy participants during performance of a
decision-making task between action movements in which we studied the influence of social facilitation and social
pressure. The original dataset is available on [Kaggle](https://doi.org/10.34740/kaggle/ds/4292829).

## Solution

Importing the original dataset from Kaggle into DBRepo allows for exploratory analysis and increased value for secondary
use, e.g. with our [`pandas`](https://pypi.org/project/pandas/)-compatible [Python Library](../../api/python).

## DBRepo Features

- [x] System versioning
- [x] Subset exploration
- [x] Large dataset (3 tables, around 6GB)
- [x] External data access for analysis
