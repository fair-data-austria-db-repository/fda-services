---
author: Martin Weise
---

## tl;dr

tbd

## Description

The main aim of the E-LAUTE project is to create a novel form of music edition: an "open knowledge platform" in which 
musicology, music practice, music informatics and literary studies intertwine and transform the "classic" edition into a 
space of interdisciplinary and discipline-specific work. 

In order to create a comprehensive, complete modern scholarly edition, the E-LAUTE project synchronises high technology
informatics in the fields of encoding, linking, recognition (OMR) and automatic transcription with manual music 
transcription and musical performance practice. We consider recordings of lute music a conceptual component of the 
edition.

## Solution

tbd

## DBRepo Features

- [x] Data preservation of historic data
- [x] Subset exploration
- [x] External visualization of the database

## Acknowledgement

This work was part of a cooperation with the University of Vienna in the context of [E-LAUTE](https://e-laute.info/).

<img src="../../images/logos/univie_small.png" width=100 />