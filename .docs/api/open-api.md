---
author: Martin Weise
---

![OpenAPI 3.1](https://img.shields.io/badge/OpenAPI-3.1-leaf)

## tl;dr

[:simple-swagger: &nbsp;View Swagger-UI](../../rest/){ .md-button .md-button--primary tabindex=-1 }

## Overview

All services are documented using the [OpenAPI 3.1](https://www.openapis.org/) documentation standard.