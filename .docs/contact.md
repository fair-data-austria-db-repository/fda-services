---
author: Martin Weise
hide:
- navigation
---

# Contact

## Team

### Strategy &amp; Partnerships

Ao.univ.Prof. Dr. [Andreas Rauber](https://www.ifs.tuwien.ac.at/~andi)<br />
Technische Universit&auml;t Wien<br />
Research Unit Data Science<br />
Favoritenstra&szlig;e 9-11<br />
A-1040 Vienna, Austria

### Technical Lead

Projektass. Dipl.-Ing. [Martin Weise](https://ec.tuwien.ac.at/~weise/)<br />
Technische Universit&auml;t Wien<br />
Research Unit Data Science<br />
Favoritenstra&szlig;e 9-11<br />
A-1040 Vienna, Austria

## Contributors (alphabetically)

- Ganguly, Raman
- Gergely, Eva
- G&uuml;&#231;l&uuml;, G&ouml;kay
- Grantner, Tobias
- Karnbach, Geoffrey
- Lukic, Nikola
- Mahler, Lukas
- Michlits, Cornelia
- Rauber, Andreas
- Spannring, Max
- Staudinger, Moritz
- Stytsenko, Kirill
- Taha, Josef
- Tsepelakis, Sotirios
- Weise, Martin

Interested in contributing? Send us an e-mail!
