<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Redirect Notice</title>
    <meta http-equiv="Refresh" content="0; url='/infrastructures/dbrepo/DOC_VERSION/'" />
</head>
<body>
<h1>Redirect Notice</h1>
<p>
    This page should automatically open the documentation for version <code>DOC_VERSION</code>. In case this page does not load the site is
    available at:
</p>
<p>
    <a href="/infrastructures/dbrepo/DOC_VERSION/">/infrastructures/dbrepo/DOC_VERSION/</a>
</p>
</body>
</html>